from Vehicle_Parameter import Vehicle_Data
import math
import time


# Definition der Klasse für Koordinatensystem
class Coordinate_System:
    
    # 2 Dimensionales Koordinatensystem
    # Definition x-Richtung: Gerade aus bei Zeitpunkt 0

    def __init__(self, myVehicle: Vehicle_Data):

        # Instanz der Klasse Verhicle_Data für notwendige Fahrzeugparameter
        self.myVehicle = myVehicle
        
        # Positionsdaten
        self.x = 0 # X-Position Truck Vorderachse
        self.y = 0 # Y-Position Truck Vorderachse
        self.x_rear = 0 # X-Position Truck Hinterachse
        self.y_rear = 0 # Y-Position Truck Hinterachse
        self.x_trailer = 0 # X-Position Trailerachse
        self.y_trailer = 0 # Y-Position Trailerachse
        self.x_hitch = 0
        self.y_hitch = 0
        self.truck_orientation = 0 # Orientirerungsrichtung des Trucks in Bogenmaß (Winkel zur x-Achse)

        self.last_timestamp = time.time()

     # Berechnung der Truck Position (Vorderachse) durch Integration des Fahrweges
    def calc_truck_position(self, velocity, steering_angle):
         deltaT = time.time() - self.last_timestamp
         deltaX = velocity * math.cos(self.truck_orientation + steering_angle) * deltaT
         deltaY = velocity * math.sin(self.truck_orientation + steering_angle) * deltaT
         self.x += deltaX
         self.y += deltaY
         self.calc_truck_orientation(steering_angle)

     # Berechnung der Hinterachsenposition
    def calc_position_rear_axis(self, velocity, steering_angle):
         self.calc_truck_position(velocity, steering_angle)
         # Vorderachse --> Hinterachse: Beachtung Orientierung
         deltaX = self.myVehicle.l_truck * math.cos(self.truck_orientation)
         self.x_rear = self.x - deltaX
         deltaY = self.myVehicle.l_truck * math.sin(self.truck_orientation)
         self.y_rear = self.y - deltaY

    
     # Berechnung der Trailerachsenposition
    def calc_position_trailer(self, velocity, steering_angle, hitch_angle):
         self.calc_truck_position(velocity, steering_angle)
         # Vorderachse --> Hitch --> Trailer: Beachtung Orientierung und Hitchwinkel
         deltaX_truck = (self.myVehicle.l_truck + self.myVehicle.l_hitch) * math.cos(self.truck_orientation)
         deltaX_trailer = self.myVehicle.l_trailer * math.cos(self.truck_orientation + hitch_angle)
         self.x_trailer = self.x - (deltaX_truck + deltaX_trailer)
         deltaY_truck = (self.myVehicle.l_truck + self.myVehicle.l_hitch) * math.sin(self.truck_orientation)
         deltaY_trailer = self.myVehicle.l_trailer * math.sin(self.truck_orientation + hitch_angle)
         self.y_trailer = self.y - (deltaY_truck + deltaY_trailer)

         # Berechnung der Hinterachsenposition
    def calc_position_hitch(self, velocity, steering_angle):
         self.calc_truck_position(velocity, steering_angle)
         # Vorderachse --> Hinterachse + Weg zur Kupplung: Beachtung Orientierung
         deltaX = (self.myVehicle.l_truck + self.myVehicle.l_hitch) * math.cos(self.truck_orientation)
         self.x_hitch = self.x - deltaX
         deltaY = (self.myVehicle.l_truck + self.myVehicle.l_hitch) * math.sin(self.truck_orientation)
         self.y_hitch = self.y - deltaY

     # Berechnung der Truck-Orientierung
    def calc_truck_orientation(self, steering_angle):
            self.truck_orientation += steering_angle
            self.truck_orientation = self.truck_orientation % 360
            self.truck_orientation = self.truck_orientation / 360 * 2 * math.pi
     

     # Berechnung aller Positionen mit aufgerufen mit einer Methode 
    def calc_positions(self, velocity, steering_angle, hitch_angle):
        # Position Vorderachse wird in anderen beiden Berechnungen aufgerufen
         self.calc_position_rear_axis(velocity, steering_angle)
         self.calc_position_hitch(velocity, steering_angle)
         self.calc_position_trailer(velocity, steering_angle, hitch_angle)
         




