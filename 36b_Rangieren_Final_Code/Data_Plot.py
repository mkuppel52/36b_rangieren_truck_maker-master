import matplotlib.pyplot as plt

# Datei-Inhalt: Funktionen zum Plotten von Regelkurven und des gefahrenen Weges

# Daten, welche geplotted werden, müssen als Array / Liste vorleigen
	# Regler: Winkel-Daten und Regelabweichung
	# Trajektorie: Position Hinterachse Fahrzeug / Anhänger, sowie die einzelnen Trajektorien (werden in Trajectory Data berechnet)
	# Zeit: wird wenn nötig innerhalb der Plot-Funktion berechnet

# Plotten eines PID Reglers: Stellgröße: Lenk- oder Knickwinkel (angezeigt durch Angle_Type "Lenk" oder "Knick"), Regelabweichung: Distanz zur Trajektorie
def plot_PID_regulator(angle_data, offset_data, time_step, k_p, k_i, k_d, angle_type, maneuver_type):

	time = []
	i = 0
	current_time = 0.0


	# Berechnung der Daten für Zeit-Achse (x-Achse0)
	# Aufaddieren der Zykluszeit / der diskreten Abtastzeitpunkte anhand der Menge an Datenpunkte
	while i < len(offset_data):
		time.append(current_time)	
		current_time = current_time + time_step	
		i = i + 1
	
		
	MEDIUM_SIZE = 12

	# Einstellen der Beschriftungen
	plt.rc('font', size=MEDIUM_SIZE)
	plt.rc('axes', titlesize=MEDIUM_SIZE)
	plt.rc('axes', labelsize=MEDIUM_SIZE)
	plt.rc('xtick', labelsize=MEDIUM_SIZE)
	plt.rc('ytick', labelsize=MEDIUM_SIZE)
	plt.rc('legend', fontsize=MEDIUM_SIZE)
	plt.rc('figure', titlesize=MEDIUM_SIZE)
	

	plt.figure()
	
	plt.subplot(121)
	plt.xlabel('Zeit [s]')

	plt.ylabel(str(angle_type) + 'winkel [°]')
	plt.title('Stellgröße')
	plt.plot(time, angle_data)

	plt.subplot(122)
	plt.title('Regelabweichung ')
	plt.xlabel('Zeit [s]')
	plt.ylabel('Distanz zur Trajektorie [m]')
	plt.plot(time, offset_data)
	
	plt.suptitle('k_p = ' + str(k_p) + '   k_i = ' + str(k_i) + '   k_d = ' + str(k_d) + ' Maneuver: ' + str(maneuver_type))
	plt.show()


# Plotten der Fahrspur des Trucks und der Parktrajektorie, bestehend aus gerader Strecke (Vorbeifahren), der Kreisbögen und der Parklücke
def plot_trajectory(truck_posX_data, truck_posY_data, straight_trajectory_posX, straight_trajectory_posY, parking_lot_trajectory_posX, parking_lot_trajectory_posY, circle_first_posX, circle_first_posY, circle_second_posX, circle_second_posY):
	
	MEDIUM_SIZE = 12

	# Einstellen der Beschriftungen
	plt.rc('font', size=MEDIUM_SIZE)
	plt.rc('axes', titlesize=MEDIUM_SIZE)
	plt.rc('axes', labelsize=MEDIUM_SIZE)
	plt.rc('xtick', labelsize=MEDIUM_SIZE)
	plt.rc('ytick', labelsize=MEDIUM_SIZE)
	plt.rc('legend', fontsize=MEDIUM_SIZE)
	plt.rc('figure', titlesize=MEDIUM_SIZE)

	plt.figure()

	plt.xlabel('x Position [m]')
	plt.ylabel('y Position [m]')
	
	plt.title('Gefahrener Weg der Truck-Hinterachse im Vergleich zur berechneten Trajektorie')

	plt.plot(truck_posX_data, truck_posY_data, color = 'red', label='Hinterachse Truck')
	plt.plot(straight_trajectory_posX, straight_trajectory_posY, 'b--', label='Soll-Trajektorie')
	plt.plot(parking_lot_trajectory_posX, parking_lot_trajectory_posY, 'g-.', label='Parklücke')
	plt.plot(circle_first_posX, circle_first_posY, 'b--')
	plt.plot(circle_second_posX, circle_second_posY, 'b--')
	plt.legend()

	plt.show()


# Plotten der Fahrspur des Trailers und der Parktrajektorie, bestehend aus gerader Strecke (Vorbeifahren), der Kreisbögen, der geraden Strecke zum Umlenken und der Parklücke
def plot_trajectory_trailer(trailer_posX_data, trailer_posY_data, straight_trajectory_posX, straight_trajectory_posY, parking_lot_trajectory_posX, parking_lot_trajectory_posY, circle_first_posX, circle_first_posY, switch_circle_posX, switch_circle_posY, circle_second_posX, circle_second_posY):
	
	MEDIUM_SIZE = 12
	
	# Einstellen der Beschriftungen
	plt.rc('font', size=MEDIUM_SIZE)
	plt.rc('axes', titlesize=MEDIUM_SIZE)
	plt.rc('axes', labelsize=MEDIUM_SIZE)
	plt.rc('xtick', labelsize=MEDIUM_SIZE)
	plt.rc('ytick', labelsize=MEDIUM_SIZE)
	plt.rc('legend', fontsize=MEDIUM_SIZE)
	plt.rc('figure', titlesize=MEDIUM_SIZE)

	plt.figure()

	plt.xlabel('x Position [m]')
	plt.ylabel('y Position [m]')
	
	plt.title('Gefahrener Weg der Trailer-Hinterachse im Vergleich zur berechneten Trajektorie')

	plt.plot(trailer_posX_data, trailer_posY_data, color = 'red', label='Hinterachse Trailer')
	plt.plot(straight_trajectory_posX, straight_trajectory_posY, 'b--', label='Soll-Trajektorie')
	plt.plot(parking_lot_trajectory_posX, parking_lot_trajectory_posY, 'g-.', label='Parklücke')
	plt.plot(circle_first_posX, circle_first_posY, 'b--')
	plt.plot(switch_circle_posX, switch_circle_posY, 'b--')
	plt.plot(circle_second_posX, circle_second_posY, 'b--')
	plt.legend()

	plt.show()

# Plotten der Fahrspur des Trucks UND des Trailers und der Parktrajektorie, bestehend aus gerader Strecke (Vorbeifahren), der Kreisbögen, der geraden Strecke zum Umlenken und der Parklücke
def plot_trajectory_truck_trailer(truck_posX_data, truck_posY_data, trailer_posX_data, trailer_posY_data, straight_trajectory_posX, straight_trajectory_posY, parking_lot_trajectory_posX, parking_lot_trajectory_posY, circle_first_posX, circle_first_posY, switch_circle_posX, switch_circle_posY, circle_second_posX, circle_second_posY):
	
	MEDIUM_SIZE = 12

	# Einstellen der Beschriftungen
	plt.rc('font', size=MEDIUM_SIZE)
	plt.rc('axes', titlesize=MEDIUM_SIZE)
	plt.rc('axes', labelsize=MEDIUM_SIZE)
	plt.rc('xtick', labelsize=MEDIUM_SIZE)
	plt.rc('ytick', labelsize=MEDIUM_SIZE)
	plt.rc('legend', fontsize=MEDIUM_SIZE)
	plt.rc('figure', titlesize=MEDIUM_SIZE)

	plt.figure()

	plt.xlabel('x Position [m]')
	plt.ylabel('y Position [m]')
	
	plt.title('Gefahrener Weg der Truck- & Trailer-Hinterachse im Vergleich zur berechneten Trajektorie')

	plt.plot(truck_posX_data, truck_posY_data, color = 'orange', linestyle=(0, (3, 1,1,1,1,1)), label='Hinterachse Truck')
	plt.plot(trailer_posX_data, trailer_posY_data, color = 'red', label='Hinterachse Trailer')
	plt.plot(straight_trajectory_posX, straight_trajectory_posY, 'b--')
	plt.plot(parking_lot_trajectory_posX, parking_lot_trajectory_posY, 'g-.', label='Parklücke')
	plt.plot(circle_first_posX, circle_first_posY,  'b--', label='Soll-Trajektorie')
	plt.plot(switch_circle_posX, switch_circle_posY, 'b--')
	plt.plot(circle_second_posX, circle_second_posY,  'b--')
	plt.legend()

	plt.show()


# Plotten des Knickwinkelreglers: Stellgröße: Lenkwinkel, Regelgröße Knickwinkel, Parameter: Lambda 1 und Lambda 2
def plot_hitch_regulator(steer_angle_data, hitch_angle_data, hitch_target, time_step, lambda1, lambda2):

	time = []
	i = 0
	current_time = 0.0

	while i < len(steer_angle_data):
		time.append(current_time)	
		current_time = current_time + time_step	
		i = i + 1
	
	MEDIUM_SIZE = 12

	# Einstellen der Beschriftungen
	plt.rc('font', size=MEDIUM_SIZE)
	plt.rc('axes', titlesize=MEDIUM_SIZE)
	plt.rc('axes', labelsize=MEDIUM_SIZE)
	plt.rc('xtick', labelsize=MEDIUM_SIZE)
	plt.rc('ytick', labelsize=MEDIUM_SIZE)
	plt.rc('legend', fontsize=MEDIUM_SIZE)
	plt.rc('figure', titlesize=MEDIUM_SIZE)	

	plt.figure()
	
	plt.subplot(121)
	plt.xlabel('Zeit [s]')

	plt.ylabel('Lenkwinkel [°]')
	plt.title('Stellgröße')
	plt.plot(time, steer_angle_data)

	plt.subplot(122)
	plt.title('Regelgröße ')
	plt.xlabel('Zeit [s]')
	plt.ylabel('Knickwinkel [°]')
	plt.plot(time, hitch_angle_data)
	
	plt.suptitle('Reichegger Hitch Regulator, l1 = ' + str(lambda1) + ', l2 = ' + str(lambda2) + ', Target = ' + str(hitch_target))
	plt.show()



# Plotten der Lenkwinkel bei implementierter Begrenzung der Lenkwinkelgeschwindigkeit
def plot_steering_slowdown(time_step, steer_angle_data, steer_target):
	
	time = []
	i = 0
	current_time = 0.0

	# Berechnung der Daten für Zeit-Achse (x-Achse0)
	# Aufaddieren der Zykluszeit / der diskreten Abtastzeitpunkte anhand der Menge an Datenpunkte
	while i < len(steer_angle_data):
		time.append(current_time)	
		current_time = current_time + time_step	
		i = i + 1
	
	MEDIUM_SIZE = 12

	# Einstellen der Beschriftungen
	plt.rc('font', size=MEDIUM_SIZE)
	plt.rc('axes', titlesize=MEDIUM_SIZE)
	plt.rc('axes', labelsize=MEDIUM_SIZE)
	plt.rc('xtick', labelsize=MEDIUM_SIZE)
	plt.rc('ytick', labelsize=MEDIUM_SIZE)
	plt.rc('legend', fontsize=MEDIUM_SIZE)
	plt.rc('figure', titlesize=MEDIUM_SIZE)

	plt.figure()
	plt.xlabel('time')

	plt.ylabel('Lenkwinkel [°]')
	plt.title('Lenkwinkel mit begrenzer Lenkgeschwindigkeit')
	plt.plot(time, steer_angle_data, 'b')
	plt.legend(['Lenwinkel'])
	plt.plot(time, steer_target, 'g..')
	plt.legend(['Ziel-Lenkwinkel'])

# Plotten der Fahrspur des Trucks UND des Trailers zur Evaluierung des Knickwinkelreglers
def plot_trajectory_reichenegger(truck_posX_data, truck_posY_data, trailer_posX_data, trailer_posY_data, straight_trajectory_posX, straight_trajectory_posY, parking_lot_trajectory_posX, parking_lot_trajectory_posY, circle_first_posX, circle_first_posY, switch_circle_posX, switch_circle_posY, circle_second_posX, circle_second_posY):
	
	MEDIUM_SIZE = 12

	# Einstellen der Beschriftungen
	plt.rc('font', size=MEDIUM_SIZE)
	plt.rc('axes', titlesize=MEDIUM_SIZE)
	plt.rc('axes', labelsize=MEDIUM_SIZE)
	plt.rc('xtick', labelsize=MEDIUM_SIZE)
	plt.rc('ytick', labelsize=MEDIUM_SIZE)
	plt.rc('legend', fontsize=MEDIUM_SIZE)
	plt.rc('figure', titlesize=MEDIUM_SIZE)

	plt.figure()

	plt.xlabel('x Position [m]')
	plt.ylabel('y Position [m]')
	
	plt.title('Gefahrener Weg der Truck- & Trailer-Hinterachse im Vergleich zur berechneten Trajektorie')

	plt.plot(truck_posX_data, truck_posY_data, color = 'orange', linestyle=(0, (3, 1,1,1,1,1)), label='Hinterachse Truck')
	plt.plot(trailer_posX_data, trailer_posY_data, color = 'red', label='Hinterachse Trailer')
	
	plt.legend()

	plt.show()

