import math
import time

from Ros_Handle_Topics import subs
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data

# Klasse, welche die Teilmanöver für das seitliche Parken mit Anhänger beinhaltet
# Teilmanöver benötigen die Klasse der Subscriber (Zugriff auf Live-Daten des Simulationsprogramms zu kommen), die Klasse der Trajektorie (Zugriff auf Trajektorie-Parameter & -funktionen) und die Klasse des Fahrzeuges (Zugriff auf Fahrzeugparameter & -funktionen)

class Maneuver_Parts_Trailer:

	# Konstrukor
	def __init__(self):
		# Werden Verwendet um innerhalb des Hauptdokuments die Teilmanöver durchzuwechseln
		self.state_park_prep = True
		self.state_roll_out = False
		self.state_roll_on = False
		self.state_circle_first = False
		self.state_switch_circle = False
		self.state_circle_second = False
		self.state_park_straighten = False
		self.state_park_finished = False
		



	# Teilmanöver für paralleles Vorbeifahren an der Parklücke
	def maneuver_park_prep(self, subscriber: subs, vd: Vehicle_Data, td: Trajectory_Data):
	
		# Vorwärtsfahren
		drive_mode = 1

		# Anfang des ersten Fahrzeugs der Parklücke
		if subscriber.get_side_distance() > 0.0 and td.vehicle1_is_detected == False:
			td.vehicle1_is_detected = True
		
		# Ende des ersten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeug nötig für die Parklückenberechnung
		if subscriber.get_side_distance() == 0.0 and td.vehicle1_is_detected == True:

			# Zwischenspeichern der Zeit für Weg Messung
			td.time_driving_distance = time.time()

			td.vehicle1_is_detected = False
			td.vehicle1_has_been_detected = True

			td.d_parkinglot = subscriber.get_side_distance()
	
		

		# Anfang des zweiten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeug nötig für die Parklückenberechnung
		if subscriber.get_side_distance() > 0.0 and td.vehicle2_is_detected == False and td.vehicle1_has_been_detected == True:

			td.vehicle2_is_detected = True	

			# Zwischenspeichern der Radumdrehung für Weg Messung
			deltaT = time.time() - td.time_driving_distance
			td.time_driving_distance = time.time() # Reset der Zeit
			td.l_park = deltaT * subscriber.get_velocity()

			td.d_vehicle2 = subscriber.get_side_distance()	



		# Endpunkt des Vorwärtsfahren wird erreicht (Truck fährt nach vorne, bis Hinterachse exakt orthogonal zu Anfang zweiten Fahrzeug ist und dann nochmals weiter um den seitlichen Abstand zum Fahrzeug) --> Beenden des Maneuvers
		if (subscriber.get_velocity() * (time.time() - td.time_driving_distance)) > (td.d_vehicle2 * td.s_drive_forward_factor + vd.l_trailer - vd.l_hitch) and td.d_vehicle2 > 0.0:
			
			# Berechnung der Einpark-Trajektorie
			td.calculate_trajectory_data(vd)

			# Beenden des Maneuvers
			self.state_park_prep = False
			self.state_roll_out = True

			# Reset Zeit
			td.time_driving_distance = time.time()

			# Speichern des Hitch Winkels für die Hitch Winkel Regelung
			vd.save_hitch_angle(subscriber.get_hitch_angle())

		return drive_mode

	# Teilmanöver: Ausrollen der Vorwärtsfahrt und rückwärts anrollen
	def maneuver_park_roll(self, subscriber: subs, vd: Vehicle_Data, td: Trajectory_Data, distance_to_trajectory):

		# Anhalten
		if self.state_roll_out == True:
			drive_mode = 2
		
		# Fahrzeug ist angehalten angehalten, Speichern der ausgerollten Distanz
		if subscriber.get_velocity() < 0.05 and self.state_roll_on == False:
			drive_mode = 3
			td.roll_out_distance += subscriber.get_velocity() * (time.time() - td.time_driving_distance)
			td.time_driving_distance = time.time()
			self.state_roll_out = False
			self.state_roll_on = True
			driven_roll_on_distance = 0

			# Speichern des anfänglichen Knickwinkels für die Knickwinkelegelung
			vd.save_hitch_angle(subscriber.get_hitch_angle())

			
		# Rückwärtsanfahren
		if self.state_roll_on == True:
			drive_mode = 3
			driven_roll_on_distance += (subscriber.get_velocity() * (time.time() - td.time_driving_distance))
			td.time_driving_distance = time.time()

		 

		# Einpark-Trajektorie beginnt nach Rückfahren der ausgerollten Distanz, dabei fährt das Gespann noch ein Stück gerade
		if self.state_roll_on == True and td.roll_out_distance + (td.l_straight / 2) < driven_roll_on_distance:
			
			drive_mode = 3
			
			self.driven_distance = 0
			td.time_driving_distance = time.time()

			self.state_roll_on = False
			self.state_circle_first = True

			# Speichern des Hitch Winkels für die Hitch Winkel Regelung
			vd.save_hitch_angle(subscriber.get_hitch_angle())

			# Berechnung der Kreismittelpunkte der Einparktrajektorie für Trajektorie-Regelung
			td.calc_r_trajectory_centers(vd.get_trailer_axis_posX(), vd.get_trailer_axis_posY(), distance_to_trajectory)

			# Berechnung der Switching Trajektorie
			td.calc_switching_trajectory()

			# Berechnung der Trajektorie der Parklücke
			td.calc_parking_lot_trajectory(distance_to_trajectory)
				
		return drive_mode


	# Teilmanöver: Erster Kreisbogen
	def maneuver_circle_first(self, subscriber: subs, vd: Vehicle_Data, td: Trajectory_Data):
		
		# Rückwärtsfahren
		drive_mode = 3

		self.driven_distance += (subscriber.get_velocity() * (time.time() - td.time_driving_distance))
		td.time_driving_distance = time.time()

		# Beenden des Teilmanövers, wenn Ende des ersten Kreisbogens erreicht wurde
		if td.bgl_trajectory_park_1 < self.driven_distance:

			self.driven_distance = 0

			self.state_circle_first = False
			self.state_switch_circle = True

			# Speichern des Hitch Winkels für die Hitch Winkel Regelung
			vd.save_hitch_angle(subscriber.get_hitch_angle())
		
		return drive_mode


	# Teilmanöver: gerades Stück zwischen beiden Kreisbögen
	def maneuver_switch_circle(self, subscriber: subs, vd: Vehicle_Data, td: Trajectory_Data):
		
		# Rückwärtsfahren
		drive_mode = 3

		self.driven_distance += (subscriber.get_velocity() * (time.time() - td.time_driving_distance))
		td.time_driving_distance = time.time()

		# Beenden des Teilmanövers, wenn Ende des geraden Stücks erreicht wurde
		if (td.l_straight) < self.driven_distance:
			self.driven_distance = 0
			self.state_switch_circle = False
			self.state_circle_second = True

			# Speichern des Hitch Winkels für die Hitch Winkel Regelung
			vd.save_hitch_angle(subscriber.get_hitch_angle())

		return drive_mode

	
	# Teilmanöver: Zweiter Kreisbogen
	def maneuver_circle_second(self, subscriber: subs, vd: Vehicle_Data, td: Trajectory_Data):
		
		# Rückwärtsfahren
		drive_mode = 3

		self.driven_distance += (subscriber.get_velocity() * (time.time() - td.time_driving_distance))
		td.time_driving_distance = time.time()
		
		# Beenden des Teilmanövers, wenn Ende des zweiten Kreisbogens erreicht wurde
		if td.bgl_trajectory_park_2 < self.driven_distance:
			self.driven_distance = 0
			self.state_circle_second = False
			self.state_park_straighten = True

			# Speichern des Hitch Winkels für die Hitch Winkel Regelung
			vd.save_hitch_angle(subscriber.get_hitch_angle())

		return drive_mode


	# Kurzes Teil-Manöver zum gerade Ziehen / zum Gespann "ent-knicken" innerhalb der Parklücke
	def maneuver_park_straighten(self, subscriber: subs, vd: Vehicle_Data, td: Trajectory_Data):

		# Rückwärtsfahren
		drive_mode = 3

		self.driven_distance += (subscriber.get_velocity() * (time.time() - td.time_driving_distance))
		td.time_driving_distance = time.time()

		# Beenden des Parkens, wenn Endpunkt erreicht
		if ((td.l_straight / 2)) < self.driven_distance:
			self.state_park_straighten = False
			self.state_park_finished = True
			drive_mode = 2
		
		return drive_mode

