import math

# Klasse, welche die Teilmanöver für das seitliche Parken des Trucks (ohne Anhänger) beinhaltet

# Teilmanöver benötigen die Klasse der Subscriber (Zugriff auf Live-Daten des Simulationsprogramms zu kommen), die Klasse der Trajektorie (Zugriff auf Trajektorie-Parameter & -funktionen) und die Klasse des Fahrzeuges (Zugriff auf Fahrzeugparameter & -funktionen)

# Ergänzung Parken mit Anhalten zum Umlenken: Zusätzlich zum eigentlichen Parkmanöver, wurde bereits versucht ein Anhalten in kritischen Punkten zum Umlenken (aufgrund begrenzter Lenkwinkelgeschwindigkeit) zu implementieren --> Hier am Übergang zwischen erstem und zweitem Kreisbogen
# --> Funktioniert noch nicht hundertprozentig (erfolgreich bei 2 und 3 km/h rückwärts), da Probleme mit der Bremsdistanz --> Fahrzeug hält zu früh an oder auch gar nicht (bei 6 km/h rückwärts)

# Deswegen für das Parken erstmal ohne Anhalten (drive_with_stop = False)

class Maneuver_Parts_Truck:

	# Konstrukor
	def __init__(self):
		# Ergänzung Parken mit Anhalten zum Umlenken (Wenn True)	
		self.drive_with_stop = True
		self.state_park_prep = True
		self.state_roll_out = False
		self.state_roll_on = False
		self.state_circle_first = False
		# Ergänzung Parken mit Anhalten zum Umlenken --> Status für Dauer des Umlenkens zwischen erstem und zweiten Kreisbogen
		self.state_circle_brake = False
		self.state_circle_second = False
		self.state_stand_still = False
		self.state_finished = False
		


	# Teilmanöver für paralleles Vorbeifahren an der Parklücke
	def maneuver_park_prep(self, subscriber, vd, td):
	
		# Vorwärtsfahren
		drive_mode = 1

		# Anfang des ersten Fahrzeugs der Parklücke
		if subscriber.get_side_truck_distance() > 0.0 and td.vehicle1_is_detected == False and td.current_roll_angle_1 == 0.0:
			td.vehicle1_is_detected = True
	
		# Ende des ersten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeug nötig für die Parklückenberechnung
		if subscriber.get_side_truck_distance() == 0.0 and td.vehicle1_is_detected == True and td.current_roll_angle_1 == 0.0:

			# Zwischenspeichern der Radumdrehung für Weg Messung
			td.current_roll_angle_1 = subscriber.get_roll_angle()
		
			td.vehicle1_is_detected = False

		# Distanz Messen zu ersten Fahrzeug --> Überschreibt sich selbst, bis letzter Punkt vom ersten Fahrzeug registriert ist
		if td.vehicle1_is_detected == True:
			td.d_point_1 = subscriber.get_side_truck_distance()



		# Anfang des zweiten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeug nötig für die Parklückenberechnung
		if subscriber.get_side_truck_distance() > 0.0 and td.vehicle2_is_detected == False and td.current_roll_angle_1 > 0.0:
			td.vehicle2_is_detected = True		
			td.d_point_2 = subscriber.get_side_truck_distance()

			# Weg der noch zurückgelegt wurde, obwohl Sensor schon Parklücke überquert hat
			td.s_1 = td.d_point_1 * math.tan(math.radians(vd.sensor_angle) * 0.5)

			# Weg der noch zurückgelegt werden muss bis Sensor exat orthogonal von Anfang zweites Fahrzeug
			td.s_2 = td.d_point_2 * math.tan(math.radians(vd.sensor_angle) * 0.5)

			# Zwischenspeichern der Radumdrehung für Weg Messung
			td.current_roll_angle_2 = subscriber.get_roll_angle()
			td.drive_distance_parking_lot = vd.calc_drive_distance(td.current_roll_angle_1, td.current_roll_angle_2, vd.r_tire)


		#Sensor exat orthogonal zu Anfang zweites Fahrzeug
		if vd.calc_drive_distance(td.current_roll_angle_2, subscriber.get_roll_angle(), vd.r_tire) > td.s_2 and td.d_point_2 > 0.0 and td.d_point_3 == 0.0:
			td.d_point_3 = subscriber.get_side_truck_distance()

			# Zwischenspeichern der Radumdrehung für Weg Messung
			td.current_roll_angle_3 = subscriber.get_roll_angle()

		# Endpunkt des Vorwärtsfahren wird erreicht --> Beenden des Maneuvers
		if vd.calc_drive_distance(td.current_roll_angle_3, subscriber.get_roll_angle(), vd.r_tire) > td.d_point_3 * td.s_drive_forward_factor and td.d_point_3 > 0.0:
		
			# Berechnung der Einpark-Trajektorie
			td.calculate_trajectory_data(vd)

			td.current_roll_angle_4 = subscriber.get_roll_angle()		

			# Beenden des Maneuvers
			self.state_park_prep = False
			self.state_roll_out = True

		return drive_mode


	# Teilmanöver: Ausrollen der Vorwärtsfahrt und rückwärts anrollen
	def maneuver_park_roll(self, subscriber, vd, td, distance_to_trajectory):
	
		# Anhalten
		if self.state_roll_out == True:
			drive_mode = 2		

		# Fahrzeug ist angehalten angehalten, Speichern der ausgerollten Distanz
		if subscriber.get_velocity() < 0.05 and self.state_roll_on == False:
			drive_mode = 3
			td.current_roll_angle_5 = subscriber.get_roll_angle()
			td.roll_out_distance = vd.calc_drive_distance(td.current_roll_angle_4, subscriber.get_roll_angle(), vd.r_tire)
			self.state_roll_out = False
			self.state_roll_on = True
			
		# Rückwärtsanfahren
		if self.state_roll_on == True:
			drive_mode = 3
		
		# Einpark-Trajektorie beginnt nach Rückfahren der ausgerollten Distanz, dabei fährt der Truck noch ein Stück gerade	
		if self.state_roll_on == True and td.roll_out_distance < vd.calc_drive_distance(td.current_roll_angle_5, subscriber.get_roll_angle(), vd.r_tire):
			drive_mode = 3
			td.current_roll_angle_6 = subscriber.get_roll_angle()
			self.state_roll_on = False
			self.state_circle_first = True

			# Berechnung der Kreismittelpunkte der Einparktrajektorie für Trajektorie-Regelung
			td.calc_r_trajectory_centers(subscriber.get_rear_axis_posX(), subscriber.get_rear_axis_posY(), distance_to_trajectory)

			# Berechnung der Trajektorie der Parklücke
			td.calc_parking_lot_trajectory(distance_to_trajectory)
							
		return drive_mode

	
	# Teilmanöver: Erster Kreisbogen
	def maneuver_circle_first(self, subscriber, vd, td, velocity_backward):
		
		# Rückwärtsfahren
		drive_mode = 3

		# Ergänzung Parken mit Anhalten zum Umlenken: Anhalten am Ende des ersten Kreisbogens
		if (self.drive_with_stop == True):
		
			# Berechnung der Bremsdistanz: Verzögerung mit 1.0 m/s² beanschlagt
			braking_distance = 0.5 * (velocity_backward ** 2) / 1.0
			
			# Anhalten, wenn Bremsdistanz gleich groß / kleiner ist als der Restweg von Kreisbogen 1
			if td.bgl_trajectory_park_1 - vd.calc_drive_distance(td.current_roll_angle_6, subscriber.get_roll_angle(), vd.r_tire) < braking_distance:
				drive_mode = 2

			# Übergang ins nächste Teil-Manöver
			if (drive_mode == 2 and subscriber.get_velocity() < 0.01):
				td.current_roll_angle_7 = subscriber.get_roll_angle()
				self.state_circle_first = False
				self.state_circle_brake = True
				self.state_circle_second = True

		# erste Kreisfahrt beim Standard-Parken ohne Anhalten zum Umlenken
		else:
			if td.bgl_trajectory_park_1 <= vd.calc_drive_distance(td.current_roll_angle_6, subscriber.get_roll_angle(), vd.r_tire):			
				td.current_roll_angle_7 = subscriber.get_roll_angle()
				self.state_circle_first = False
				self.state_circle_second = True	
			

		return drive_mode


	# Teilmanöver: Zweiter Kreisbogen
	def maneuver_circle_second(self, subscriber, vd, td, steer_target, velocity_backward):
		
		# Rückwärtsfahren
		drive_mode = 3

		# Ergänzung Parken mit Anhalten zum Umlenken: Anfahren nach Umlenkem zu Beginn des zweiten Kreisbogen (Umlenken erfolgt durch Befehle in Hauptdatei)
		if (self.drive_with_stop == True):
		
			# Umlenken beendet, dann Weiterfahren
			if ((subscriber.get_steering_angle() - steer_target) < 1.0) and self.state_circle_brake is True:
				self.state_circle_brake = False

			# Anhalten, solande noch Umgelenkt wird
			if (self.state_circle_brake is True):
				drive_mode = 2
			
			# Berechnung der Bremsdistanz bei aktueller Fahrt
			braking_distance = 0.5 * (velocity_backward ** 2) / 1.0

			# Anhalten zum Ende des zweiten Kreisbogens --> Übergang in nächste Teilmanöver / bzw. ins Ende des Parkens
			if td.bgl_trajectory_park_2 - vd.calc_drive_distance(td.current_roll_angle_7, subscriber.get_roll_angle(), vd.r_tire) < braking_distance:
				drive_mode = 2
				self.state_circle_second = False
				self.state_stand_still = True

		# zweite Kreisfahrt beim Standard-Parken ohne Anhalten zum Umlenken
		else:
			
			if td.bgl_trajectory_park_2 <= vd.calc_drive_distance(td.current_roll_angle_7, subscriber.get_roll_angle(), vd.r_tire):			
				td.current_roll_angle_8 = subscriber.get_roll_angle()
				self.state_circle_second = False
				self.state_stand_still = True
		return drive_mode


	# Kurzes Teil-Manöver am Ende --> Anhalten und Beenden des Parkmanövers
	def maneuver_stand_still(self, subscriber):
		
		drive_mode = 2

		if(subscriber.get_velocity() < 0.001):
			self.state_stand_still = False
			self.state_finished = True

		return drive_mode

