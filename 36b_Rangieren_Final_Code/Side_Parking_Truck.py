import time
import roslibpy
import math
import numpy

from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data_Truck
from Trajectory_Regulator import Trajectory_Regulator
from Trajectory_Regulator import Trajectory_Regulator_Velocity_Dependent
from Data_Plot import plot_PID_regulator
from Data_Plot import plot_trajectory
from Maneuver_Parts_Truck import Maneuver_Parts_Truck

# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz Trajektorie Berechnung
myTrajectory = Trajectory_Data_Truck()

# Instanz Manöver
myMParts = Maneuver_Parts_Truck()

# Instanz des Reglers um auf Trajektorie zu Fahren
# Regelparameter alle zu null, da die Regelparameter aufgrund der Geschwindigkeitsabhänigen Kennlinien innerhalb des Reglers berechnet werden
straight_PID = Trajectory_Regulator_Velocity_Dependent(0, 0, 0)
straight_PID_back = Trajectory_Regulator_Velocity_Dependent(0, 0, 0)
curved_PID_first = Trajectory_Regulator_Velocity_Dependent(0, 0, 0)
curved_PID_second = Trajectory_Regulator_Velocity_Dependent(0, 0, 0)

# ungefähre Zykluszeit mit dem die Loop durchgeführt wird in Sekunden --> In IPG Truckmaker 20 Hz eingestellt
time_step = 0.05

# Geschwindigkeit mit dem das Manöver durchgeführt werden soll
velocity_forward = 10.0
velocity_backward = 2.0

# Initiale Werte, die gepublished werden
pub_velocity = velocity_forward
pub_drive_mode = 2
pub_steering = 0.0

# Container zum Speichern von Lenkwinkel
steer_data_straight_front = []
steer_data_straight_back = []
steer_data_first_circle = []
steer_data_second_circle = []

# Starten des Ros-Clients
myClient.start_client()

# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()

# Warten bis Ros-Kommunikation vollständig da ist
mySubs.wait_for_signals()

# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myTrajectory.calc_trajectory_straight(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())

# Main Loop
while(myMParts.state_finished == False):
	
	# Warten, bis ROS neuer Simulationstakt sendet
	mySubs.wait_for_signals()

	# Teilmanöver: Vorbeifahren an Parklücke
	if (myMParts.state_park_prep == True):
		
		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_drive_mode = myMParts.maneuver_park_prep(mySubs, myVehicle, myTrajectory)

		# Regelung zum Fahren auf gerader Trajektorie
		calculated_steering_angle = straight_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step, mySubs.get_velocity(), pub_drive_mode)

		# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())

		steer_data_straight_front.append(pub_steering)


	# Teilmanöver: Aus- und wieder Anrollen
	if (myMParts.state_roll_out == True or myMParts.state_roll_on == True):
	
		# Anhalten
		if (myMParts.state_roll_out == True):
			pub_velocity = 0.0
			# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
			pub_drive_mode = myMParts.maneuver_park_roll(mySubs, myVehicle, myTrajectory, straight_PID.offset_prev)

			# Regelung zum Fahren auf gerader Trajektorie
			calculated_steering_angle = straight_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step, mySubs.get_velocity(), pub_drive_mode)

			# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
			pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())

			steer_data_straight_front.append(pub_steering)
		
		# Wieder Anfahren, nun Rückwärts
		if (myMParts.state_roll_on == True):
			pub_velocity = velocity_backward
			# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
			pub_drive_mode = myMParts.maneuver_park_roll(mySubs, myVehicle, myTrajectory, straight_PID.offset_prev)

			# Regelung zum Fahren auf gerader Trajektorie
			calculated_steering_angle = straight_PID_back.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step, mySubs.get_velocity(), pub_drive_mode)

			# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
			pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())

			steer_data_straight_back.append(pub_steering)


	# Teilmanöver: erster Kreis zum Parken
	if (myMParts.state_circle_first == True):
		# Lenkwinkel, welcher theoretisch für den Kreis gefahren werden muss
		steer_target = - myVehicle.calc_radius_to_steer(myTrajectory.r_trajectory)

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_drive_mode = myMParts.maneuver_circle_first(mySubs, myVehicle, myTrajectory, mySubs.get_velocity())
		pub_velocity = velocity_backward

		# Regelung des Lenkwinkels um auf tatsächlichem Kreis zu fahren --> Um den errechneten Lenkwinkel herum regeln
		calculated_steering_angle = curved_PID_first.regulate_steering_curved (myTrajectory.circle_center_first, mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), steer_target, myTrajectory.r_trajectory, time_step, mySubs.get_velocity(), pub_drive_mode)
		
		# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())

		steer_data_first_circle.append(pub_steering)


	# Teilmanöver: zweiter Kreis zum Parken
	if (myMParts.state_circle_second == True):

		# Lenkwinkel, welcher theoretisch für den Kreis gefahren werden muss
		steer_target = myVehicle.calc_radius_to_steer(myTrajectory.r_trajectory)

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_drive_mode = myMParts.maneuver_circle_second(mySubs, myVehicle, myTrajectory, steer_target, mySubs.get_velocity())
		pub_velocity = velocity_backward

		# Regelung des Lenkwinkels um auf tatsächlichem Kreis zu fahren --> Um den errechneten Lenkwinkel herum regeln
		calculated_steering_angle = curved_PID_second.regulate_steering_curved (myTrajectory.circle_center_second, mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), steer_target, myTrajectory.r_trajectory, time_step, mySubs.get_velocity(), pub_drive_mode)

		# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())

		steer_data_second_circle.append(pub_steering)

	# Anhalten in der Parklücke und dann Beenden des Manövers
	if(myMParts.state_stand_still == True):
		# Manöverteil zum Anhalten
		pub_drive_mode = myMParts.maneuver_stand_still(mySubs)

		# Lenkung gerade Ausrichten
		pub_velocity = 0.0
		steer_target = 0.0

		# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), steer_target, mySubs.get_steering_angle())
		

	# Publishen der gewünschten Fahrbefehle
	myPub.pub_velc_stc_dmc(pub_velocity, pub_steering, pub_drive_mode)

	# Speichern der Positionsdaten für spätere Auswertung und Analysis
	myVehicle.save_position_data(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())


# Plots der Regler zur Auswertung
plot_PID_regulator(steer_data_straight_front, straight_PID.offset_data, time_step, straight_PID.k_p, straight_PID.k_i, straight_PID.k_d, "Lenk", "Park_Prep")
plot_PID_regulator(steer_data_straight_back, straight_PID_back.offset_data, time_step, straight_PID_back.k_p, straight_PID_back.k_i, straight_PID_back.k_d, "Lenk", "Roll on")
plot_PID_regulator(steer_data_first_circle, curved_PID_first.offset_data, time_step, curved_PID_first.k_p, curved_PID_first.k_i, curved_PID_first.k_d, "Lenk", "First_circle")
plot_PID_regulator(steer_data_second_circle, curved_PID_second.offset_data, time_step, curved_PID_second.k_p, curved_PID_second.k_i, curved_PID_second.k_d, "Lenk", "Second_circle")

# Berechnen der Plot-Punkte der Trajektorie für die spätere grafische Darstellung
myTrajectory.calc_straight_trajectory_data()
myTrajectory.calc_parking_lot_trajectory_data()
myTrajectory.calc_circle_first_data()
myTrajectory.calc_circle_second_data()

# Plotten der Trajektorien zur Auswertung
plot_trajectory(myVehicle.get_posX_data(), myVehicle.get_posY_data(), myTrajectory.trajectory_straight_data_posX, myTrajectory.trajectory_straight_data_posY, myTrajectory.trajectory_parking_lot_data_posX, myTrajectory.trajectory_parking_lot_data_posY, myTrajectory.circle_first_data_posX, myTrajectory.circle_first_data_posY, myTrajectory.circle_second_data_posX, myTrajectory.circle_second_data_posY)

