import math

from Vector_Operations import rotate_orientation_vector


class Trajectory_Data:

	# Indikator, welches Fahrzeug der Parklücke gerade im Blickfeld ist
	vehicle1_is_detected = False
	vehicle2_is_detected = False
	vehicle1_has_been_detected = False

	# Zwischenspeicher für die Messung über den gerollten Weg
	time_driving_distance = 0.0

	# Distanz die ausgerollt wird
	roll_out_distance = 0.0 
 
	# Distanzpunkte, nötig für Parklückenvermessung und die Park-Trajektorie
	d_vehicle2 = 0.0
	d_parkinglot = 0.0


	location_vector_straight = [] # Ortsvektor der Trajektorie die beim Vorbeifahren der Parklücke gefahren wird
	orientation_vector_straight = [] # Richtungsvektor der Trajektorie die beim Vorbeifahren der Parklücke gefahren wird
	
	circle_center_first = [] # Position des Mittelpunktes von der ersten Kreis-Trajektorie
	circle_center_second = [] # Position des Mittelpunktes von der zweiten Kreis-Trajektorie

	location_vector_parking_lot = []	# Ortsvektor der Parklücke
	orientation_vector_parking_lot = []	# Richtungssvektor der Parklücke
	
	
	l_park = 0.0 # Länge der Parklücke
	b_park = 0.0 # Tiefe der Parklücke
	d_center_truck_park = 0.0 # Abstand Truck Mitte zu Mitte der Parklückentiefe
	s_parked = 0.0  # Distanz Trailer Ende und Parklücken Ende nach dem Parkvorgang
	s_trailer_rear_park = 0.0 # Distanz Trailer Ende und Parklückenanfang vor dem Parkvorgang
	l_trajectory = 0.0 # Luftlinie zwischen Trailer Ende vor und nach dem Einparken

	s_drive_forward_factor = 0.0 # Faktor mit dem die seitliche Distanz multipliziert wird um zu Berechnen wie weit Truck nach Parklücke vorfahren muss

	a_park_rad = 0.0 # Winkel der Trajektorien-Luftline und Parklücke in rad
	r_trajectory = 0.0 # Radien der Trajektorie
	bgl_trajectory = 0.0 # Bogenlänge der Radien für die Trajektorie
	bgl_trajectory_park_1 = 0.0 # Bogenlänge der Radien für die Trajektorie am rechten Hinterrad bei einer Rechts-Kurve rückwärts
	bgl_trajectory_park_2 = 0.0 # Bogenlänge der Radien für die Trajektorie am rechten Hinterrad bei einer Links-Kurve rückwärts

	# Speicher für die Trajektorien-Daten für das Plotten der Trajektorien in der Auswertung
	trajectory_straight_data_posX = []
	trajectory_straight_data_posY = []

	trajectory_parking_lot_data_posX = []
	trajectory_parking_lot_data_posY = []

	circle_first_data_posX = []
	circle_first_data_posY = []

	circle_second_data_posX = []
	circle_second_data_posY = []


	# Berechnung der Trajektorie zum parallelen Vorbeifahren an der Parklücke
	def calc_trajectory_straight(self, hitch_posX, hitch_posY, rear_axis_posX, rear_axis_posY):
	
		# Koordinaten des Ortsvektors
		if not self.location_vector_straight:
			self.location_vector_straight.append(rear_axis_posX)
			self.location_vector_straight.append(rear_axis_posY)

		# Richtungsvektor
		if not self.orientation_vector_straight:
			self.orientation_vector_straight.append(hitch_posX - rear_axis_posX)
			self.orientation_vector_straight.append(hitch_posY - rear_axis_posY)


	# Berechnung der Mittellinie der Parklücke als Orientierung
	def calc_parking_lot_trajectory(self, offset):

		# Verbindung von Parktrajektorie und gerader Trajektorie beim Vorbeifahren an der Parklücke
		connection_vector = rotate_orientation_vector(self.orientation_vector_straight, - 90 )
		length_connection_vector = math.sqrt(connection_vector[0] ** 2 + connection_vector[1] ** 2)
		
		lv_posX = self.location_vector_straight[0] + ((self.d_center_truck_park + offset) / length_connection_vector) * connection_vector[0]
		lv_posY = self.location_vector_straight[1] + ((self.d_center_truck_park + offset) / length_connection_vector) * connection_vector[1]

	

		self.location_vector_parking_lot.append(lv_posX)
		self.location_vector_parking_lot.append(lv_posY)

		ov_posX = self.orientation_vector_straight[0]
		ov_posY = self.orientation_vector_straight[1]

		self.orientation_vector_parking_lot.append(ov_posX)
		self.orientation_vector_parking_lot.append(ov_posY)

	


	# Berechnung der einzelnen Datenpunkte des ersten Kreisbogen für die Plots in der Auswertung
	def calc_circle_first_data(self):

		i = 0

		while (i < 2 * math.degrees(self.a_park_rad)):

			vector_center_to_radian = rotate_orientation_vector(self.orientation_vector_straight, 90 + i)
			length_vector_cr = math.sqrt(vector_center_to_radian[0] ** 2 + vector_center_to_radian[1] ** 2)

			posX = self.circle_center_first[0] + (self.r_trajectory / length_vector_cr) * vector_center_to_radian[0]
			posY = self.circle_center_first[1] + (self.r_trajectory / length_vector_cr) * vector_center_to_radian[1]

			self.circle_first_data_posX.append(posX)
			self.circle_first_data_posY.append(posY)

			# Schrittweite mit der später geplotted wird 
			i = i + 0.5


	# Berechnung der einzelnen Datenpunkte des ersten Kreisbogen für die Plots in der Auswertung
	def calc_circle_second_data(self):

		i = 0

		while (i < 2 * math.degrees(self.a_park_rad)):

			vector_center_to_radian = rotate_orientation_vector(self.orientation_vector_straight, - 90 + i)
			length_vector_cr = math.sqrt(vector_center_to_radian[0] ** 2 + vector_center_to_radian[1] ** 2)

			posX = self.circle_center_second[0] + (self.r_trajectory / length_vector_cr) * vector_center_to_radian[0]
			posY = self.circle_center_second[1] + (self.r_trajectory / length_vector_cr) * vector_center_to_radian[1]

			self.circle_second_data_posX.append(posX)
			self.circle_second_data_posY.append(posY)

			# Schrittweite mit der später geplotted wird 
			i = i + 0.5
			




# Klasse für die Datenspeicherung und -berechnung für die Trajektorie des Truck-Manövers 
class Trajectory_Data_Truck(Trajectory_Data):

	# Konstrukor
	def __init__(self):
		self.s_drive_forward_factor = 1.25 # Faktor mit dem die seitliche Distanz multipliziert wird um zu Berechnen wie weit Truck nach Parklücke vorfahren muss

		# Statische Vorgabe der Parklückengröße
		self.l_park = 10		
		self.b_park = 2.5
	
	# Berechnung der Trajectorie und dafür erforderliche Parameter
	def calculate_trajectory_data(self, vehicle_data):

		# Parklückenvermessung hier nicht verwendet, da mit statischen (genaueren) Angaben (siehe oben) gerechnet wird --> keine Beeinflussung des Manövers durch ungenaue Vermessung

		#self.b_park = self.d_vehicle2 - self.d_parkinglot # Breite der Parklücke

		self.d_center_truck_park = vehicle_data.b_truck * 0.5 + self.d_parkinglot + self.b_park * 0.5 

		self.s_parked = (self.l_park - vehicle_data.l_complete_truck) * 0.1

		self.s_trailer_rear_park = self.s_drive_forward_factor * self.d_parkinglot 

		self.l_trajectory = math.sqrt(self.d_center_truck_park ** 2 + (self.s_trailer_rear_park + self.l_park - self.s_parked - vehicle_data.l_truck_axis_to_end) ** 2) 

		self.a_park_rad = math.atan(self.d_center_truck_park / (self.s_trailer_rear_park + self.l_park - self.s_parked - vehicle_data.l_truck_axis_to_end)) 

		self.r_trajectory = (self.l_trajectory / 4) * 1 / math.cos((math.pi / 2) - self.a_park_rad) 

		self.bgl_trajectory = self.r_trajectory * 2 * self.a_park_rad  

		self.bgl_trajectory_park_1 = (self.r_trajectory - vehicle_data.d_tire_center) * 2 * self.a_park_rad

		self.bgl_trajectory_park_2 = (self.r_trajectory + vehicle_data.d_tire_center) * 2 * self.a_park_rad

		print("Breite Park: ", self.b_park, "  Länge Park: ", self.l_park, "  Radius: ", self.r_trajectory, " D3 :", self.d_parkinglot)




	# Berechnung der Kreismittelpunkte der Einpark Trajektorie
	def calc_r_trajectory_centers(self, rear_axis_posX, rear_axis_posY, distance):
	
		center_posX = 0.0
		center_posY = 0.0
	
		# Berechnung des Kreismittelpunktes zum ersten Einpark-Manöver-Teil (rechtskurve)
		
		# Drehung des Richtungsvektors der parallelen Trajektorie um 90°, um dazu orthogonalen Richtungsvektor zu erzeugen, da Kreismittelpunkt senkrecht auf der parallelen Trajektorie steht
		rotated_orientation_vector = rotate_orientation_vector(self.orientation_vector_straight, -90)

		# Länge des rotierten Richtungsvektor: Wird für Umrechnung auf Einheitsvektor benötigt
		length_rotated_ov = math.sqrt(rotated_orientation_vector[0] ** 2 + rotated_orientation_vector[1] ** 2)

		# Entfernung von Hinterachse zu Kreismittelpunkt
		d_truck_r_trajectory_center = (self.r_trajectory - distance)

		center_posX = rear_axis_posX + (d_truck_r_trajectory_center / (length_rotated_ov)) * rotated_orientation_vector[0]
		center_posY = rear_axis_posY + (d_truck_r_trajectory_center / (length_rotated_ov)) * rotated_orientation_vector[1]

		if not self.circle_center_first:
			self.circle_center_first.append(center_posX)
			self.circle_center_first.append(center_posY)


		# Berechnung des Kreismittelpunktes zum zweiten Einpark-Manöver-Teil (linkskurve)

		# Drehung des Richtungsvektors der parallelen Trajektorie um 90°, um orthogonalen Richtungsvektor zu erzeugen
		connection_vector_both_centers = rotate_orientation_vector(self.orientation_vector_straight, 90 + 2 * math.degrees(self.a_park_rad))
		
		# Länge des rotierten Richtungsvektor: Wird für Umrechnung auf Einheitsvektor benötigt
		length_connection_vector = math.sqrt(connection_vector_both_centers[0] ** 2 + connection_vector_both_centers[1] ** 2)

		# Distanz zwischen den beiden Zentren der Kreismittelpunkte
		d_centers = self.r_trajectory * 2
	
		center_posX = self.circle_center_first[0] + (d_centers / (length_connection_vector)) * connection_vector_both_centers[0]
		center_posY = self.circle_center_first[1] + (d_centers / (length_connection_vector)) * connection_vector_both_centers[1]
		
		if not self.circle_center_second:
			self.circle_center_second.append(center_posX)
			self.circle_center_second.append(center_posY)



	# Berechnung der Datenpunkt der geraden Trajektorie für die Plots in der Auswertung
	def calc_straight_trajectory_data(self):
		length_ov = math.sqrt(self.orientation_vector_straight[0] ** 2 + self.orientation_vector_straight[1] ** 2)
		i = 0
		 

		# Da nur die Orientierung in der Darstellung eine Rolle spielt --> Wird hier keine exakte, aber eine ausreichend große Länge geplotted (z.B. 15m) 
		while (i < 15):
			posX = self.location_vector_straight[0] + ( i / length_ov) * self.orientation_vector_straight[0]
			posY = self.location_vector_straight[1] + ( i / length_ov) * self.orientation_vector_straight[1]
			
			

			self.trajectory_straight_data_posX.append(posX)
			self.trajectory_straight_data_posY.append(posY)

			# Schrittweite mit der später geplotted wird
			i = i + 0.05
	
	# Berechnung der Datenpunkt der Mittellinie der Parklücke für die Plots in der Auswertung
	def calc_parking_lot_trajectory_data(self):
		length_ov = math.sqrt(self.orientation_vector_parking_lot[0] ** 2 + self.orientation_vector_parking_lot[1] ** 2)
		i = 0

		while (i < 10):
			posX = 91 + i
			posY = 15
			

			self.trajectory_parking_lot_data_posX.append(posX)
			self.trajectory_parking_lot_data_posY.append(posY)

			# Schrittweite mit der später geplotted wird
			i = i + 0.05	



# Klasse für die Datenspeicherung und -berechnung für die Trajektorie des Trailer-Manövers 
class Trajectory_Data_Trailer(Trajectory_Data):
	# Konstrukor
	def __init__(self):
		
		self.l_straight = 6

		# Beginn Trajektorie zum Wechseln von Kreis 1 zu Kreis 2
		self.location_vector_switch_circle = []
		# Richtung Trajektorie zum Wechseln von Kreis 1 zu Kreis 2
		self.orientation_vector_switch_circle = []

		# Datenspeicher für Koordinaten für das gerade Stück zwischen den beiden Kreisbögen
		self.switch_circle_data_posX = []
		self.switch_circle_data_posY = []

		self.s_drive_forward_factor = 3.0  # Faktor mit dem die seitliche Distanz multipliziert wird um zu Berechnen wie weit Truck nach Parklücke vorfahren muss

		# Statische Vorgabe der Parklückengröße
		self.l_park = 23.5			
		self.b_park = 2.5


		


	# Berechnung der Trajectorie und dafür erforderliche Parameter
	def calculate_trajectory_data(self, vehicle_data):

		# Parklückenvermessung hier nicht verwendet, da mit statischen (genaueren) Angaben (siehe oben) gerechnet wird --> keine Beeinflussung des Manövers durch ungenaue Vermessung

		#self.l_park = self.drive_distance_parking_lot # Länge der Parklücke

		#self.b_park = self.d_vehicle2 - self.d_parkinglot # Breite der Parklücke

		self.d_center_truck_park = (vehicle_data.b_truck * 0.5) + self.d_parkinglot + (self.b_park * 0.5) 

		self.s_parked = (self.l_park - vehicle_data.l_complete) * 0.5

		self.s_trailer_rear_park = self.s_drive_forward_factor * self.d_parkinglot 

		self.a_park_rad = math.atan(self.d_center_truck_park / (self.s_trailer_rear_park + self.l_park - self.s_parked - vehicle_data.l_trailer_axis_to_end)) 

		x = self.s_trailer_rear_park + self.l_park - self.s_parked - vehicle_data.l_trailer_axis_to_end
		y = self.d_center_truck_park

		delta_X = math.cos(2 * self.a_park_rad) * (self.l_straight / 2)
		delta_Y = math.sin(2 * self.a_park_rad) * (self.l_straight / 2)

		x_new = (x / 2) - delta_X - (self.l_straight / 2)
		y_new = (y / 2) - delta_Y

		self.l_trajectory = math.sqrt(x_new ** 2 + y_new ** 2)  

		self.r_trajectory = (self.l_trajectory / 2) * (1 / math.cos((math.pi / 2) - self.a_park_rad))

		self.bgl_trajectory = self.r_trajectory * 2 * self.a_park_rad  

		self.bgl_trajectory_park_1 = (self.r_trajectory - vehicle_data.d_tire_center) * 2 * self.a_park_rad

		self.bgl_trajectory_park_2 = (self.r_trajectory + vehicle_data.d_tire_center) * 2 * self.a_park_rad

		print("Breite Park: ", self.b_park, "  Länge Park: ", self.l_park, "  Radius: ", self.r_trajectory, " D3 :", self.d_parkinglot)



	# Berechnung der Kreismittelpunkte der Einpark Trajektorie
	def calc_r_trajectory_centers(self, rear_axis_trailer_posX, rear_axis_trailer_posY, distance):

		center_posX = 0.0

		center_posY = 0.0
	
		# Berechnung des Kreismittelpunktes zum ersten Einpark-Manöver-Teil (rechtskurve)

		length_ov = math.sqrt(self.orientation_vector_straight[0] ** 2 + self.orientation_vector_straight[1] ** 2)
		
		# Drehung des Richtungsvektors der parallelen Trajektorie um 90°, um dazu orthogonalen Richtungsvektor zu erzeugen, da Kreismittelpunkt senkrecht auf der parallelen Trajektorie steht
		rotated_orientation_vector = rotate_orientation_vector(self.orientation_vector_straight, -90)

		# Länge des rotierten Richtungsvektor: Wird für Umrechnung auf Einheitsvektor benötigt
		length_rotated_ov = math.sqrt(rotated_orientation_vector[0] ** 2 + rotated_orientation_vector[1] ** 2)

		# Entfernung von Hinterachse zu Kreismittelpunkt
		d_truck_r_trajectory_center = (self.r_trajectory - distance)
		
		# Verschiebung des Beginn des ersten Kreises um die gerade Strecke zwischen den Kreisen parallel zur Fahrt-Richtung
		start_circle_first_X = rear_axis_trailer_posX - ((self.l_straight / 2) / length_ov) * self.orientation_vector_straight[0]
		start_circle_first_Y = rear_axis_trailer_posY - ((self.l_straight / 2) / length_ov) * self.orientation_vector_straight[1]

		center_posX = start_circle_first_X + (d_truck_r_trajectory_center / (length_rotated_ov)) * rotated_orientation_vector[0]
		center_posY = start_circle_first_Y + (d_truck_r_trajectory_center / (length_rotated_ov)) * rotated_orientation_vector[1]

		if not self.circle_center_first:
			self.circle_center_first.append(center_posX)
			self.circle_center_first.append(center_posY)


		# Berechnung des Kreismittelpunktes zum zweiten Einpark-Manöver-Teil (linkskurve)

		# Drehung des Richtungsvektors der parallelen Trajektorie um 90°, um orthogonalen Richtungsvektor zu erzeugen
		connection_vector_both_centers = rotate_orientation_vector(self.orientation_vector_straight, 90 + 2 * math.degrees(self.a_park_rad))

		# Länge des rotierten Richtungsvektor: Wird für Umrechnung auf Einheitsvektor benötigt
		length_connection_vector = math.sqrt(connection_vector_both_centers[0] ** 2 + connection_vector_both_centers[1] ** 2)

		# Distanz zwischen den beiden Zentren der Kreismittelpunkte
		d_centers = self.r_trajectory * 2

		# Richtung von Mittelpunkt der geraden Strecke zwischen den Kreisen zum zweiten Kreismittelpunkt
		orientation_vector_connect = rotate_orientation_vector(connection_vector_both_centers, 90)

		length_connect_ov = math.sqrt(orientation_vector_connect[0] ** 2 + orientation_vector_connect[1] ** 2)

		# Verschiebung des ersten Kreismittelpunktes, um Beginn für Richtungsvektor zum zweiten Kreismittelpunkts zu erzeugen
		shifted_circle_first_X = self.circle_center_first[0] + (self.l_straight / length_connect_ov) * orientation_vector_connect[0]
		shifted_circle_first_Y = self.circle_center_first[1] + (self.l_straight / length_connect_ov) * orientation_vector_connect[1]
	
		center_posX = shifted_circle_first_X + (d_centers / (length_connection_vector)) * connection_vector_both_centers[0]
		center_posY = shifted_circle_first_Y + (d_centers / (length_connection_vector)) * connection_vector_both_centers[1]
		
		if not self.circle_center_second:
			self.circle_center_second.append(center_posX)
			self.circle_center_second.append(center_posY)


	# Gerade Trajektorie zum Übergang von Kreis 1 zu Kreis 2, da Zeit zum Wechseln von des Hitchwinkels / des Knickverhaltens benötigt wird
	def calc_switching_trajectory(self):

		# Verbindung von Mittelpunkt erster Kreis zu Beginn der Switching Trajektorie
		connection_vector = rotate_orientation_vector(self.orientation_vector_straight, 90 + 2 * math.degrees(self.a_park_rad))
		length_connection_vector = math.sqrt(connection_vector[0] ** 2 + connection_vector[1] ** 2)

		lv_posX = self.circle_center_first[0] + (self.r_trajectory / length_connection_vector) * connection_vector[0]
		lv_posY = self.circle_center_first[1] + (self.r_trajectory / length_connection_vector) * connection_vector[1]

		self.location_vector_switch_circle.append(lv_posX)
		self.location_vector_switch_circle.append(lv_posY)


		# Richtung der Switch Trajektorie
		orientation_vector_switch = rotate_orientation_vector(connection_vector, 90)

		ov_posX = orientation_vector_switch[0]
		ov_posY = orientation_vector_switch[1]

		self.orientation_vector_switch_circle.append(ov_posX)
		self.orientation_vector_switch_circle.append(ov_posY)

	

	# Berechnung der Datenpunkt der geraden Trajektorie beim Vorbeifahren für die Plots in der Auswertung
	def calc_straight_trajectory_data(self):
		length_ov = math.sqrt(self.orientation_vector_straight[0] ** 2 + self.orientation_vector_straight[1] ** 2)
		i = 0
		 

		# Da nur die Orientierung in der Darstellung eine Rolle spielt --> Wird hier keine exakte, aber eine ausreichend große Länge geplotted (z.B. 15m) 
		while (i < 35):
			posX = self.location_vector_straight[0] + ( i / length_ov) * self.orientation_vector_straight[0]
			posY = self.location_vector_straight[1] + ( i / length_ov) * self.orientation_vector_straight[1]
			
			

			self.trajectory_straight_data_posX.append(posX)
			self.trajectory_straight_data_posY.append(posY)

			# Schrittweite mit der später geplotted wird
			i = i + 0.05


	# Berechnung der Datenpunkt der geraden Trajektorie zwischen den Kreisbögen für die Plots in der Auswertung
	def calc_switch_circle_data(self):
		
		i = 0

		length_ov = math.sqrt(self.orientation_vector_switch_circle[0] ** 2 + self.orientation_vector_switch_circle[1] ** 2)

		while (i < self.l_straight):
			posX = self.location_vector_switch_circle[0] + ( i / length_ov) * self.orientation_vector_switch_circle[0]
			posY = self.location_vector_switch_circle[1] + ( i / length_ov) * self.orientation_vector_switch_circle[1]
			
			self.switch_circle_data_posX.append(posX)
			self.switch_circle_data_posY.append(posY)

			# Schrittweite mit der später geplotted wird
			i = i + 0.05


	# Berechnung der Datenpunkt der Mittellinie der Parklücke für die Plots in der Auswertung
	def calc_parking_lot_trajectory_data(self):
		length_ov = math.sqrt(self.orientation_vector_parking_lot[0] ** 2 + self.orientation_vector_parking_lot[1] ** 2)
		i = 0


		while (i < 23.5):
			posX = 26.5 + i
			posY = 15
			

			self.trajectory_parking_lot_data_posX.append(posX)
			self.trajectory_parking_lot_data_posY.append(posY)

			# Schrittweite mit der später geplotted wird
			i = i + 0.05

