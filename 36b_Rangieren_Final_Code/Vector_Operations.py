import math


# Drehung des Richtungsvektors um bestimmter Winkel gegen den Uhrzeigersinn
def rotate_orientation_vector(orientation_vector, rotation_angle):
	
	rotated_orientation_vector = []

	# Drehmatrix für 2 Dimensionale Vektor
	rotated_orient_vector_X = orientation_vector[0] * math.cos(math.radians(rotation_angle)) - orientation_vector[1] * math.sin(math.radians(rotation_angle))
	rotated_orient_vector_Y = orientation_vector[0] * math.sin(math.radians(rotation_angle)) + orientation_vector[1] * math.cos(math.radians(rotation_angle))
	
	
	rotated_orientation_vector.append(rotated_orient_vector_X)
	rotated_orientation_vector.append(rotated_orient_vector_Y)

	return rotated_orientation_vector
