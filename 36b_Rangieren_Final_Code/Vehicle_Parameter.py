import math

from Vector_Operations import rotate_orientation_vector


# Definition LKW Parameter
class Vehicle_Data:
	# Konstrukor
	def __init__(self):
		self.l_truck = 43 # Länge Zugmaschine --> Achse zu Achse
		self.l_hitch = 0 # Länge Hinterachse zu Auflagepunkt
		self.l_truck_axis_to_end = 12 # Länge Truck-Hinterachse zu hinterem Endpunkt des Trucks
		self.l_trailer = 99.5 # Trailer Auflagepunkt bis Achse
		self.l_complete_truck = 75 # Truck Außenpunkte Vorne-Hinten
		self.l_complete = 216.0 # Gesamtlänge Truck + Trailer
		self.l_trailer_axis_to_end = 56  # Abstand der mittleren Trailer-Achse zu Endpunkt des Trailers 
		self.b_truck = 32 # Breite Truck
		self.b_trailer = 33.5 # Breite Anhänger
		self.d_tire_center = 16.75 # Abstand Radaufhängung zu Mitte der Hinterachse
		self.r_tire = 5.5 # Roll Radius der Reifen

		self.sensor_angle = 30.0 # in Grad

		self.gear_drive = 1 # Vorwärtsgang
		self.gear_back = 3 # Rückwärtsgang
		self.gear_stop = 2 # Anahlten
		self.gear_neutral = 0 # Neutral

		self.hitch_angle_stored = 0.0
		self.trailer_axis_pos = [] # [0] = X, [1] = Y
		self.truck_orientation = []
		
		self.truck_angle_limitation = 25 # Maximaler Lenlwinkel, den der Truck an der Vorderachse einschlagen kann

		self.posX_data = []	# Listen zum Speichern der Positionsdaten z.B von Hinterachse Truck / Trailer
		self.posY_data = []	# Listen zum Speichern der Positionsdaten z.B von Hinterachse Truck / Trailer
		self.posX_data_2 = []	# Listen zum Speichern der Positionsdaten z.B von Hinterachse Truck / Trailer
		self.posY_data_2 = []	# Listen zum Speichern der Positionsdaten z.B von Hinterachse Truck / Trailer
		self.steer_angle_velocity = 25 # Grad pro Sekunde, Lenkwinkel Einschlaggeschwindigkeit
		self.current_hitch_target = 0.0 # Aktueller Hitch Target, der vom Programm vorgegeben wird
		self.prev_hitch_target = 0.0 # Hitch Target der vorherigen Iteration


	# Hinzufügen einzelner Positions-Koordinaten zum Positionsspeicher
	def save_position_data(self, posX, posY):
		self.posX_data.append(posX)
		self.posY_data.append(posY)

	# Hinzufügen einzelner Positions-Koordinaten zum Positionsspeicher
	def save_position_data_2(self, posX, posY):
		self.posX_data_2.append(posX)
		self.posY_data_2.append(posY)

	# Getter für die Positionsspeicher
	def get_posX_data(self):
		return self.posX_data

	def get_posY_data(self):
		return self.posY_data

	def get_posX_data_2(self):
		return self.posX_data_2

	def get_posY_data_2(self):
		return self.posY_data_2
		
		
	# Zwischenspeichern eines Knickwinkels
	def save_hitch_angle(self, hitch_angle):
		self.hitch_angle_stored = hitch_angle

	# Überprüfen ob neuer Knickwinkel gesetzt wird
	def check_new_hitch_target(self, hitch_target):
		self.current_hitch_target = hitch_target
		
		# Grenze, bis zu der zwei Floats als gleich betrachtet werden
		float_threshhold = 0.1
 		
		if(abs(self.current_hitch_target - self.prev_hitch_target) >= float_threshhold):
			self.prev_hitch_target = self.current_hitch_target			
			return True
		else:
			return False

	# Getter des gespeicherten Knickwinkels
	def get_stored_hitch_angle(self):
		return self.hitch_angle_stored

	
	# Berechnung der Truck-Orientierung
	def calc_truck_orientation(self, hitch_posX, hitch_posY, rear_axis_posX, rear_axis_posY):
		# Richtungsvektor
		value_X = hitch_posX - rear_axis_posX
		value_Y = hitch_posY - rear_axis_posY


		# Erstmaliges Berechnen der Truck-Orientierung
		if not self.truck_orientation:
			self.truck_orientation.append(value_X)
			self.truck_orientation.append(value_Y)

		# Überschreiben der letzen Orientierung mit neuem Wert
		if self.truck_orientation:
			self.truck_orientation[0] = value_X
			self.truck_orientation[1] = value_Y



	# Limitierung des Lenkwinkels auf Lenkwinkelbegrenzung
	def limit_steering_angle(self, steer_angle_in):
		steer_angle_out = steer_angle_in
		
		if steer_angle_in < - self.truck_angle_limitation:
			steer_angle_out = - self.truck_angle_limitation

		if steer_angle_in > self.truck_angle_limitation:
			steer_angle_out = self.truck_angle_limitation

		return steer_angle_out



	# Numerische Lösung um von dem Radius der Trajektorie auf einen Lenkwinkel zu kommen
	def calc_radius_to_steer (self, radius):
		steering_angle = math.atan(self.l_truck / radius)
		return math.degrees(steering_angle)

	# Numerische Lösung um von dem Radius der Trajektorie auf einen Knickwinkel zu kommen
	def numeric_radius_to_hitch (self, radius):
		radius_try = 0.0
		angle = 0.1
		angle_step = 0.001
		r_tolerance = 0.1

		while True:
			if angle < 0 or angle > 0:
				radius_try = (self.l_trailer - (self.l_hitch / math.cos(angle))) * (1 / math.tan(angle))
			angle = angle + angle_step
			if radius_try < radius + r_tolerance and radius_try > radius - r_tolerance:			
				return angle


	# Begrenzung der Geschwindigkeit des Einlenkens (Lenkwinkel pro Sekunde) --> Änderung / Delta wird ausgerechnet, welcher auf den aktuellen Lenkwinkel drauf gerechnet werden muss
	def slowdown_steering_angle(self, time_step, steer_target, current_steer_angle):
		
		# positiver Grenzwert überschritten		
		if steer_target > self.truck_angle_limitation:
			steer_target = self.truck_angle_limitation

		# negativer Grenzwert überschritten
		if steer_target < - self.truck_angle_limitation:
			steer_target = - self.truck_angle_limitation

		# Unterscheidung ob Lenkwinkel vergrößert oder verkleinert wird
		if (steer_target - current_steer_angle) < 0:
			negative_delta = True
		else:
			negative_delta = False
		
		# Berechnung einer Lenkwinkeländerung, welche zum aktuellen Lenkwinkel beaufschlagt wird	
		if (negative_delta == True):
			delta_steer_angle = - self.steer_angle_velocity * time_step
		else:
			delta_steer_angle =  self.steer_angle_velocity * time_step

		# Ausnahme: Fehlende Lenkwinkeländerung zum Ziel-Lenkwinkel ist kleiner als Lenkwinkel pro Simulationszykluszeit, dann wird Lenkwinkel direkt auf Wert gesetzt
		if abs(steer_target - current_steer_angle) <= abs(delta_steer_angle):
			delta_steer_angle = steer_target - current_steer_angle

		return delta_steer_angle
	


