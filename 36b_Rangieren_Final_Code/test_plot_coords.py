from Ros_Handle_Topics import subs, client
from Coordinate_system import Coordinate_System
from Vehicle_Parameter import Vehicle_Data
import time
import matplotlib.pyplot as plt

myVehicle = Vehicle_Data()
myClient = client()
mySubs = subs()

# Starten des Ros-Clients
myClient.start_client()
# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()

# Warten bis Ros-Kommunikation vollständig da ist
# mySubs.wait_for_signals()

myCoords = Coordinate_System(myVehicle)

x = []
y = []

start = time.time()

while time.time() - start < 50:
    vel, steer = mySubs.get_velocity(), mySubs.get_steering_angle()
    print(str(vel) + "\t" + str(steer))
    myCoords.calc_truck_position(vel, steer)
    # print(str(myCoords.x) + "\t" + str(myCoords.y))
    x.append(myCoords.x)
    y.append(myCoords.y)

plt.plot(x, y)
plt.title("Truck Route x-y")
plt.show()
