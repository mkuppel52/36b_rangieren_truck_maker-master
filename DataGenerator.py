import os
import rosbag
import yaml
from argparse import ArgumentParser
import pandas as pd
import re


class DataGenerator():
    def __init__(self,
                 rosbagname: str, 
                 ignore_objects: bool
                 ):
                

        self.ignore_objects = ignore_objects
        self.rosbagname = rosbagname

        self.__convert_bagfiles()
        
    
        
        

    def __get_dynamic_lists(self, bag, topic):
        lists = {}
        dyn_lists = []
        
        FirstIteration = True
        for subtopic, msg, t in bag.read_messages(topic):
            if FirstIteration:
                FirstIteration = False
                for slot in msg.__slots__:
                    if isinstance(msg.__getattribute__(slot), list):
                        lists[slot] = msg.__getattribute__(slot).__len__()
            else:
                del_slots = []
                for slot in lists.keys():
                    if abs(msg.__getattribute__(slot).__len__() - lists[slot]) > 1:
                        dyn_lists.append(slot)
                        del_slots.append(slot)
                for slot in del_slots:
                    del lists[slot]
            if not lists:
                break
        return dyn_lists
    

    def __get_list_data(self, high_level, __hierarchy_string__):
        return

            

    def __extract_msg_attributes(self, high_level, __hierarchy_string__ = "", dynamic_lists = []):
        attribute_names = []
        attribute_values = [] 
        if __hierarchy_string__:
            hierarchy_string = __hierarchy_string__ + '.'
        else:
            hierarchy_string = __hierarchy_string__
        for attribute in high_level.__slots__:
            attr_value = getattr(high_level, attribute)
            if isinstance(attr_value, bytes):
                attr_value = tuple(attr_value)
            if hasattr(attr_value, "__slots__"):
                hierarchy_string_attr = hierarchy_string + attribute
                attr_names, attr_values = self.__extract_msg_attributes(high_level = attr_value, __hierarchy_string__ = hierarchy_string_attr, dynamic_lists=[])
                attribute_names.extend(attr_names)
                attribute_values.extend(attr_values)
            elif isinstance(attr_value, list) and attr_value and hasattr(attr_value[0], "__slots__"):
                hierarchy_string_attr = hierarchy_string +  attribute
                for index, elem in enumerate(attr_value):
                    hierarchy_string_attr_index = hierarchy_string_attr + '.' + str(index)
                    attr_names, attr_values = self.__extract_msg_attributes(high_level = attr_value[index], __hierarchy_string__ = hierarchy_string_attr_index, dynamic_lists=[])  
                    attribute_names.extend(attr_names)
                    attribute_values.extend(attr_values) 
            else:
                if isinstance(attr_value, tuple) or isinstance(attr_value, list):
                    if self.ignore_objects:
                        attr_name_value = hierarchy_string + attribute + '.Values'
                        attr_name_len = hierarchy_string + attribute + '.__len__'
                        attr_value_len = attr_value.__len__()
                        attribute_names.extend((attr_name_value, attr_name_len))
                        attribute_values.extend((attr_value, attr_value_len))
                    else:
                        for index, elem in enumerate(attr_value):
                            hierarchy_string_attr_index = hierarchy_string + attribute + '.' + str(index)
                            attribute_names.append(hierarchy_string_attr_index)
                            attribute_values.append(elem) 
                else:
                    attr_name = hierarchy_string + attribute
                    if self.ignore_objects and attribute in dynamic_lists:
                        attr_name = attr_name + '.__len__'
                        attribute_names.append(attr_name)
                        attribute_values.append(attr_value)
                    else:
                        attribute_names.append(attr_name)
                        attribute_values.append(attr_value)
        return attribute_names, attribute_values

    def __edit_msg(self, msg, dynamic_lists):
         # Check if data contains dynamic list
        if self.ignore_objects and any(x in msg.__slots__ for x in dynamic_lists):
            x = [x for x in msg.__slots__ if x in dynamic_lists]
            for elem in x:
                # Set Attribute of Dynamic List to its length
                length = msg.__getattribute__(elem).__len__()
                msg.__setattr__(elem, length)
        return msg   


    def __read_msg(self, msg, t, dynamic_lists):
        attribute_names = ["rosbagTimestamp"]
        attribute_values = [str(t)]
        msg = self.__edit_msg(msg, dynamic_lists)
        attr_names, attr_values = self.__extract_msg_attributes(high_level = msg, dynamic_lists = dynamic_lists)
        attribute_names.extend(attr_names)
        attribute_values.extend(attr_values)
        yield attribute_names, attribute_values


    def __convert_bagfiles(self):
        # Connect rosbag dataframe with message dataframe
            if os.path.exists(self.rosbagname):
                # Open rosbag
                bag = rosbag.Bag(self.rosbagname)
                filename = self.rosbagname.replace(".bag", ".csv")
                rosbag_df = pd.DataFrame()

                dynamic_lists = self.__get_dynamic_lists(bag = bag, topic = "scan")
                             
                for subtopic, msg, t in bag.read_messages("/scan"):
                    attribute_names, attribute_values = next(self.__read_msg(msg, t, dynamic_lists))
                    msg_df = pd.DataFrame(columns=attribute_names, data = [attribute_values])
                    rosbag_df = pd.concat([rosbag_df, msg_df], ignore_index=True, sort=False)

                bag.close()
               
                # Sorting columns
                sorted_cols = sorted(rosbag_df.columns.values, key=self.__build_key())
                rosbag_df = rosbag_df[sorted_cols]
                if not rosbag_df.empty:
                    rosbag_df.to_csv(filename, index = False)

            

                


    def __build_key(self):
        
        rx = re.compile(r'(\d+)|(\d+)')
        def key(x):
            return [(j, int(i)) if i != '' else (j, i) for i, j in rx.findall(x)]
        return key




if __name__ == '__main__':

    dataGenerator = DataGenerator(rosbagname="2022-11-29-12-33-14.bag", ignore_objects=False)

