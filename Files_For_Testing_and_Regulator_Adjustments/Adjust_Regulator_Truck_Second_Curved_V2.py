import time
import roslibpy
import math
import numpy

from log_maneuvers import log_file_init
from log_maneuvers import log_vehicle_param
from log_maneuvers import log_test_param
from log_maneuvers import log_file_shut_down
from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data_Truck
from Trajectory_Regulator import Trajectory_Regulator
from Data_Plot import plot_PID_regulator
from Data_Plot import plot_trajectory
from Maneuver_Parts_Truck import Maneuver_Parts_Truck




timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = False


# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6

# Definition der zu veröffentlichen Variablen	
current_steer_angle_target = 0 # Lenkwinkel Truck vorne
current_velocity_target = 0
current_drive_mode_target = 0.0


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz Trajektorie Berechnung
myTrajectory = Trajectory_Data_Truck()

# Instanz des Reglers um auf Trajektorie zu Fahren
straight_PID = Trajectory_Regulator(100, 100, 100)
curved_PID_first = Trajectory_Regulator(200, 200, 200)
curved_PID_second = Trajectory_Regulator(200, 0, 200)

# Instanz Manöver
myMParts = Maneuver_Parts_Truck()

# Starten des Ros-Clients
myClient.start_client()


# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()

# Warten bis Ros-Kommunikation vollständig da ist
mySubs.wait_for_signals()

velocity_backwards = 5.0
pub_velocity = 10.0
pub_drive_mode = 2
pub_steering = 0.0

time_step = 0.05
time_prev = 0


# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myTrajectory.calc_trajectory_straight(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())

print("Hinter: ", mySubs.get_rear_axis_posX(), "  ", mySubs.get_rear_axis_posY())
print("Hitch: ", mySubs.get_hitch_posX(), "  ", mySubs.get_hitch_posY())

# Main Loop
print("start")
maneuver_time_start = mySubs.get_maneuver_time()

while(myMParts.state_finished == False):
	
	# Warten bis Ros-Kommunikation vollständig da ist
	mySubs.wait_for_signals()

	# Teilmanöver: Vorbeifahren an Parklücke
	if (myMParts.state_park_prep == True):

		# Regelung zum Fahren auf gerader Trajektorie
		calculated_steering_angle = straight_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)

		# Errechneter Lenkwinkel begrenzen, falls maximal möglicher Lenkwinkel überschritten wird
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())
		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_drive_mode = myMParts.maneuver_park_prep(mySubs, myVehicle, myTrajectory)


	# Teilmanöver: Aus- und wieder Anrollen
	if (myMParts.state_roll_out == True or myMParts.state_roll_on == True):
		
		# Regelung zum Fahren auf gerader Trajektorie
		calculated_steering_angle = straight_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)

		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())
		#pub_steering = 0.0
		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_drive_mode = myMParts.maneuver_park_roll(mySubs, myVehicle, myTrajectory, straight_PID.offset_prev)

		# Anhalten
		if (myMParts.state_roll_out == True):
			pub_velocity = 0.0

		# Wieder Anfahren, nun Rückwärts
		if (myMParts.state_roll_on == True):
			pub_velocity = 10.0

	# Teilmanöver: erster Kreis zum Parken
	if (myMParts.state_circle_first == True):
	
		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_drive_mode = myMParts.maneuver_circle_first(mySubs, myVehicle, myTrajectory, velocity_backwards)
		pub_velocity = 10.0
		# Lenkwinkel, welcher theoretisch für den Kreis gefahren werden muss
		steer_target = - myVehicle.calc_radius_to_steer(myTrajectory.r_trajectory)
		# Regelung des Lenkwinkels um auf tatsächlichem Kreis zu fahren --> Um den errechneten Lenkwinkel herum regeln
		calculated_steering_angle = curved_PID_first.regulate_steering_curved (myTrajectory.circle_center_first, mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), steer_target, myTrajectory.r_trajectory, time_step)
		# Lenkwinkelbegrenzung
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())
		

	# Teilmanöver: zweiter Kreis zum Parken
	if (myMParts.state_circle_second == True):
		
		# Lenkwinkel, welcher theoretisch für den Kreis gefahren werden muss
		steer_target = myVehicle.calc_radius_to_steer(myTrajectory.r_trajectory)

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		#pub_drive_mode = myMParts.maneuver_circle_second(mySubs, myVehicle, myTrajectory, steer_target, velocity_backwards)
		pub_drive_mode = 3
		pub_velocity = 10.0
		
		# Regelung des Lenkwinkels um auf tatsächlichem Kreis zu fahren --> Um den errechneten Lenkwinkel herum regeln
		calculated_steering_angle = curved_PID_second.regulate_steering_curved (myTrajectory.circle_center_second, mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), steer_target, myTrajectory.r_trajectory, time_step)
		# Lenkwinkelbegrenzung
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())

		if(mySubs.get_maneuver_time() - maneuver_time_start < 105):	
			myMParts.state_circle_second = True
		else:
			myMParts.state_finished = True



	# Publishen der gewünschten Fahrbefehle
	myPub.pub_velc_stc_dmc(pub_velocity, pub_steering, pub_drive_mode)

	# Speichern der Positionsdaten für spätere Auswertung und Analysis
	myVehicle.save_position_data(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())
	#print(str(time.time()-time_prev))
	print("Pub", pub_steering)
	print("Sub", mySubs.get_steering_angle())
	time_prev = time.time()



# Plots der Regler zur Auswertung
plot_PID_regulator(straight_PID.steer_angle_data, straight_PID.offset_data, time_step, straight_PID.k_p, straight_PID.k_i, straight_PID.k_d, "Steer", "Park_Prep")
plot_PID_regulator(curved_PID_first.steer_angle_data, curved_PID_first.offset_data, time_step, curved_PID_first.k_p, curved_PID_first.k_i, curved_PID_first.k_d, "Steer", "First_circle")
plot_PID_regulator(curved_PID_second.steer_angle_data, curved_PID_second.offset_data, time_step, curved_PID_second.k_p, curved_PID_second.k_i, curved_PID_second.k_d, "Steer", "Second_circle")


# Berechnen der Plot-Punkte der Trajektorie für die spätere grafische Darstellung
myTrajectory.calc_straight_trajectory_data()
myTrajectory.calc_parking_lot_trajectory_data()
myTrajectory.calc_circle_first_data()
myTrajectory.calc_circle_second_data()

# Plotten der Trajektorien zur Auswertung
plot_trajectory(myVehicle.get_posX_data(), myVehicle.get_posY_data(), myTrajectory.trajectory_straight_data_posX, myTrajectory.trajectory_straight_data_posY, myTrajectory.trajectory_parking_lot_data_posX, myTrajectory.trajectory_parking_lot_data_posY, myTrajectory.circle_first_data_posX, myTrajectory.circle_first_data_posY, myTrajectory.circle_second_data_posX, myTrajectory.circle_second_data_posY)




if (log_mode_on == True):	
	log_file_shut_down(logfile)
	
