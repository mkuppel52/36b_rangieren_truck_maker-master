import time
import roslibpy
import math
import numpy

from log_maneuvers import log_file_init
from log_maneuvers import log_vehicle_param
from log_maneuvers import log_test_param
from log_maneuvers import log_file_shut_down
from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data
from Trajectory_Regulator import Trajectory_Regulator
from Data_Plot import plot_hitch_regulator
from Data_Plot import plot_PID_regulator




def delta_drive_distance(current_time, previous_time, velocity):
	drive_distance_new = (current_time - previous_time) * velocity
	return drive_distance_new

# Numerische Lösung um von dem Radius der Trajektorie auf einen Knickwinkel zu kommen
def calc_radius_to_steer (vd, td):
	steering_angle = math.atan(vd.l_truck / td.r_trajectory)
	return math.degrees(steering_angle)


def calc_velocity_rear(velocity_front, radius, l_truck):
	velocity_back = velocity_front * math.sqrt(radius ** 2 + l_truck ** 2) / radius
	return velocity_back




timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = False



# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6

# Definition der zu veröffentlichen Variablen	
current_steer_angle_target = 0 # Lenkwinkel Truck vorne
current_velocity_target = 0
current_drive_mode_target = 0.0


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz für Trajektorien
myTrajectory = Trajectory_Data()

# Instanz des Reglers um auf Trajektorie zu Fahren
straight_PID = Trajectory_Regulator(95, 16, 150)

# Starten des Ros-Clients
myClient.start_client()

# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()

# Warten bis Ros-Kommunikation vollständig da ist
mySubs.wait_for_signals()

maneuver_time_start = mySubs.get_maneuver_time()


pub_velocity = 5
pub_drive_mode = 2
pub_steering = 0.0

time_step = 0.05

test_regulator = True

myVehicle.save_hitch_angle(mySubs.get_hitch_angle())

# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myTrajectory.calc_trajectory_straight(mySubs.get_hitch_posX(), mySubs.get_hitch_posY()+0.5, mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY()+0.5)

hitch_data = []
steer_data = []



# Main Loop
print("start")

while(test_regulator == True):
	
	# Warten bis Ros-Kommunikation vollständig da ist
	mySubs.wait_for_signals()

	pub_drive_mode = 3
	pub_steering = 0.0

	if (mySubs.get_velocity() > (0.5/3.6)):
		calculated_steering_angle = straight_PID.regulate_hitch_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())
		steer_data.append(pub_steering)

	if(mySubs.get_maneuver_time() - maneuver_time_start > 30):
		test_regulator = False
		pub_drive_mode = 2

	#Publishen der gewünschten Fahrbefehle
	myPub.pub_velc_stc_dmc(pub_velocity, pub_steering, pub_drive_mode)


plot_PID_regulator(steer_data, straight_PID.offset_data, time_step, straight_PID.k_p, straight_PID.k_i, straight_PID.k_d, "Lenk", "PID Straight Front, no Trailer")
