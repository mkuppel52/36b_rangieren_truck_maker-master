import time
import roslibpy
import math
import numpy

from log_maneuvers import log_file_init
from log_maneuvers import log_vehicle_param
from log_maneuvers import log_test_param
from log_maneuvers import log_file_shut_down
from Hitch_Angle_Reg import calc_steer_angle_reichenegger_old
from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data
from Trajectory_Regulator import Trajectory_Regulator
from Data_Plot import plot_hitch_regulator
from Data_Plot import plot_PID_regulator
from Data_Plot import plot_trajectory_trailer
from Vector_Operations import rotate_orientation_vector




def calc_circle_data(circle_center, radius):

	i = 0

	circle_data = []
	circle_data_X = []
	circle_data_Y = []

	orientation_vector = [1,1]


	while (i < 360):
		vector_center_to_radian = rotate_orientation_vector(orientation_vector,  i)
		length_vector_cr = math.sqrt(vector_center_to_radian[0] ** 2 + vector_center_to_radian[1] ** 2)

		posX = circle_center[0] + (radius / length_vector_cr) * vector_center_to_radian[0]
		posY = circle_center[1] + (radius / length_vector_cr) * vector_center_to_radian[1]

		circle_data_X.append(posX)
		circle_data_Y.append(posY)


		i = i + 0.5


	circle_data.append(circle_data_X)
	circle_data.append(circle_data_Y)

	return circle_data


def cycle_wait(wait_time):
	current_time_start = time.time()		
	while time.time() <= current_time_start + wait_time:
		pass



def delta_drive_distance(current_time, previous_time, velocity):
	drive_distance_new = (current_time - previous_time) * velocity
	return drive_distance_new

# Numerische Lösung um von dem Radius der Trajektorie auf einen Knickwinkel zu kommen
def calc_radius_to_steer (vd, td):
	steering_angle = math.atan(vd.l_truck / td.r_trajectory)
	return math.degrees(steering_angle)


def calc_velocity_rear(velocity_front, radius, l_truck):
	velocity_back = velocity_front * math.sqrt(radius ** 2 + l_truck ** 2) / radius
	return velocity_back


def calc_straight_kaska_trajectory(location_vector_straight, orientation_vector_straight):
	
	trajectory_straight_data_posX = []
	trajectory_straight_data_posY = []
	
	trajectory_straight_data = []

	length_ov = math.sqrt(orientation_vector_straight[0] ** 2 + orientation_vector_straight[1] ** 2)
	i = 6


	while (i < 154):
		posX = location_vector_straight[0] - ( i / length_ov) * orientation_vector_straight[0]
		posY = location_vector_straight[1] - ( i / length_ov) * orientation_vector_straight[1]
			
			

		trajectory_straight_data_posX.append(posX)
		trajectory_straight_data_posY.append(posY)

		i = i + 0.05

	trajectory_straight_data.append(trajectory_straight_data_posX)
	trajectory_straight_data.append(trajectory_straight_data_posY)
			
	return trajectory_straight_data

timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = False



# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6

# Definition der zu veröffentlichen Variablen	
current_steer_angle_target = 0 # Lenkwinkel Truck vorne
current_velocity_target = 0
current_drive_mode_target = 0.0


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz für Trajektorien
myTrajectory = Trajectory_Data()

# Instanz des Reglers um auf Trajektorie zu Fahren
curved_PID_hitch = Trajectory_Regulator(1, 0, 10)

# Starten des Ros-Clients
myClient.start_client()

# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()



# Warten bis Ros-Kommunikation vollständig da ist
mySubs.wait_for_signals()

maneuver_time_start = mySubs.get_maneuver_time()

velocity_backward = 10.0

pub_velocity = velocity_backward
pub_drive_mode = 2
pub_steering = 0.0

time_step = 0.05

test_regulator = True
start_pid_regulating = False

hitch_target = 0

# Nur zur grafischen Überwachung des Offsets zur Trajektorie
steering_dummy = 0.0

myVehicle.save_hitch_angle(mySubs.get_hitch_angle())

# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myTrajectory.calc_trajectory_straight(mySubs.get_hitch_posX(), mySubs.get_hitch_posY()+1.0, mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY()+1.0)

hitch_data = []
steer_data = []

prev_distance_check_time = 0.0
check_distance_time_intervals = 0.2

circle_center_pos = [94, 35]
r_trajectory = 25

# Main Loop
print("start")

while(test_regulator == True):
	
	# Warten bis Ros-Kommunikation vollständig da ist
	mySubs.wait_for_signals()

	pub_drive_mode = 3
	pub_steering = 0.0
	
	# Bestimmung Trailer Hinterachse Position, nötig für die Regelung der Trajektorie beim Rückwärtsfahren
	myVehicle.calc_trailer_axis_position(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), mySubs.get_hitch_angle())

	if(start_pid_regulating is False):
		hitch_target = - math.degrees(myVehicle.numeric_radius_to_hitch(r_trajectory))	
		calculated_hitch_angle = hitch_target
		calculated_steering_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), calculated_hitch_angle, calculated_hitch_angle - myVehicle.get_stored_hitch_angle()) 
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())
	
	if (abs(hitch_target)-abs(mySubs.get_hitch_angle()) < 2.0 and start_pid_regulating is False):
		start_pid_regulating = True
		print("Start Regulating with PID")	
	

	if((mySubs.get_maneuver_time() - maneuver_time_start - prev_distance_check_time) > check_distance_time_intervals and start_pid_regulating is True):
		hitch_target = 	- math.degrees(myVehicle.numeric_radius_to_hitch(r_trajectory))	
		calculated_hitch_angle = curved_PID_hitch.regulate_hitch_curved(circle_center_pos, myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), hitch_target, r_trajectory, check_distance_time_intervals)
		prev_distance_check_time = mySubs.get_maneuver_time() - maneuver_time_start
		#print("H-Target: ", hitch_target, " Radius: ", r_trajectory)

	if (mySubs.get_velocity() > (velocity_backward * 0.5 / 3.6)):
		calculated_steering_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), calculated_hitch_angle, calculated_hitch_angle - myVehicle.get_stored_hitch_angle()) 

		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())


	if(mySubs.get_maneuver_time() - maneuver_time_start > 199):
		test_regulator = False

	if (mySubs.get_hitch_angle() > 100 or mySubs.get_hitch_angle() < -100):
		test_regulator = False
		pub_drive_mode = 2

	hitch_data.append(mySubs.get_hitch_angle())
	steer_data.append(pub_steering)
 
	#Publishen der gewünschten Fahrbefehle
	myPub.pub_velc_stc_dmc(pub_velocity, pub_steering, pub_drive_mode)

	# Speichern der Positionsdaten für spätere Auswertung und Analysis
	myVehicle.save_position_data(myVehicle.trailer_axis_pos[0], myVehicle.trailer_axis_pos[1])


# Berechnen der Plot-Punkte der Trajektorie für die spätere grafische Darstellung
circle_data = []
circle_data = calc_circle_data(circle_center_pos, r_trajectory)

plot_trajectory_trailer(myVehicle.get_posX_data(), myVehicle.get_posY_data(), circle_data[0], circle_data[1], 0, 0, 0, 0, 0, 0, 0, 0)


plot_hitch_regulator(steer_data, hitch_data, hitch_target, time_step, -0.458, 18.0)
plot_PID_regulator(curved_PID_hitch.hitch_angle_data, curved_PID_hitch.offset_data, check_distance_time_intervals, curved_PID_hitch.k_p, curved_PID_hitch.k_i, curved_PID_hitch.k_d, "Knick", "Straight back, combi with Hitch Reg")





	
