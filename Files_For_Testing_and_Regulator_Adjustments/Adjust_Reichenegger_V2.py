import time
import roslibpy
import math
import numpy

from log_maneuvers import log_file_init
from log_maneuvers import log_vehicle_param
from log_maneuvers import log_test_param
from log_maneuvers import log_file_shut_down
from Hitch_Angle_Reg import calc_steer_angle_reichenegger_old
from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data
from Trajectory_Regulator import Trajectory_Regulator
from Data_Plot import plot_hitch_regulator
from Data_Plot import plot_PID_regulator
from Data_Plot import plot_trajectory_trailer



timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = False



# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6

# Definition der zu veröffentlichen Variablen	
current_steer_angle_target = 0 # Lenkwinkel Truck vorne
current_velocity_target = 0
current_drive_mode_target = 0.0


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz für Trajektorien
myTrajectory = Trajectory_Data()

# Instanz des Reglers um auf Trajektorie zu Fahren
straight_PID = Trajectory_Regulator(0, 0, 0)

# Starten des Ros-Clients
myClient.start_client()

# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()



# Warten bis Ros-Kommunikation vollständig da ist
while mySubs.get_maneuver_time() == 0.0:
	maneuver_time_start = mySubs.get_maneuver_time()

maneuver_time_start = mySubs.get_maneuver_time()

velocity_backward = 10.0

pub_velocity = velocity_backward
pub_drive_mode = 2
pub_steering = 0.0

time_step = 0.05

test_regulator = True

hitch_target = 10.0

save_hitch_angle = 0.0
hitch_angle_thresh = 0.0

# Nur zur grafischen Überwachung des Offsets zur Trajektorie
steering_dummy = 0.0

myVehicle.save_hitch_angle(mySubs.get_hitch_angle())

# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myTrajectory.calc_trajectory_straight(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())

hitch_data = []
steer_data = []



# Main Loop
print("start")

while(test_regulator == True):
	
	# Warten, bis ROS neuer Simulationstakt sendet
	mySubs.wait_for_signals()

	pub_drive_mode = 3
	pub_steering = 0.0

	# Bestimmung Trailer Hinterachse Position, nötig für die Regelung der Trajektorie beim Rückwärtsfahren
	myVehicle.calc_trailer_axis_position(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), mySubs.get_hitch_angle())


	if (mySubs.get_velocity() > (velocity_backward * 0.5 / 3.6) and mySubs.get_maneuver_time() - maneuver_time_start <= 10):
		
		if(myVehicle.check_new_hitch_target(hitch_target) is True):
			myVehicle.save_hitch_angle(mySubs.get_hitch_angle())
			print("Store now")
		
		#calculated_steering_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle()) 

		calculated_steering_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target) 

		print("Hitch Target: ", hitch_target, " Current Hitch: ", mySubs.get_hitch_angle(), " Stored Hitch: ", myVehicle.get_stored_hitch_angle(), " Delta_start: ", hitch_target - myVehicle.get_stored_hitch_angle())

		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())

			
	if(mySubs.get_maneuver_time() - maneuver_time_start > 10 and mySubs.get_maneuver_time() - maneuver_time_start <= 30):
		hitch_target = 45	
		
		if(myVehicle.check_new_hitch_target(hitch_target) is True):
			myVehicle.save_hitch_angle(mySubs.get_hitch_angle())
			print("Store now")

		#calculated_steering_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle())

		calculated_steering_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target) 
 
		print("Hitch Target: ", hitch_target, " Current Hitch: ", mySubs.get_hitch_angle(), " Stored Hitch: ", myVehicle.get_stored_hitch_angle(), " Delta_start: ", hitch_target - myVehicle.get_stored_hitch_angle())

		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())


	if(mySubs.get_maneuver_time() - maneuver_time_start > 30 and mySubs.get_maneuver_time() - maneuver_time_start <= 50):
		hitch_target = -5.0	
		
		if(myVehicle.check_new_hitch_target(hitch_target) is True):
			#myVehicle.save_hitch_angle(mySubs.get_hitch_angle())
			print("Store now")

		calculated_steering_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle()) 
		print("Hitch Target: ", hitch_target, " Current Hitch: ", mySubs.get_hitch_angle(), " Stored Hitch: ", myVehicle.get_stored_hitch_angle(), " Delta_start: ", hitch_target - myVehicle.get_stored_hitch_angle())

		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())


	if(mySubs.get_maneuver_time() - maneuver_time_start > 50 and mySubs.get_maneuver_time() - maneuver_time_start <= 70):
		hitch_target = 11.0	
		
		if(myVehicle.check_new_hitch_target(hitch_target) is True):
			myVehicle.save_hitch_angle(mySubs.get_hitch_angle())
			print("Store now")

		calculated_steering_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle()) 

		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())

	if(mySubs.get_maneuver_time() - maneuver_time_start > 70 and mySubs.get_maneuver_time() - maneuver_time_start <= 75):
		hitch_target = 0.0	
		
		if(myVehicle.check_new_hitch_target(hitch_target) is True):
			myVehicle.save_hitch_angle(mySubs.get_hitch_angle())
			print("Store now")

		calculated_steering_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle()) 

		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())

		print("Target: ", calculated_steering_angle, " Current: ", mySubs.get_steering_angle(), " Delta: ",  myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle()), " Pub Angle: ", pub_steering)
		#if (pub_steering > save_steer_angle + new_angle_thresh):
			#pub_steering =  save_steer_angle + new_angle_thresh
			#print("o" , pub_steering)

		#if (pub_steering < save_steer_angle - new_angle_thresh):
			#pub_steering =  save_steer_angle - new_angle_thresh
			#print("a" , pub_steering)

	if(mySubs.get_maneuver_time() - maneuver_time_start > 65):
		test_regulator = False

	hitch_data.append(mySubs.get_hitch_angle())
	steer_data.append(pub_steering)

	steering_dummy = straight_PID.regulate_steering_straight(myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)
 
	#Publishen der gewünschten Fahrbefehle
	myPub.pub_velc_stc_dmc(pub_velocity, pub_steering, pub_drive_mode)

	# Speichern der Positionsdaten für spätere Auswertung und Analysis
	myVehicle.save_position_data(myVehicle.trailer_axis_pos[0], myVehicle.trailer_axis_pos[1])





plot_hitch_regulator(steer_data, hitch_data, hitch_target, time_step, "Poly-Approx", "Poly-Approx")
#plot_PID_regulator(straight_PID.steer_angle_data, straight_PID.offset_data, time_step, straight_PID.k_p, straight_PID.k_i, straight_PID.k_d, "Steer", "Offset Information, back only wiht Hitch Reg")

plot_trajectory_trailer(myVehicle.get_posX_data(), myVehicle.get_posY_data(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
	
