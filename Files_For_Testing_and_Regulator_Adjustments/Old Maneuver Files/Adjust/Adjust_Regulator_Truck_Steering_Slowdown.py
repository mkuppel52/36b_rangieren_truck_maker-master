import time
import roslibpy
import math
import numpy

from log_maneuvers import log_file_init
from log_maneuvers import log_vehicle_param
from log_maneuvers import log_test_param
from log_maneuvers import log_file_shut_down
from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data_Truck
from Trajectory_Regulator import Trajectory_Regulator
from Data_Plot import plot_PID_regulator
from Data_Plot import plot_trajectory
from Data_Plot import plot_steering_slowdown
from Maneuver_Parts_Truck import Maneuver_Parts_Truck



def cycle_wait(wait_time):
	current_time_start = time.time()		
	while time.time() <= current_time_start + wait_time:
		pass


timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = False


# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6

# Definition der zu veröffentlichen Variablen	
current_steer_angle_target = 0 # Lenkwinkel Truck vorne
current_velocity_target = 0
current_drive_mode_target = 0.0


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz Trajektorie Berechnung
myTrajectory = Trajectory_Data_Truck()

# Instanz Manöver
myMParts = Maneuver_Parts_Truck()

# Starten des Ros-Clients
myClient.start_client()


# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()



# Erstellen der Log Datei --> Neue Datei mit Datum und Zeitpunkt des Erstellens wird im Ordner log_files erstellt
if log_mode_on == True: 
	logfile = log_file_init(timestr, "E")
	log_vehicle_param(logfile, l_hitch, l_truck, l_trailer)
	log_test_param(logfile, hitch_angle_target_1 ,vel_target, 0.05, 18.0,)

# Warten bis Ros-Kommunikation vollständig da ist
while mySubs.get_maneuver_time() == 0.0:
	maneuver_time_start = mySubs.get_maneuver_time()



pub_velocity = 5.0
pub_drive_mode = 1
pub_steering = 0.0

time_step = 0.07
time_prev = 0

steer_target = 0.0

steer_angle_data = []
steer_target_data = []

# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myTrajectory.calc_trajectory_straight(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())

print("Hinter: ", mySubs.get_rear_axis_posX(), "  ", mySubs.get_rear_axis_posY())
print("Hitch: ", mySubs.get_hitch_posX(), "  ", mySubs.get_hitch_posY())

# Main Loop
print("start")
print(mySubs.get_steering_angle())

while(myMParts.state_finished == False):

	if(mySubs.get_maneuver_time() - maneuver_time_start > 1):	
		steer_target = -25

	if(mySubs.get_maneuver_time() - maneuver_time_start > 15):	
		steer_target = -15

	if(mySubs.get_maneuver_time() - maneuver_time_start > 20):	
		myMParts.state_finished = True
	
	#print(mySubs.get_steering_angle())
	steer_angle = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(time_step, steer_target, mySubs.get_steering_angle())

	pub_steering = myVehicle.limit_steering_angle(steer_angle)
	
	print("Time: [s] ", format(((time.time()) % 1000), ".3f") ," Pub: ", pub_steering, " ", "Current: ", mySubs.get_steering_angle(), " ", "Target: ", steer_target)

	#print(pub_drive_mode)

	# Speichern der Positionsdaten für spätere Auswertung und Analysis
	myVehicle.save_position_data(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())
	steer_target_data.append(steer_target)
	steer_angle_data.append(mySubs.get_steering_angle())

	# Publishen der gewünschten Fahrbefehle
	myPub.pub_velc_stc_dmc(pub_velocity, pub_steering, pub_drive_mode)

	

	#print(str(time.time()-time_prev))
	#print(pub_steering)
	time_prev = time.time()

	# Zykluszeit von 0.05 s --> 20 Hz
	cycle_wait(time_step)


# Plots der Regler zur Auswertung
plot_steering_slowdown(time_step, steer_angle_data, steer_target_data)


# Plotten der Trajektorien zur Auswertung
plot_trajectory(myVehicle.get_posX_data(), myVehicle.get_posY_data(), myTrajectory.trajectory_straight_data_posX, myTrajectory.trajectory_straight_data_posY, myTrajectory.trajectory_parking_lot_data_posX, myTrajectory.trajectory_parking_lot_data_posY, myTrajectory.circle_first_data_posX, myTrajectory.circle_first_data_posY, myTrajectory.circle_second_data_posX, myTrajectory.circle_second_data_posY)




if (log_mode_on == True):	
	log_file_shut_down(logfile)
	
