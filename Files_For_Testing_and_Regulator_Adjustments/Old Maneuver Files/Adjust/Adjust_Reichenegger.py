import time
import roslibpy
import math
import numpy

from log_maneuvers import log_file_init
from log_maneuvers import log_vehicle_param
from log_maneuvers import log_test_param
from log_maneuvers import log_file_shut_down
from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data
from Trajectory_Regulator import Trajectory_Regulator
from Data_Plot import plot_hitch_regulator
from Data_Plot import plot_PID_regulator
from Data_Plot import plot_trajectory_trailer


def cycle_wait(wait_time):
	current_time_start = time.time()		
	while time.time() <= current_time_start + wait_time:
		pass



def delta_drive_distance(current_time, previous_time, velocity):
	drive_distance_new = (current_time - previous_time) * velocity
	return drive_distance_new

# Numerische Lösung um von dem Radius der Trajektorie auf einen Knickwinkel zu kommen
def calc_radius_to_steer (vd, td):
	steering_angle = math.atan(vd.l_truck / td.r_trajectory)
	return math.degrees(steering_angle)


def calc_velocity_rear(velocity_front, radius, l_truck):
	velocity_back = velocity_front * math.sqrt(radius ** 2 + l_truck ** 2) / radius
	return velocity_back




timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = False



# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6

# Definition der zu veröffentlichen Variablen	
current_steer_angle_target = 0 # Lenkwinkel Truck vorne
current_velocity_target = 0
current_drive_mode_target = 0.0


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz für Trajektorien
myTrajectory = Trajectory_Data()

# Instanz des Reglers um auf Trajektorie zu Fahren
straight_PID = Trajectory_Regulator(0, 0, 0)

# Starten des Ros-Clients
myClient.start_client()

# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()



# Warten bis Ros-Kommunikation vollständig da ist
while mySubs.get_maneuver_time() == 0.0:
	maneuver_time_start = mySubs.get_maneuver_time()

maneuver_time_start = mySubs.get_maneuver_time()

velocity_backward = 10.0

pub_velocity = velocity_backward
pub_drive_mode = 2
pub_steering = 0.0

time_step = 0.05

test_regulator = True

hitch_target = 10.0

save_steer_angle = 0.0
new_angle_thresh = 50.0

# Nur zur grafischen Überwachung des Offsets zur Trajektorie
steering_dummy = 0.0

myVehicle.save_hitch_angle(mySubs.get_hitch_angle())

# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myTrajectory.calc_trajectory_straight(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())

hitch_data = []
steer_data = []



# Main Loop
print("start")

while(test_regulator == True):
	
	pub_drive_mode = 3
	pub_steering = 0.0

	# Bestimmung Trailer Hinterachse Position, nötig für die Regelung der Trajektorie beim Rückwärtsfahren
	myVehicle.calc_trailer_axis_position(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), mySubs.get_hitch_angle())


	if (mySubs.get_velocity() > (velocity_backward * 0.5 / 3.6) and mySubs.get_maneuver_time() - maneuver_time_start < 10):
		calculated_steering_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle()) 

		pub_steering = myVehicle.limit_steering_angle(calculated_steering_angle)
		save_steer_angle = pub_steering
		print(save_steer_angle)
			
	if(mySubs.get_maneuver_time() - maneuver_time_start > 10):
		hitch_target = 11.0	

		calculated_steering_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle()) 

		pub_steering = myVehicle.limit_steering_angle(calculated_steering_angle)

		if (pub_steering > save_steer_angle + new_angle_thresh):
			pub_steering =  save_steer_angle + new_angle_thresh
			print("o" , pub_steering)

		if (pub_steering < save_steer_angle - new_angle_thresh):
			pub_steering =  save_steer_angle - new_angle_thresh
			print("a" , pub_steering)

	if(mySubs.get_maneuver_time() - maneuver_time_start > 30):
		test_regulator = False

	hitch_data.append(mySubs.get_hitch_angle())
	steer_data.append(pub_steering)

	steering_dummy = straight_PID.regulate_steering_straight(myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)
 
	#Publishen der gewünschten Fahrbefehle
	myPub.pub_velc_stc_dmc(pub_velocity, pub_steering, pub_drive_mode)

	# Speichern der Positionsdaten für spätere Auswertung und Analysis
	myVehicle.save_position_data(myVehicle.trailer_axis_pos[0], myVehicle.trailer_axis_pos[1])

	# Zykluszeit von 0.05 s --> 20 Hz
	cycle_wait(time_step)



plot_hitch_regulator(steer_data, hitch_data, hitch_target, time_step, -0.008, 18.0)
#plot_PID_regulator(straight_PID.steer_angle_data, straight_PID.offset_data, time_step, straight_PID.k_p, straight_PID.k_i, straight_PID.k_d, "Steer", "Offset Information, back only wiht Hitch Reg")

plot_trajectory_trailer(myVehicle.get_posX_data(), myVehicle.get_posY_data(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
	
