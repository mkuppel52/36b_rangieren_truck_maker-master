import math


# Regler nach Reichenegger --> Funktion zur Lenkwinkelberechnung für die Regelung des Knickwinkels auf konstanten Wert
def calc_steer_angle_reichenegger(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle, hitch_angle_target, delta_hitch_angle_start_rad):
	

	lambda_1 = -0.008
	lambda_2 = 18

	delta_hitch_angle_rad = math.radians(hitch_angle_target - current_hitch_angle)

	g = g_phi(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle)
	f = f_phi(current_velocity, l_trailer, current_hitch_angle, hitch_angle_target)

	# Verhindert das bei u durch 0 geteilt wird	
	if g == 0:
		g = 0.00001

	# Formel zur Berechnung des Lenkwinkels	
	u = (1 / g) * (-f - lambda_1 * delta_hitch_angle_start_rad  - lambda_2 * delta_hitch_angle_rad)

	steering_angle = math.degrees(math.atan(u))

	# Begrenzung des maximalen Lenkwinkels zur naturgetreuen Darstellung --> 25° ist nur geschätzer Wert
	if steering_angle > 25:
		steering_angle = 25
	if steering_angle < -25:
		steering_angle = -25

	return steering_angle



# Mathematische Teilfunktion für die Lenkwinkelberechnung
def g_phi(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle):
	
	current_hitch_angle_rad = math.radians(current_hitch_angle)
	
	g = (current_velocity / l_truck) - (current_velocity * l_hitch * math.cos(current_hitch_angle_rad)) / (l_truck * l_trailer)

	return g



# Mathematische Teilfunktion für die Lenkwinkelberechnung
def f_phi(current_velocity, l_trailer, current_hitch_angle, hitch_angle_target):
	
	current_hitch_angle_rad = math.radians(current_hitch_angle)

	f = 0 + (current_velocity * math.sin(current_hitch_angle_rad)) / l_trailer

	return f
