import time
import roslibpy
import math

# Definition LKW Parameter
l_truck = 3.57 # Länge Zugmaschine --> Achse zu Achse
l_hitch = 0.95 # Länge Hinterachse zu Auflagepunkt
l_trailer = 7.75 # Trailer Auflagepunkt bis Achse

# Definition Regelparameter
hitch_angle_target = 2.0
vel_target = 5.0
vel_target_mps = vel_target / 3.6
lambda_1 = 0.05
lambda_2 = 3
delta_phi_start = 0.0 # nur hier mit 0.0 initialisiert, wird später in Main entsprechend der Startbedingung initialisiert



# Client für Ros --> Rosbride mit WebSocket muss gelauncht werden --> roslaunch rosbridge_server rosbridge_websocket.launch
client = roslibpy.Ros(host='localhost', port=9090)
client.run()

# Definition der Topics die veröffentlich werden
pub_velocity_control = roslibpy.Topic(client, '/velocity_control', 'std_msgs/Float64')
pub_steering_control = roslibpy.Topic(client, '/steering_control', 'std_msgs/Int32')

# Definition der zu veröffentlichen Variablen
#current_vel = 0.0 # Truckgeschwindigkeit	
current_steer_angle = 0 # Lenkwinkel Truck vorne


# Definition der Topics die abonniert werden
sub_rear_distance =roslibpy.Topic(client, '/rear_distance', 'std_msgs/Float32')
sub_hitch_angle =roslibpy.Topic(client, '/hitch_angle', 'std_msgs/Float64')
sub_hitch_angle_vel =roslibpy.Topic(client, '/hitch_angle_vel', 'std_msgs/Float64')
sub_velocity = roslibpy.Topic(client, '/velocity', 'std_msgs/Float64')


# Klasse zum zwischen speichern der Subscribe Messages
class subs (object):

	current_velocity = 0.0	# Geschwindigkeit des Trucks
	current_rear_distance = 0.0 # Abstand nach Hinten zu detektierten Objekten
		
	current_hitch_angle = 0.0 # Winkel zwischen Truck und Trailer
	current_hitch_angle_vel = 0.0 # Winkelgeschwindigkeit zwischen Truck und Trailer

	def callback_velocity(self,msg):
		 self.current_velocity = msg['data']
	
	def callback_rear_distance(self,msg):
		self.current_rear_distance = msg['data']

	def callback_hitch_angle(self,msg): 
		self.current_hitch_angle = msg['data']

	def callback_hitch_angle_vel(self,msg): 
		self.current_hitch_angle_vel = msg['data']

	def get_velocity(self):
		return self.current_velocity

	def get_rear_distance(self):
		return self.current_rear_distance	

	def get_hitch_angle(self):
		return self.current_hitch_angle
	
	def get_hitch_angle_vel(self):
		return self.current_hitch_angle_vel


	
# Funktion zur Lenkwinkel berechnung für die Regelung des Knickwinkels auf konstanten Wert
def calc_steer_angle(vel_target_mps, l_hitch, l_truck, l_trailer, current_hitch_angle, hitch_angle_target, delta_hitch_angle_start, lambda_1, lambda_2):
	
	steering_angle = 0.0

	# Formel zur Berechnung des Lenkwinkels	
	u = (1 / g_phi(vel_target_mps, l_hitch, l_truck, l_trailer, current_hitch_angle)) * (-f_phi(vel_target_mps, l_trailer, current_hitch_angle, hitch_angle_target) - lambda_1 * delta_hitch_angle_start - 	lambda_2 * math.radians((hitch_angle_target - current_hitch_angle)))

	steering_angle = math.degrees(math.atan(u))

	# Begrenzung des maximalen Lenkwinkels zur naturgetreuen Darstellung --> 25° ist nur geschätzer Wert
	if steering_angle > 25:
		steering_angle = 25
	if steering_angle < -25:
		steering_angle = -25

	return steering_angle



# Mathematische Teilfunktion für die Lenkwinkelberechnung
def g_phi(vel_target_mps, l_hitch, l_truck, l_trailer, current_hitch_angle):
	g = (vel_target_mps / l_truck) - (vel_target_mps * l_hitch * math.cos(math.radians(current_hitch_angle))) / (l_truck * l_trailer)
	return g



# Mathematische Teilfunktion für die Lenkwinkelberechnung
def f_phi(vel_target_mps, l_trailer, current_hitch_angle, hitch_angle_goal):
	f = math.radians(hitch_angle_goal) + (vel_target_mps * math.sin(math.radians(current_hitch_angle))) / l_trailer
	return f



# Instanz der Klasse
mySubs = subs()


# Subscriber
sub_rear_distance.subscribe(mySubs.callback_rear_distance)	
sub_hitch_angle.subscribe(mySubs.callback_hitch_angle)
sub_velocity.subscribe(mySubs.callback_velocity)



# Main Loop

# Regeldifferenz zu Beginn in rad
delta_hitch_angle_start = math.radians(hitch_angle_target - mySubs.get_hitch_angle()) 

loop_count = 0;

# Abbruch Kriterium
while time.clock() < 100:
	# Ausführen der Regelung und der Ros-Befehle alle 10.000 Cyclen zur Begrenzung des Rechenaufwandes
	if loop_count == 10000:
		current_steer_angle = calc_steer_angle(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target, delta_hitch_angle_start, lambda_1, lambda_2)
		pub_velocity_control.publish(roslibpy.Message({'data':float(vel_target)}))		
		pub_steering_control.publish(roslibpy.Message({'data':int(current_steer_angle)}))
		print("Zeit:", time.clock(), "Geschwindigkeit:", mySubs.get_velocity(), "Lenkwinkel:" , current_steer_angle, "Hitch Winkel:", mySubs.get_hitch_angle())
		loop_count = 0
	loop_count = loop_count + 1
	


	
	
