import time
import roslibpy
import math
from log_maneuvers import log_file_init
from log_maneuvers import log_file_logger
from log_maneuvers import log_file_shut_down

timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = True
# Desired Hitch Angle between Truck and Trailer
hitch_angle_target = 0.0

# Definition LKW Parameter
l_truck = 3.57 # Länge Zugmaschine --> Achse zu Achse
l_hitch = 0.95 # Länge Hinterachse zu Auflagepunkt
l_trailer = 7.75 # Trailer Auflagepunkt bis Achse

# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6
# Gewichtungsfaktoren des Reglers --> Wurdem empirisch ermittelt
lambda_1 = 0.05
lambda_2 = 18.0



# Client für Ros --> Rosbride mit WebSocket muss gelauncht werden --> roslaunch rosbridge_server rosbridge_websocket.launch
client = roslibpy.Ros(host='localhost', port=9090)
client.run()

# Definition der Topics die veröffentlich werden
pub_velocity_control = roslibpy.Topic(client, '/velocity_control', 'std_msgs/Float64')
pub_steering_control = roslibpy.Topic(client, '/steering_control', 'std_msgs/Int32')

# Definition der zu veröffentlichen Variablen	
current_steer_angle = 0 # Lenkwinkel Truck vorne


# Definition der Topics die abonniert werden
sub_rear_distance =roslibpy.Topic(client, '/rear_distance', 'std_msgs/Float32')
sub_hitch_angle =roslibpy.Topic(client, '/hitch_angle', 'std_msgs/Float64')
sub_hitch_angle_vel =roslibpy.Topic(client, '/hitch_angle_vel', 'std_msgs/Float64')
sub_velocity = roslibpy.Topic(client, '/velocity', 'std_msgs/Float64')


# Klasse zum zwischen speichern der Subscribe Messages
class Ros_Subscriber (object):

	current_velocity = 0.0	# Geschwindigkeit des Trucks
	current_rear_distance = 0.0 # Abstand nach Hinten zu detektierten Objekten
		
	current_hitch_angle = 0.0 # Winkel zwischen Truck und Trailer
	current_hitch_angle_vel = 0.0 # Winkelgeschwindigkeit zwischen Truck und Trailer

	def callback_velocity(self,msg):
		 self.current_velocity = msg['data']
	
	def callback_rear_distance(self,msg):
		self.current_rear_distance = msg['data']

	def callback_hitch_angle(self,msg): 
		self.current_hitch_angle = msg['data']

	def callback_hitch_angle_vel(self,msg): 
		self.current_hitch_angle_vel = msg['data']

	def get_velocity(self):
		return self.current_velocity

	def get_rear_distance(self):
		return self.current_rear_distance	

	def get_hitch_angle(self):
		return self.current_hitch_angle
	
	def get_hitch_angle_vel(self):
		return self.current_hitch_angle_vel


	
# Regler nach Reichenegger --> Funktion zur Lenkwinkelberechnung für die Regelung des Knickwinkels auf konstanten Wert
def calc_steer_angle_reichenegger(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle, hitch_angle_target, delta_hitch_angle_start_rad, lambda_1, lambda_2):
	
	delta_hitch_angle_rad = math.radians(hitch_angle_target - current_hitch_angle)

	g = g_phi(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle)
	f = f_phi(current_velocity, l_trailer, current_hitch_angle, hitch_angle_target)

	# Verhindert das bei u durch 0 geteilt wird	
	if g == 0:
		g = 0.00001

	# Formel zur Berechnung des Lenkwinkels	
	u = (1 / g) * (-f - lambda_1 * delta_hitch_angle_start_rad  - lambda_2 * delta_hitch_angle_rad)

	steering_angle = math.degrees(math.atan(u))

	# Begrenzung des maximalen Lenkwinkels zur naturgetreuen Darstellung --> 25° ist nur geschätzer Wert
	if steering_angle > 25:
		steering_angle = 25
	if steering_angle < -25:
		steering_angle = -25

	return steering_angle



# Mathematische Teilfunktion für die Lenkwinkelberechnung
def g_phi(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle):
	
	current_hitch_angle_rad = math.radians(current_hitch_angle)
	
	g = (current_velocity / l_truck) - (current_velocity * l_hitch * math.cos(current_hitch_angle_rad)) / (l_truck * l_trailer)

	return g



# Mathematische Teilfunktion für die Lenkwinkelberechnung
def f_phi(current_velocity, l_trailer, current_hitch_angle, hitch_angle_target):
	
	current_hitch_angle_rad = math.radians(current_hitch_angle)

	f = 0 + (current_velocity * math.sin(current_hitch_angle_rad)) / l_trailer

	return f


# Instanz der Klasse
mySubs = Ros_Subscriber()



# Subscriber
sub_rear_distance.subscribe(mySubs.callback_rear_distance)	
sub_hitch_angle.subscribe(mySubs.callback_hitch_angle)
sub_velocity.subscribe(mySubs.callback_velocity)



# Erstellen der Log Datei --> Neue Datei mit Datum und Zeitpunkt des Erstellens wird im Ordner log_files erstellt
if log_mode_on == True: 
	logfile = log_file_init(timestr, "J", hitch_angle_target, vel_target, lambda_1, lambda_2, l_hitch, l_truck, l_trailer)

# Regeldifferenz zu Beginn in rad
delta_hitch_angle_start_rad = math.radians(hitch_angle_target - mySubs.get_hitch_angle()) 

# Main Loop
loop_count = 0;
log_count = 0;

# Abbruch Kriterium
while time.clock() < 40:
	# Ausführen der Regelung und der Ros-Befehle alle 10.000 Cyclen zur Begrenzung des Rechenaufwandes
	if loop_count == 10000:
		current_steer_angle = calc_steer_angle_reichenegger(mySubs.get_velocity(), l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target, delta_hitch_angle_start_rad, lambda_1, lambda_2)
		pub_velocity_control.publish(roslibpy.Message({'data':float(vel_target)}))		
		pub_steering_control.publish(roslibpy.Message({'data':int(current_steer_angle)}))
		print("Zeit:", time.clock(), "Geschwindigkeit:", mySubs.get_velocity(), "Lenkwinkel:" , current_steer_angle, "Hitch Winkel:", mySubs.get_hitch_angle())
		loop_count = 0
		log_count = log_count + 1

	# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_file_logger(logfile, time.clock(), mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target)
		log_count = 0
	loop_count = loop_count + 1
	


log_file_shut_down(logfile)
	
