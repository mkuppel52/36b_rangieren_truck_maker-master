import time
import roslibpy
import math

# Definition LKW Parameter
l_truck = 3.57 # Länge Zugmaschine --> Achse zu Achse
l_hitch = 0.95 # Länge Hinterachse zu Auflagepunkt
l_trailer = 7.75 # Trailer Auflagepunkt bis Achse

# Definition Regelparameter
hitch_angle_goal = -20.0
vel_goal = 10.0
vel_o = vel_goal / 3.6
lambda_1 = 0.05
lambda_2 = 10.7
delta_phi_start = 0.0 # nur hier mit 0.0 initialisiert, wird später in Main entsprechend der Startbedingung initialisiert



# Client für Ros --> Rosbride mit WebSocket muss gelauncht werden --> roslaunch rosbridge_server rosbridge_websocket.launch
client = roslibpy.Ros(host='localhost', port=9090)
client.run()

# Definition der Topics die veröffentlich werden
pub_velocity_control = roslibpy.Topic(client, '/velocity_control', 'std_msgs/Float64')
pub_steering_control = roslibpy.Topic(client, '/steering_control', 'std_msgs/Int32')

# Definition der zu veröffentlichen Variablen
#current_vel = 0.0 # Truckgeschwindigkeit	
current_steer_angle = 0 # Lenkwinkel Truck vorne


# Definition der Topics die abonniert werden
sub_rear_distance =roslibpy.Topic(client, '/rear_distance', 'std_msgs/Float32')
sub_hitch_angle =roslibpy.Topic(client, '/hitch_angle', 'std_msgs/Float64')
sub_hitch_angle_vel =roslibpy.Topic(client, '/hitch_angle_vel', 'std_msgs/Float64')
sub_velocity = roslibpy.Topic(client, '/velocity', 'std_msgs/Float64')



class subs (object):

	current_velocity = 0.0
	current_rear_distance = 0.0
		
	current_hitch_angle = 0.0 # Winkel zwischen Truck und Trailer
	current_hitch_angle_vel = 0.0 # Winkelgeschwindigkeit zwischen Truck und Trailer

	def callback_velocity(self,msg):
		 self.current_velocity = msg['data']
	
	def callback_rear_distance(self,msg):
		self.current_rear_distance = msg['data']

	def callback_hitch_angle(self,msg): 
		self.current_hitch_angle = msg['data']

	def callback_hitch_angle_vel(self,msg): 
		self.current_hitch_angle_vel = msg['data']

	def get_velocity(self):
		return self.current_velocity

	def get_rear_distance(self):
		return self.current_rear_distance	

	def get_hitch_angle(self):
		return self.current_hitch_angle
	
	def get_hitch_angle_vel(self):
		return self.current_hitch_angle_vel


# Function to calculate resulting velocity
def calc_velocity():
	vel_out = 10
	if time.clock() > 5 and time.clock() < 10:
		vel_out = 0
	if time.clock() > 10 and time.clock() < 15:
		vel_out = 10
	return vel_out
	
# Function to calculate resulting steering angle
def calc_steer_angle(vel_in, l_hitch, l_truck, l_trailer, current_hitch_angle, hitch_angle_goal, delta_hitch_angle_start, lambda_1, lambda_2):
	steering_angle = 0.0
	#if vel_in > 0.0001:	
	u = (1 / g_phi(vel_in, l_hitch, l_truck, l_trailer, current_hitch_angle)) * (-f_phi(vel_in, l_trailer, current_hitch_angle, hitch_angle_goal) - lambda_1 * delta_hitch_angle_start - 	lambda_2 * math.radians((hitch_angle_goal - current_hitch_angle)))
	steering_angle = math.degrees(math.atan(u))
	if steering_angle > 25:
		steering_angle = 25
	if steering_angle < -25:
		steering_angle = -25
	return steering_angle

# Mathematische Teilfunktion für die Lenkwinkelberechnung
def g_phi(vel_in, l_hitch, l_truck, l_trailer, current_hitch_angle):
	g = (vel_in / l_truck) - (vel_in * l_hitch * math.cos(math.radians(current_hitch_angle))) / (l_truck * l_trailer)
	return g

# Mathematische Teilfunktion für die Lenkwinkelberechnung
def f_phi(vel_in, l_trailer, current_hitch_angle, hitch_angle_goal):
	f = math.radians(hitch_angle_goal) + (vel_in * math.sin(math.radians(current_hitch_angle))) / l_trailer
	return f	


# Instanz der Klasse
mySubs = subs()


# Subscriber
sub_rear_distance.subscribe(mySubs.callback_rear_distance)	
sub_hitch_angle.subscribe(mySubs.callback_hitch_angle)
sub_velocity.subscribe(mySubs.callback_velocity)



# Main Loop
delta_hitch_angle_start = math.radians(hitch_angle_goal - mySubs.get_hitch_angle()) # Regeldifferenz zu Beginn in rad
loop_count = 0;
while time.clock() < 100:
	if loop_count == 10000:
		current_steer_angle = calc_steer_angle(vel_o, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_goal, delta_hitch_angle_start, lambda_1, lambda_2)
		pub_velocity_control.publish(roslibpy.Message({'data':float(vel_goal)}))		
		pub_steering_control.publish(roslibpy.Message({'data':int(current_steer_angle)}))
		print("Zeit:", time.clock(), "Geschwindigkeit:", mySubs.get_velocity(), "Lenkwinkel:" , current_steer_angle, "Hitch Winkel:", mySubs.get_hitch_angle())
		loop_count = 0
	loop_count = loop_count + 1
	


	
	
