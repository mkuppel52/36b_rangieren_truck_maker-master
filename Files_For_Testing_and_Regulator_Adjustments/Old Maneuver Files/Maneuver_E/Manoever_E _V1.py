import time
import roslibpy
import math
from log_maneuvers import log_file_init
from log_maneuvers import log_file_logger
from log_maneuvers import log_file_shut_down

timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = True
# Desired Hitch Angle between Truck and Trailer
hitch_angle_target_1 = 0.0

# Definition LKW Parameter
l_truck = 3.57 # Länge Zugmaschine --> Achse zu Achse
l_hitch = 0.95 # Länge Hinterachse zu Auflagepunkt
l_trailer = 7.75 # Trailer Auflagepunkt bis Achse
l_complete = 15.00 # Gesamtlänge Truck + Trailer
b_trailer = 2.5 # Breite Anhänger
sensor_angle = 10.0 # in Grad
gear_drive = 1
gear_back = 3
gear_neutral = 0

# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6
# Gewichtungsfaktoren des Reglers --> Wurdem empirisch ermittelt
lambda_1 = 0.05
lambda_2 = 18.0



# Client für Ros --> Rosbride mit WebSocket muss gelauncht werden --> roslaunch rosbridge_server rosbridge_websocket.launch
client = roslibpy.Ros(host='localhost', port=9090)
client.run()

# Definition der Topics die veröffentlich werden
pub_velocity_control = roslibpy.Topic(client, '/velocity_control', 'std_msgs/Float64')
pub_steering_control = roslibpy.Topic(client, '/steering_control', 'std_msgs/Int32')
pub_drive_mode_control = roslibpy.Topic(client, '/drive_mode_control', 'std_msgs/Int32')

# Definition der zu veröffentlichen Variablen
#current_vel = 0.0 # Truckgeschwindigkeit	
current_steer_angle = 0 # Lenkwinkel Truck vorne


# Definition der Topics die abonniert werden
sub_rear_distance =roslibpy.Topic(client, '/rear_distance', 'std_msgs/Float32')
sub_hitch_angle =roslibpy.Topic(client, '/hitch_angle', 'std_msgs/Float64')
sub_hitch_angle_vel =roslibpy.Topic(client, '/hitch_angle_vel', 'std_msgs/Float64')
sub_velocity = roslibpy.Topic(client, '/velocity', 'std_msgs/Float64')
sub_maneuver_time = roslibpy.Topic(client, '/maneuver_time', 'std_msgs/Float64')
sub_side_truck_distance = roslibpy.Topic(client, '/side_truck_distance', 'std_msgs/Float64')
sub_rear_trailer_distance = roslibpy.Topic(client, '/rear_trailer_distance', 'std_msgs/Float64')
sub_side_trailer_distance = roslibpy.Topic(client, '/side_trailer_distance', 'std_msgs/Float64')




# Klasse zum zwischen speichern der Subscribe Messages
class subs (object):

	current_velocity = 0.0	# Geschwindigkeit des Trucks
	current_rear_distance = 0.0 # Abstand nach Hinten zu detektierten Objekten
		
	current_hitch_angle = 0.0 # Winkel zwischen Truck und Trailer
	current_hitch_angle_vel = 0.0 # Winkelgeschwindigkeit zwischen Truck und Trailer

	current_maneuver_time = 0.0

	current_side_truck_distance  = 0.0
	current_rear_trailer_distance  = 0.0
	current_side_trailer_distance  = 0.0



	def callback_velocity(self,msg):
		 self.current_velocity = msg['data']

	def callback_hitch_angle(self,msg): 
		self.current_hitch_angle = msg['data']

	def callback_hitch_angle_vel(self,msg): 
		self.current_hitch_angle_vel = msg['data']

	def callback_maneuver_time(self,msg): 
		self.current_maneuver_time = msg['data']

	def callback_side_truck_distance(self,msg): 
		self.current_side_truck_distance = msg['data']

	def callback_rear_trailer_distance(self,msg): 
		self.current_rear_trailer_distance = msg['data']

	def callback_side_trailer_distance(self,msg): 
		self.current_side_trailer_distance = msg['data']

	def get_velocity(self):
		return self.current_velocity	

	def get_hitch_angle(self):
		return self.current_hitch_angle
	
	def get_hitch_angle_vel(self):
		return self.current_hitch_angle_vel

	def get_maneuver_time(self):
		return self.current_maneuver_time
	
	def get_side_truck_distance(self):
		return self.current_side_truck_distance

	def get_rear_trailer_distance(self):
		return self.current_rear_trailer_distance

	def get_side_trailer_distance(self):
		return self.current_side_trailer_distance


	
# Regler nach Reichenegger --> Funktion zur Lenkwinkelberechnung für die Regelung des Knickwinkels auf konstanten Wert
def calc_steer_angle_reichenegger(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle, hitch_angle_target, delta_hitch_angle_start_rad, lambda_1, lambda_2):
	
	delta_hitch_angle_rad = math.radians(hitch_angle_target - current_hitch_angle)

	g = g_phi(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle)
	f = f_phi(current_velocity, l_trailer, current_hitch_angle, hitch_angle_target)

	# Verhindert das bei u durch 0 geteilt wird	
	if g == 0:
		g = 0.00001

	# Formel zur Berechnung des Lenkwinkels	
	u = (1 / g) * (-f - lambda_1 * delta_hitch_angle_start_rad  - lambda_2 * delta_hitch_angle_rad)

	steering_angle = math.degrees(math.atan(u))

	# Begrenzung des maximalen Lenkwinkels zur naturgetreuen Darstellung --> 25° ist nur geschätzer Wert
	if steering_angle > 25:
		steering_angle = 25
	if steering_angle < -25:
		steering_angle = -25

	return steering_angle



# Mathematische Teilfunktion für die Lenkwinkelberechnung
def g_phi(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle):
	
	current_hitch_angle_rad = math.radians(current_hitch_angle)
	
	g = (current_velocity / l_truck) - (current_velocity * l_hitch * math.cos(current_hitch_angle_rad)) / (l_truck * l_trailer)

	return g



# Mathematische Teilfunktion für die Lenkwinkelberechnung
def f_phi(current_velocity, l_trailer, current_hitch_angle, hitch_angle_target):
	
	current_hitch_angle_rad = math.radians(current_hitch_angle)

	f = 0 + (current_velocity * math.sin(current_hitch_angle_rad)) / l_trailer

	return f

# Numerische Lösung um von dem Radius der Trajektorie auf einen Knickwinkel zu kommen
def numeric_radius_to_hitch (l_trailer, l_hitch, radius):
	radius_try = 0.0
	angle = 0.1
	angle_step = 0.001
	r_tolerance = 0.1

	while True:
		if angle < 0 or angle > 0:
			radius_try = (l_trailer - (l_hitch / math.cos(angle))) * (1 / math.tan(angle))
		angle = angle + angle_step
		if radius_try < radius + r_tolerance and radius_try > radius - r_tolerance:
			print(radius_try)			
			return angle

# Geschwindigkeit der Hinterachse des Trailers
def calc_trailer_velocity (hitch_angle, vel_truck, l_trailer, radius_trailer):
	vel_trailer = vel_truck * (radius_trailer / math.sqrt(radius_trailer**2 + l_trailer**2))
	if vel_trailer < 0:
		vel_trailer = -vel_trailer
	return vel_trailer





# Instanz der Klasse
mySubs = subs()


# Subscriber	
sub_hitch_angle.subscribe(mySubs.callback_hitch_angle)
sub_velocity.subscribe(mySubs.callback_velocity)
sub_maneuver_time.subscribe(mySubs.callback_maneuver_time)
sub_side_truck_distance.subscribe(mySubs.callback_side_truck_distance)
sub_rear_trailer_distance.subscribe(mySubs.callback_rear_trailer_distance)
sub_side_trailer_distance.subscribe(mySubs.callback_side_trailer_distance)



# Erstellen der Log Datei --> Neue Datei mit Datum und Zeitpunkt des Erstellens wird im Ordner log_files erstellt
if log_mode_on == True: 
	logfile = log_file_init(timestr, "E", hitch_angle_target_1 ,vel_target, lambda_1, lambda_2, l_hitch, l_truck, l_trailer)

# Regeldifferenz zu Beginn in rad
delta_hitch_angle_start_1 = math.radians(hitch_angle_target_1 - mySubs.get_hitch_angle()) 
#delta_hitch_angle_start_3 = math.radians(hitch_angle_target_3 - mySubs.get_hitch_angle())


# Warten bis Ros-Kommunikation vollständig da ist
while mySubs.get_maneuver_time() == 0.0:
	maneuver_time_start = mySubs.get_maneuver_time()


maneuver_1 = True
maneuver_2 = True

vehicle1_is_detected = False
vehicle2_is_detected = False

time_point_1 = 0.0
time_point_2 = 0.0
time_point_3 = 0.0
time_point_4 = 0.0

d_point_1 = 0.0
d_point_2 = 0.0
d_point_3 = 0.0


l_park = 0.0
d_sensor_side_truck_trailer_axle = l_trailer - l_hitch



# Main Loop
loop_count = 0;
log_count = 0;


# Parklücke vermessen beim parallelen Vorbeifahren
while maneuver_1 == True:
	# Ausführen der Regelung und der Ros-Befehle alle 10.000 Cyclen zur Begrenzung des Rechenaufwandes
	if loop_count == 10000:
		# Berechnung des benötigten Lenkwinkels aufgrund der Regelung
		current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target_1, delta_hitch_angle_start_1, lambda_1, lambda_2)


		pub_drive_mode_control.publish(roslibpy.Message({'data':int(gear_drive)}))
		pub_velocity_control.publish(roslibpy.Message({'data':float(vel_target)}))
		# Lenkwinkel muss invertiert werden, da nun vorwärts gefahren wird		
		pub_steering_control.publish(roslibpy.Message({'data':int(-current_steer_angle)}))
		#print("Zeit:", mySubs.get_maneuver_time() - maneuver_time_start, "Geschwindigkeit:", mySubs.get_velocity(), "Lenkwinkel:" , current_steer_angle, "Hitch Winkel:", mySubs.get_hitch_angle())


		loop_count = 0
		log_count = log_count + 1


	# Anfang des ersten Fahrzeugs der Parklücke
	if mySubs.get_side_truck_distance() > 0.0 and vehicle1_is_detected == False and time_point_1 == 0.0:
		vehicle1_is_detected = True
		d_point_1 = mySubs.get_side_truck_distance()
		s_1 = d_point_1 * math.tan(math.radians(sensor_angle) * 0.5)

	# Ende des ersten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeug nötig für die Parklückenberechnung
	if mySubs.get_side_truck_distance() == 0.0 and vehicle1_is_detected == True and time_point_1 == 0.0:
		time_point_1 = mySubs.get_maneuver_time() - maneuver_time_start
		vehicle1_is_detected = False

	# Anfang des zweiten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeugmm nötig für die Parklückenberechnung
	if mySubs.get_side_truck_distance() > 0.0 and vehicle2_is_detected == False and time_point_1 > 0.0:
		vehicle2_is_detected = True		
		time_point_2 = mySubs.get_maneuver_time() - maneuver_time_start
		d_point_2 = mySubs.get_side_truck_distance()
		print(d_point_2)



	# Berechnung, wie der Truck nach der Parklückenvermessung noch weiter fahren muss
	if time_point_2 > 0:
		s_2 = d_point_2 * math.tan(math.radians(sensor_angle) * 0.5)
		time_point_3 = s_2 / mySubs.get_velocity() + time_point_2

	# Berechnung, wie der Truck nach der Parklückenvermessung noch weiter fahren muss
	if time_point_3 > 0 and mySubs.get_maneuver_time() - maneuver_time_start > time_point_3 and time_point_4 == 0:
		d_point_3 = mySubs.get_side_truck_distance()
		time_point_4 = (d_point_3 * 2 + l_trailer-l_hitch) / mySubs.get_velocity() + time_point_3
		print(d_point_3)


	# Endpunkt des Vorwärtsfahren wird erreicht --> Beenden des Maneuvers
	if time_point_4 > 0 and mySubs.get_maneuver_time() - maneuver_time_start > time_point_4:
		maneuver_1 = False



	# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_file_logger(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0
	loop_count = loop_count + 1



# Anhalten
pub_velocity_control.publish(roslibpy.Message({'data':float(0.0)}))

time_roll_out_start = mySubs.get_maneuver_time() - maneuver_time_start
time_roll_out_previous = 0.0
time_roll_out_current = 0.0
path_roll_out_current = 0.0
path_roll_out_complete = 0.0

print("now")

# Messen der Distanz die beim Ausrollen noch zurückgelegt wird 
while mySubs.get_velocity() > 0.1:
	
	# Aufrecht erhalten des Anhalten-Befehls
	current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target_1, delta_hitch_angle_start_1, lambda_1, lambda_2)

	if loop_count == 10000:	
		pub_steering_control.publish(roslibpy.Message({'data':int(-current_steer_angle)}))
		pub_velocity_control.publish(roslibpy.Message({'data':float(0.0)}))
		loop_count = 0
		log_count = log_count+1


	time_roll_out_current = mySubs.get_maneuver_time() - maneuver_time_start - time_roll_out_start	
	
	path_roll_out_current = mySubs.get_velocity() * (time_roll_out_current - time_roll_out_previous)
	
	path_roll_out_complete = path_roll_out_complete + path_roll_out_current

	time_roll_out_previous = time_roll_out_current

	loop_count = loop_count + 1

	# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_file_logger(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0


# Wechsel des Drive Modus auf Rückwärts
pub_drive_mode_control.publish(roslibpy.Message({'data':int(gear_back)}))



time_acc_start = mySubs.get_maneuver_time() - maneuver_time_start
time_acc_previous = 0.0
time_acc_current = 0.0
path_acc_current = 0.0
path_acc_complete = 0.0

delta_hitch_angle_start_1 = math.radians(hitch_angle_target_1 - mySubs.get_hitch_angle()) 

# Rückfahren der ausgerolten Distanz
while path_acc_complete < path_roll_out_complete:
		
	if loop_count == 10000:	
		# Aufrecht erhalten des Anfahren-Befehls
		#pub_drive_mode_control.publish(roslibpy.Message({'data':int(gear_back)}))
		pub_velocity_control.publish(roslibpy.Message({'data':float(vel_target)})) 
		current_steer_angle = 0.0
		current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target_1, delta_hitch_angle_start_1, lambda_1, lambda_2)	
		pub_steering_control.publish(roslibpy.Message({'data':int(current_steer_angle)}))

		#print("Zeit:", mySubs.get_maneuver_time() - maneuver_time_start, "Geschwindigkeit:", mySubs.get_velocity(), "Lenkwinkel:" , current_steer_angle, "Hitch Winkel:", mySubs.get_hitch_angle())
		loop_count = 0
		log_count = log_count+1


	loop_count = loop_count + 1

	time_acc_current = mySubs.get_maneuver_time() - maneuver_time_start - time_acc_start	
	
	path_acc_current = mySubs.get_velocity() * (time_acc_current - time_acc_previous)
	
	path_acc_complete = path_acc_complete + path_acc_current

	time_acc_previous = time_acc_current


	# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_file_logger(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0




# Berechnung der Trajectorie und dafür erforderliche Parameter
l_park = vel_target_mps * (time_point_2 - time_point_1) + s_1 + s_2 # Länge der Parklücke
b_park = d_point_2 - d_point_3 # Tiefe der Parklücke
d_center_truck_park = b_trailer * 0.5 + d_point_3 + b_park * 0.5 # Abstand Truck Mitte zu Mitte der Parklückentiefe
s_parked = (l_park - l_complete) * 0.5 # Distanz Trailer Ende und Parklücken Ende nach dem Parkvorgang
s_trailer_rear_park = 2* d_point_3 # Distanz Trailer Ende und Parklückenanfang vor dem Parkvorgang
l_trajectory = math.sqrt(d_center_truck_park ** 2 + s_trailer_rear_park ** 2 + (l_park - s_parked) ** 2) # Luftlinie zwischen Trailer Ende vor und nach dem Einparken
a_park_rad = math.atan(d_center_truck_park / (l_park - s_parked + s_trailer_rear_park)) # Winkel der Trajektorien-Luftline und Parklücke in rad
r_trajectory = (l_trajectory / 4) * 1 / math.cos((math.pi / 2) - a_park_rad) # Radien der Trajektorie
bgl_trajectory = r_trajectory * 2 * a_park_rad  # Bogenlänge der Radien für die Trajektorie




hitch_angle_target_2 = math.degrees(numeric_radius_to_hitch(l_trailer, l_hitch, r_trajectory))
delta_hitch_angle_start_2 = math.radians(hitch_angle_target_2 - mySubs.get_hitch_angle())

print("Parklucke: ", l_park, "           ", "d3: ", d_point_3,  "           " "s: ", s_trailer_rear_park,  "           ", "Radius:               ", r_trajectory, "Bogenlange:               ", bgl_trajectory)

time_park_1_start = mySubs.get_maneuver_time() - maneuver_time_start
time_park_1_previous = 0.0
time_park_1_current = 0.0
path_park_1_current = 0.0
path_park_1_complete = 0.0

loop_count = 0


pub_drive_mode_control.publish(roslibpy.Message({'data':int(gear_back)}))
print("Park1")
while path_park_1_complete < bgl_trajectory:
	
	if loop_count == 10000:	
		current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), -hitch_angle_target_2, delta_hitch_angle_start_2, lambda_1, lambda_2)
		pub_velocity_control.publish(roslibpy.Message({'data':float(vel_target)}))		
		pub_steering_control.publish(roslibpy.Message({'data':int(current_steer_angle)}))
		pub_drive_mode_control.publish(roslibpy.Message({'data':int(gear_back)}))
		loop_count = 0
		log_count = log_count+1

		time_park_1_current = mySubs.get_maneuver_time() - maneuver_time_start - time_park_1_start	
	
		path_park_1_current = calc_trailer_velocity(mySubs.get_hitch_angle(), mySubs.get_velocity(), l_trailer, r_trajectory) * (time_park_1_current - time_park_1_previous)
	
		path_park_1_complete = path_park_1_complete + path_park_1_current

		time_park_1_previous = time_park_1_current

		print("Park 1:", "Path_Park_1:" , path_park_1_complete, "Diff:", str( bgl_trajectory - path_park_1_complete), "          calc_trailer_vel:", calc_trailer_velocity(mySubs.get_hitch_angle(), mySubs.get_velocity(), l_trailer, r_trajectory))

	loop_count = loop_count + 1





# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_file_logger(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0



time_park_2_start = mySubs.get_maneuver_time() - maneuver_time_start
time_park_2_previous = 0.0
time_park_2_current = 0.0
path_park_2_current = 0.0
path_park_2_complete = 0.0


pub_drive_mode_control.publish(roslibpy.Message({'data':int(gear_back)}))
print("Park2")
while path_park_2_complete < bgl_trajectory:
	
	if loop_count == 10000:	
		current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target_2, delta_hitch_angle_start_2, lambda_1, lambda_2)
		pub_velocity_control.publish(roslibpy.Message({'data':float(vel_target)}))		
		pub_steering_control.publish(roslibpy.Message({'data':int(current_steer_angle)}))
		pub_drive_mode_control.publish(roslibpy.Message({'data':int(gear_back)}))
		loop_count = 0
		log_count = log_count+1

		print("Park 2:", "Path_Park_2:" , path_park_2_complete, "Diff:", str(bgl_trajectory - path_park_2_complete), "          calc_trailer_vel:", calc_trailer_velocity(mySubs.get_hitch_angle(), mySubs.get_velocity(), l_trailer, r_trajectory))


		time_park_2_current = mySubs.get_maneuver_time() - maneuver_time_start - time_park_2_start	
	
		path_park_2_current = calc_trailer_velocity(mySubs.get_hitch_angle(), mySubs.get_velocity(), l_trailer, r_trajectory) * (time_park_2_current - time_park_2_previous)
	
		path_park_2_complete = path_park_2_complete + path_park_2_current

		time_park_2_previous = time_park_2_current

	loop_count = loop_count + 1


# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_file_logger(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0


time_park_3_start = mySubs.get_maneuver_time() - maneuver_time_start
time_park_3_previous = 0.0
time_park_3_current = 0.0
path_park_3_current = 0.0
path_park_3_complete = 0.0

hitch_angle_target_3 = 0.0
delta_hitch_angle_start_3 = math.radians(hitch_angle_target_3 - mySubs.get_hitch_angle())


pub_drive_mode_control.publish(roslibpy.Message({'data':int(gear_back)}))

print("Park3")
while path_park_3_complete < s_parked:
	
	if loop_count == 10000:	

		hitch_angle_target_3 = (hitch_angle_target_2 - hitch_angle_target_1) * 0.5

		if path_park_3_complete > 0.5 * s_parked:
			hitch_angle_target_3 = hitch_angle_target_1

		current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target_3, delta_hitch_angle_start_3, lambda_1, lambda_2)
		pub_velocity_control.publish(roslibpy.Message({'data':float(vel_target)}))		
		pub_steering_control.publish(roslibpy.Message({'data':int(current_steer_angle)}))
		pub_drive_mode_control.publish(roslibpy.Message({'data':int(gear_back)}))
		loop_count = 0
		log_count = log_count+1

		print("Park 3:", str( s_parked - path_park_3_complete))

		time_park_3_current = mySubs.get_maneuver_time() - maneuver_time_start - time_park_3_start	
	
		path_park_3_current = calc_trailer_velocity(mySubs.get_hitch_angle(), mySubs.get_velocity(), l_trailer, r_trajectory) * (time_park_3_current - time_park_3_previous)
	
		path_park_3_complete = path_park_3_complete + path_park_3_current

		time_park_3_previous = time_park_3_current


	loop_count = loop_count + 1


# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_file_logger(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0
	
	
log_file_shut_down(logfile)
	
