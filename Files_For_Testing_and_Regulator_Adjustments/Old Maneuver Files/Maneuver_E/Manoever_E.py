import time
import roslibpy
import math
from log_maneuvers import log_file_init
from log_maneuvers import log_vehicle_param
from log_maneuvers import log_test_param
from log_maneuvers import log_hitch_angle
from log_maneuvers import log_file_shut_down
from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub

timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = False

# Desired Hitch Angle between Truck and Trailer
hitch_angle_target_1 = 0.0

# Definition LKW Parameter
l_truck = 3.57 # Länge Zugmaschine --> Achse zu Achse
l_hitch = 0.95 # Länge Hinterachse zu Auflagepunkt
l_trailer = 7.75 # Trailer Auflagepunkt bis Achse
l_complete = 15.00 # Gesamtlänge Truck + Trailer
b_trailer = 2.5 # Breite Anhänger
sensor_angle = 10.0 # in Grad
gear_drive = 1
gear_back = 3
gear_neutral = 0

# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6

# Definition der zu veröffentlichen Variablen	
current_steer_angle = 0 # Lenkwinkel Truck vorne


def delta_drive_distance(current_time, previous_time, velocity):
	drive_distance_new = (current_time - previous_time) * velocity
	return drive_distance_new


# Numerische Lösung um von dem Radius der Trajektorie auf einen Knickwinkel zu kommen
def numeric_radius_to_hitch (l_trailer, l_hitch, radius):
	radius_try = 0.0
	angle = 0.1
	angle_step = 0.001
	r_tolerance = 0.1

	while True:
		if angle < 0 or angle > 0:
			radius_try = (l_trailer - (l_hitch / math.cos(angle))) * (1 / math.tan(angle))
		angle = angle + angle_step
		if radius_try < radius + r_tolerance and radius_try > radius - r_tolerance:
			print(radius_try)			
			return angle

# Geschwindigkeit der Hinterachse des Trailers
def calc_trailer_velocity (hitch_angle, vel_truck, l_trailer, radius_trailer):
	vel_trailer = vel_truck * (radius_trailer / math.sqrt(radius_trailer**2 + l_trailer**2))
	if vel_trailer < 0:
		vel_trailer = -vel_trailer
	return vel_trailer



# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Starten des Ros-Clients
myClient.start_client()


# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()


# Erstellen der Log Datei --> Neue Datei mit Datum und Zeitpunkt des Erstellens wird im Ordner log_files erstellt
if log_mode_on == True: 
	logfile = log_file_init(timestr, "E")
	log_vehicle_param(logfile, l_hitch, l_truck, l_trailer)
	log_test_param(logfile, hitch_angle_target_1 ,vel_target, 0.05, 18.0,)

# Regeldifferenz zu Beginn in rad
delta_hitch_angle_start_1 = math.radians(hitch_angle_target_1 - mySubs.get_hitch_angle()) 
#delta_hitch_angle_start_3 = math.radians(hitch_angle_target_3 - mySubs.get_hitch_angle())


# Warten bis Ros-Kommunikation vollständig da ist
while mySubs.get_maneuver_time() == 0.0:
	maneuver_time_start = mySubs.get_maneuver_time()


maneuver_1 = True
maneuver_2 = True

vehicle1_is_detected = False
vehicle2_is_detected = False

time_point_1 = 0.0
time_point_2 = 0.0
time_point_3 = 0.0
time_point_4 = 0.0

d_point_1 = 0.0
d_point_2 = 0.0
d_point_3 = 0.0


l_park = 0.0
d_sensor_side_truck_trailer_axle = l_trailer - l_hitch



# Main Loop
loop_count = 0;
log_count = 0;


# Parklücke vermessen beim parallelen Vorbeifahren
while maneuver_1 == True:
	# Ausführen der Regelung und der Ros-Befehle alle 10.000 Cyclen zur Begrenzung des Rechenaufwandes
	if loop_count == 10000:
		# Berechnung des benötigten Lenkwinkels aufgrund der Regelung
		current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target_1, delta_hitch_angle_start_1)

		# Lenkwinkel muss invertiert werden, da nun vorwärts gefahren wird
		myPub.pub_velc_stc_dmc(vel_target, -current_steer_angle, gear_drive)

		#print("Zeit:", mySubs.get_maneuver_time() - maneuver_time_start, "Geschwindigkeit:", mySubs.get_velocity(), "Lenkwinkel:" , current_steer_angle, "Hitch Winkel:", mySubs.get_hitch_angle())

		loop_count = 0
		log_count = log_count + 1


	# Anfang des ersten Fahrzeugs der Parklücke
	if mySubs.get_side_truck_distance() > 0.0 and vehicle1_is_detected == False and time_point_1 == 0.0:
		vehicle1_is_detected = True
		d_point_1 = mySubs.get_side_truck_distance()
		s_1 = d_point_1 * math.tan(math.radians(sensor_angle) * 0.5)

	# Ende des ersten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeug nötig für die Parklückenberechnung
	if mySubs.get_side_truck_distance() == 0.0 and vehicle1_is_detected == True and time_point_1 == 0.0:
		time_point_1 = mySubs.get_maneuver_time() - maneuver_time_start
		vehicle1_is_detected = False

	# Anfang des zweiten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeugmm nötig für die Parklückenberechnung
	if mySubs.get_side_truck_distance() > 0.0 and vehicle2_is_detected == False and time_point_1 > 0.0:
		vehicle2_is_detected = True		
		time_point_2 = mySubs.get_maneuver_time() - maneuver_time_start
		d_point_2 = mySubs.get_side_truck_distance()
		print(d_point_2)



	# Berechnung, wie der Truck nach der Parklückenvermessung noch weiter fahren muss
	if time_point_2 > 0:
		s_2 = d_point_2 * math.tan(math.radians(sensor_angle) * 0.5)
		time_point_3 = s_2 / mySubs.get_velocity() + time_point_2

	# Berechnung, wie der Truck nach der Parklückenvermessung noch weiter fahren muss
	if time_point_3 > 0 and mySubs.get_maneuver_time() - maneuver_time_start > time_point_3 and time_point_4 == 0:
		d_point_3 = mySubs.get_side_truck_distance()
		time_point_4 = (d_point_3 * 2 + l_trailer-l_hitch) / mySubs.get_velocity() + time_point_3
		print(d_point_3)


	# Endpunkt des Vorwärtsfahren wird erreicht --> Beenden des Maneuvers
	if time_point_4 > 0 and mySubs.get_maneuver_time() - maneuver_time_start > time_point_4:
		maneuver_1 = False



	# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_file_logger(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0
	loop_count = loop_count + 1



# Anhalten
myPub.pub_velc(0.0)


time_prev = mySubs.get_maneuver_time()
drive_distance = 0.0

print("now")

# Messen der Distanz die beim Ausrollen noch zurückgelegt wird 
while mySubs.get_velocity() > 0.1:
	
	# Aufrecht erhalten des Anhalten-Befehls
	current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target_1, delta_hitch_angle_start_1)

	if loop_count == 10000:	
		myPub.pub_velc_stc_dmc(0.0, -current_steer_angle, gear_drive)

		drive_distance = drive_distance + delta_drive_distance(mySubs.get_maneuver_time(), time_prev, mySubs.get_velocity())
		time_prev = mySubs.get_maneuver_time()
		
		loop_count = 0
		log_count = log_count+1
	loop_count = loop_count + 1

	# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_hitch_angle(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0


drive_distance_roll_out = drive_distance

# Wechsel des Drive Modus auf Rückwärts
myPub.pub_dmc(gear_back)




delta_hitch_angle_start_1 = math.radians(hitch_angle_target_1 - mySubs.get_hitch_angle()) 

time_prev = mySubs.get_maneuver_time()
drive_distance = 0.0

# Rückfahren der ausgerolten Distanz
while drive_distance < drive_distance_roll_out:
		
	if loop_count == 10000:	
		# Aufrecht erhalten des Anfahren-Befehls
		current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target_1, delta_hitch_angle_start_1)
		
		myPub.pub_velc_stc_dmc(vel_target, current_steer_angle, gear_back)


		drive_distance = drive_distance + delta_drive_distance(mySubs.get_maneuver_time(), time_prev, mySubs.get_velocity())
		time_prev = mySubs.get_maneuver_time()	

		#print("Zeit:", mySubs.get_maneuver_time() - maneuver_time_start, "Geschwindigkeit:", mySubs.get_velocity(), "Lenkwinkel:" , current_steer_angle, "Hitch Winkel:", mySubs.get_hitch_angle())	

		loop_count = 0
		log_count = log_count+1
	loop_count = loop_count + 1


	# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_hitch_angle(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0




# Berechnung der Trajectorie und dafür erforderliche Parameter
l_park = vel_target_mps * (time_point_2 - time_point_1) + s_1 + s_2 # Länge der Parklücke

b_park = d_point_2 - d_point_3 # Tiefe der Parklücke

d_center_truck_park = b_trailer * 0.5 + d_point_3 + b_park * 0.5 # Abstand Truck Mitte zu Mitte der Parklückentiefe

s_parked = (l_park - l_complete) * 0.5 # Distanz Trailer Ende und Parklücken Ende nach dem Parkvorgang

s_trailer_rear_park = 2* d_point_3 # Distanz Trailer Ende und Parklückenanfang vor dem Parkvorgang

l_trajectory = math.sqrt(d_center_truck_park ** 2 + (s_trailer_rear_park +l_park - s_parked) ** 2) # Luftlinie zwischen Trailer Ende vor und nach dem Einparken

a_park_rad = math.atan(d_center_truck_park / (l_park - s_parked + s_trailer_rear_park)) # Winkel der Trajektorien-Luftline und Parklücke in rad

r_trajectory = (l_trajectory / 4) * 1 / math.cos((math.pi / 2) - a_park_rad) # Radien der Trajektorie

bgl_trajectory = r_trajectory * 2 * a_park_rad  # Bogenlänge der Radien für die Trajektorie




hitch_angle_target_2 = math.degrees(numeric_radius_to_hitch(l_trailer, l_hitch, r_trajectory))
delta_hitch_angle_start_2 = math.radians(hitch_angle_target_2 - mySubs.get_hitch_angle())

print("Parklucke: ", l_park, "           ", "d3: ", d_point_3,  "           " "s: ", s_trailer_rear_park,  "           ", "Radius:               ", r_trajectory, "Bogenlange:               ", bgl_trajectory)



time_prev = mySubs.get_maneuver_time()
drive_distance = 0.0

print("Park1")
while drive_distance < bgl_trajectory:
	
	if loop_count == 10000:	
		current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), -hitch_angle_target_2, delta_hitch_angle_start_2)
		
		myPub.pub_velc_stc_dmc(vel_target, current_steer_angle, gear_back)

		drive_distance = drive_distance + delta_drive_distance(mySubs.get_maneuver_time(), time_prev, mySubs.get_velocity())
		time_prev = mySubs.get_maneuver_time()

		print("Park 1:", "Path_Park_1:" , drive_distance, "Diff:", str( bgl_trajectory - drive_distance), "          calc_trailer_vel:", calc_trailer_velocity(mySubs.get_hitch_angle(), mySubs.get_velocity(), l_trailer, r_trajectory))

		loop_count = 0
		log_count = log_count+1
	loop_count = loop_count + 1

# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_hitch_angle(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0



time_prev = mySubs.get_maneuver_time()
drive_distance = 0.0

print("Park2")
while drive_distance < bgl_trajectory:
	
	if loop_count == 10000:	
		current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target_2, delta_hitch_angle_start_2)
		
		myPub.pub_velc_stc_dmc(vel_target, current_steer_angle, gear_back)


		drive_distance = drive_distance + delta_drive_distance(mySubs.get_maneuver_time(), time_prev, mySubs.get_velocity())
		time_prev = mySubs.get_maneuver_time()

		print("Park 2:", "Path_Park_2:" , drive_distance, "Diff:", str(bgl_trajectory - drive_distance), "          calc_trailer_vel:", calc_trailer_velocity(mySubs.get_hitch_angle(), mySubs.get_velocity(), l_trailer, r_trajectory))

		loop_count = 0
		log_count = log_count+1
	loop_count = loop_count + 1

# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_hitch_angle(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0




hitch_angle_target_3 = 0.0
delta_hitch_angle_start_3 = math.radians(hitch_angle_target_3 - mySubs.get_hitch_angle())

time_prev = mySubs.get_maneuver_time()
drive_distance = 0.0

print("Park3")
while drive_distance < s_parked:
	
	if loop_count == 10000:	

		hitch_angle_target_3 = (hitch_angle_target_2 - hitch_angle_target_1) * 0.5

		if drive_distance > 0.5 * s_parked:
			hitch_angle_target_3 = hitch_angle_target_1

		current_steer_angle = calc_steer_angle_reichenegger(vel_target_mps, l_hitch, l_truck, l_trailer, mySubs.get_hitch_angle(), hitch_angle_target_3, delta_hitch_angle_start_3)

		myPub.pub_velc_stc_dmc(vel_target, current_steer_angle, gear_back)

		drive_distance = drive_distance + delta_drive_distance(mySubs.get_maneuver_time(), time_prev, mySubs.get_velocity())
		time_prev = mySubs.get_maneuver_time()
		

		print("Park 3:", str( s_parked - drive_distance))


		loop_count = 0
		log_count = log_count+1	
	loop_count = loop_count + 1

# Loggen der Parameter alle 5 Berechnungsdruchläufe
	if log_mode_on == True and log_count == 1:
		log_hitch_angle(logfile, mySubs.get_maneuver_time() - maneuver_time_start, mySubs.get_velocity(), current_steer_angle, mySubs.get_hitch_angle(), hitch_angle_target_1)
		log_count = 0
	
	
log_file_shut_down(logfile)
	
