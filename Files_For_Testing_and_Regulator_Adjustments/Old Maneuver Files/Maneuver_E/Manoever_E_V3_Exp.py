import time
import roslibpy
import math
import numpy

from log_maneuvers import log_file_init
from log_maneuvers import log_vehicle_param
from log_maneuvers import log_test_param
from log_maneuvers import log_file_shut_down
from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data_Trailer
from Trajectory_Regulator import Trajectory_Regulator
from Data_Plot import plot_PID_regulator




class Maneuver_States_Data:
# Konstrukor
	def __init__(self):
		self.state_park_prep = True
		self.state_roll_out = False
		self.state_roll_on = False
		self.state_park_1 = False
		self.state_park_switch = False
		self.state_park_2 = False
		self.state_park_3 = False
		self.state_park_finished = False





def cycle_wait(wait_time):
	current_time_start = time.time()		
	while time.time() <= current_time_start + wait_time:
		pass


# Numerische Lösung um von dem Radius der Trajektorie auf einen Knickwinkel zu kommen
def numeric_radius_to_hitch (l_trailer, l_hitch, radius):
	radius_try = 0.0
	angle = 0.1
	angle_step = 0.001
	r_tolerance = 0.1

	while True:
		if angle < 0 or angle > 0:
			radius_try = (l_trailer - (l_hitch / math.cos(angle))) * (1 / math.tan(angle))
		angle = angle + angle_step
		if radius_try < radius + r_tolerance and radius_try > radius - r_tolerance:
			#print(radius_try)			
			return angle



def calc_drive_distance(roll_angle_1, roll_angle_2, r_tire):
	
	if roll_angle_2 == roll_angle_1:
		drive_distance = 0.0
	
	if roll_angle_2 > roll_angle_1:
		drive_distance = (roll_angle_2 - roll_angle_1) * r_tire	

	if roll_angle_2 < roll_angle_1:
		drive_distance = (roll_angle_1 - roll_angle_2) * r_tire	
		
	return drive_distance



def delta_drive_distance(current_time, previous_time, velocity):
	drive_distance_new = (current_time - previous_time) * velocity
	return drive_distance_new

# Numerische Lösung um von dem Radius der Trajektorie auf einen Knickwinkel zu kommen
def calc_radius_to_steer (vd, td):
	steering_angle = math.atan(vd.l_truck / td.r_trajectory)
	return math.degrees(steering_angle)


def calc_velocity_rear(velocity_front, radius, l_truck):
	velocity_back = velocity_front * math.sqrt(radius ** 2 + l_truck ** 2) / radius
	return velocity_back





def maneuver_park_prep(subscriber, vd, td, msd):
	
	drive_mode = 1

	# Anfang des ersten Fahrzeugs der Parklücke
	if subscriber.get_side_truck_distance() > 0.0 and td.vehicle1_is_detected == False and td.current_roll_angle_1 == 0.0:
		td.vehicle1_is_detected = True
		#print("1")
		
	
	# Distanz Messen zu ersten Fahrzeug --> Überschreibt sich selbst, bis letzter Punkt vom ersten Fahrzeug registriert ist
	if td.vehicle1_is_detected == True:
		td.d_point_1 = subscriber.get_side_truck_distance()
		#print("2")

	

	# Ende des ersten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeug nötig für die Parklückenberechnung
	if subscriber.get_side_truck_distance() == 0.0 and td.vehicle1_is_detected == True and td.current_roll_angle_1 == 0.0:

		# Zwischenspeichern der Radumdrehung für Weg Messung
		td.current_roll_angle_1 = subscriber.get_trailer_roll_angle()
		
		td.vehicle1_is_detected = False
		#print("3")



	# Anfang des zweiten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeug nötig für die Parklückenberechnung
	if subscriber.get_side_truck_distance() > 0.0 and td.vehicle2_is_detected == False and td.current_roll_angle_1 > 0.0:
		td.vehicle2_is_detected = True		
		td.d_point_2 = subscriber.get_side_truck_distance()
		print("Truck_Hinten: ", str(td.d_point_2))


		# Weg der noch zurückgelegt wurde, obwohl Sensor schon Parklücke überquert hat
		td.s_1 = td.d_point_1 * math.tan(math.radians(vd.sensor_angle) * 0.5)

		# Weg der noch zurückgelegt werden muss bis Sensor exat orthogonal von Anfang zweites Fahrzeug
		td.s_2 = td.d_point_2 * math.tan(math.radians(vd.sensor_angle) * 0.5)

		# Zwischenspeichern der Radumdrehung für Weg Messung
		td.current_roll_angle_2 = subscriber.get_trailer_roll_angle()
		td.drive_distance_parking_lot = calc_drive_distance(td.current_roll_angle_1, td.current_roll_angle_2, vd.r_tire)



	#Sensor exat orthogonal zu Anfang zweites Fahrzeug
	if calc_drive_distance(td.current_roll_angle_2, subscriber.get_trailer_roll_angle(), vd.r_tire) > td.s_2 and td.d_point_2 > 0.0 and td.d_point_3 == 0.0:
		td.d_point_3 = subscriber.get_side_truck_distance()
		print("Truck_Vorne: ", str(td.d_point_3))

		# Zwischenspeichern der Radumdrehung für Weg Messung
		td.current_roll_angle_3 = subscriber.get_trailer_roll_angle()



	# Endpunkt des Vorwärtsfahren wird erreicht (Truck fährt nach vorne, bis Hinterachse exakt orthogonal zu Anfang zweiten Fahrzeug ist und dann nochmals weiter um den seitlichen Abstand zum Fahrzeug) --> Beenden des Maneuvers
	if calc_drive_distance(td.current_roll_angle_3, subscriber.get_trailer_roll_angle(), vd.r_tire) > (td.d_point_3 * 1.0 + vd.l_trailer - vd.l_hitch) and td.d_point_3 > 0.0:
		
		# Berechnung der Einpark Trajektorie
		td.calculate_trajectory_data(vd)

		td.current_roll_angle_4 = subscriber.get_trailer_roll_angle()		

		# Beenden des Maneuvers
		msd.state_park_prep = False
		msd.state_roll_out = True

		# Speichern des Hitch Winkels für die Hitch Winkel Regelung
		vd.save_hitch_angle(mySubs.get_hitch_angle())

		print("Ausrollen")

	return drive_mode


def maneuver_park_roll(subscriber, vd, td, msd, distance_to_trajectory):
	


	if msd.state_roll_out == True:
		drive_mode = 2		

	if subscriber.get_velocity() < 0.05 and msd.state_roll_on == False:
		drive_mode = 3
		td.current_roll_angle_5 = subscriber.get_trailer_roll_angle()
		td.roll_out_distance = calc_drive_distance(td.current_roll_angle_4, subscriber.get_trailer_roll_angle(), vd.r_tire)
		msd.state_roll_out = False
		msd.state_roll_on = True

		# Speichern des Hitch Winkels für die Hitch Winkel Regelung
		vd.save_hitch_angle(mySubs.get_hitch_angle())

		print("Ausrollen Ende ", str(td.roll_out_distance))
		

	if msd.state_roll_on == True:
		drive_mode = 3
		
	# Einpark-Trajektorie beginnt nach Rückfahren der Ausroll Distanz, dabei fährt der Truck noch ein Stück gerade
	if msd.state_roll_on == True and td.roll_out_distance * 15 + (td.l_straight / 2) < calc_drive_distance(td.current_roll_angle_5, subscriber.get_trailer_roll_angle(), vd.r_tire):
		drive_mode = 3
		
		td.current_roll_angle_6 = subscriber.get_trailer_roll_angle()
		msd.state_roll_on = False
		#msd.state_park_1 = True
		msd.state_park_finished = True

		# Speichern des Hitch Winkels für die Hitch Winkel Regelung
		vd.save_hitch_angle(mySubs.get_hitch_angle())

		# Berechnung der Kreismittelpunkte der Einparktrajektorie für Trajektorie-Regelung
		td.calc_r_trajectory_centers(vd.get_trailer_axis_posX(), vd.get_trailer_axis_posY(), distance_to_trajectory)

		# Berechnung der Switching Trajektorie
		td.calc_switching_trajectory()


		# Berechnung der Trajektorie der Parklücke
		td.calc_parking_lot_trajectory(distance_to_trajectory)
		
		print("Anrollen Ende ", str(calc_drive_distance(td.current_roll_angle_5, subscriber.get_trailer_roll_angle(), vd.r_tire)))
			
	return drive_mode



def maneuver_park_1(subscriber, vd, td, msd):
	drive_mode = 3

	if td.bgl_trajectory_park_1 < calc_drive_distance(td.current_roll_angle_6, subscriber.get_trailer_roll_angle(), vd.r_tire):
		td.current_roll_angle_7 = subscriber.get_trailer_roll_angle()
		msd.state_park_1 = False
		msd.state_park_switch = True

		# Speichern des Hitch Winkels für die Hitch Winkel Regelung
		vd.save_hitch_angle(mySubs.get_hitch_angle())
	
	return drive_mode

def maneuver_park_switch(subscriber, vd, td, msd):
	drive_mode = 3

	if (td.l_straight / 2) < calc_drive_distance(td.current_roll_angle_7, subscriber.get_trailer_roll_angle(), vd.r_tire):
		td.current_roll_angle_8 = subscriber.get_trailer_roll_angle()
		msd.state_park_switch = False
		msd.state_park_2 = True

		# Speichern des Hitch Winkels für die Hitch Winkel Regelung
		vd.save_hitch_angle(mySubs.get_hitch_angle())

	return drive_mode

def maneuver_park_2(subscriber, vd, td, msd):
	drive_mode = 3

	if td.bgl_trajectory_park_2 < calc_drive_distance(td.current_roll_angle_8, subscriber.get_trailer_roll_angle(), vd.r_tire):
		td.current_roll_angle_9 = subscriber.get_trailer_roll_angle()
		msd.state_park_2 = False
		msd.state_park_3 = True

		# Speichern des Hitch Winkels für die Hitch Winkel Regelung
		vd.save_hitch_angle(mySubs.get_hitch_angle())

	return drive_mode


def maneuver_park_3(subscriber, vd, td, msd):

	drive_mode = 3

	if ((td.l_straight / 2) + td.s_parked) < calc_drive_distance(td.current_roll_angle_9, subscriber.get_trailer_roll_angle(), vd.r_tire):
		msd.state_park_3 = False
		msd.state_park_finished = True
		drive_mode = 2
	
	return drive_mode






timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = False



# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6

# Definition der zu veröffentlichen Variablen	
current_steer_angle_target = 0 # Lenkwinkel Truck vorne
current_velocity_target = 0
current_drive_mode_target = 0.0


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz Trajektorie Berechnung
myTrajectory = Trajectory_Data_Trailer()

# Instanz des Reglers um auf Trajektorie zu Fahren
straight_PID = Trajectory_Regulator(100, 100, 100)
roll_on_PID = Trajectory_Regulator(300, 0, 300)
curved_PID_first = Trajectory_Regulator(30, 0, 0)
switching_PID = Trajectory_Regulator(30, 0, 0)
curved_PID_second = Trajectory_Regulator(30, 0, 0)
straight_PID_end = Trajectory_Regulator(30, 0, 0)

# Instanz Manöver
myStates = Maneuver_States_Data()

# Starten des Ros-Clients
myClient.start_client()


# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()


# Erstellen der Log Datei --> Neue Datei mit Datum und Zeitpunkt des Erstellens wird im Ordner log_files erstellt
if log_mode_on == True: 
	logfile = log_file_init(timestr, "E")
	log_vehicle_param(logfile, l_hitch, l_truck, l_trailer)
	log_test_param(logfile, hitch_angle_target_1 ,vel_target, 0.05, 18.0,)

# Warten bis Ros-Kommunikation vollständig da ist
while mySubs.get_maneuver_time() == 0.0:
	maneuver_time_start = mySubs.get_maneuver_time()


loop = 0


pub_velocity = 10.0
pub_drive_mode = 2
pub_steering = 0


# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myTrajectory.calc_trajectory_straight(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())
myVehicle.save_hitch_angle(mySubs.get_hitch_angle())



print("Hinter: ", mySubs.get_rear_axis_posX(), "  ", mySubs.get_rear_axis_posY())
print("Hitch: ", mySubs.get_hitch_posX(), "  ", mySubs.get_hitch_posY())


# Zykluszeit mit dem die Loop durchgeführt wird in Sekunden
time_step = 0.05

# Main Loop
print("start")
while(not myStates.state_park_finished):


	# Bestimmung Trailer Hinterachse Position, nötig für die Regelung der Trajektorie beim Rückwärtsfahren
	myVehicle.calc_trailer_axis_position(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), mySubs.get_hitch_angle())


	if (myStates.state_park_prep == True):

		pub_drive_mode = maneuver_park_prep(mySubs, myVehicle, myTrajectory, myStates)
		pub_steering = straight_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)
		#print("Distanz zu Gerade: ", distance_to_trajectory, " Lenkwinkel: " , pub_steering)

	if (myStates.state_roll_out == True or myStates.state_roll_on == True):
		
		pub_drive_mode = maneuver_park_roll(mySubs, myVehicle, myTrajectory, myStates, straight_PID.offset_prev)
		

		#print("Distanz zu Gerade: ", distance_to_trajectory, " Lenkwinkel: " , pub_steering)
		
		if (myStates.state_roll_out == True):
			pub_velocity = 0.0
			pub_steering = straight_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)

		if (myStates.state_roll_on == True):
			pub_velocity = 10.0

			pub_steering = roll_on_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)

			# Hitch Winkel muss invertiert werden, da negativer Offset zur Trajektorie durch einen positiven Knickwinkel korriegiert werden muss
			#hitch_angle = - roll_on_PID.regulate_hitch_straight(myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)

			#pub_steering = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_angle, hitch_angle - myVehicle.get_stored_hitch_angle())


	if (myStates.state_park_1 == True):

		pub_drive_mode = maneuver_park_1(mySubs, myVehicle, myTrajectory, myStates)
		pub_velocity = 10.0

		hitch_target = - math.degrees(numeric_radius_to_hitch(myVehicle.l_trailer, myVehicle.l_hitch, myTrajectory.r_trajectory))
		hitch_target_corrected = curved_PID_first.regulate_hitch_curved(myTrajectory.circle_center_first, myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), hitch_target, myTrajectory.r_trajectory, time_step)

		pub_steering = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target_corrected, hitch_target_corrected - myVehicle.get_stored_hitch_angle())


	if (myStates.state_park_switch == True):

		pub_drive_mode = maneuver_park_switch(mySubs, myVehicle, myTrajectory, myStates)
		pub_velocity = 10.0

		hitch_angle = switching_PID.regulate_hitch_straight(myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), myTrajectory.location_vector_switch_circle, myTrajectory.orientation_vector_switch_circle, time_step)

		pub_steering = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_angle, hitch_angle - myVehicle.get_stored_hitch_angle())
		

	if (myStates.state_park_2 == True):

		pub_drive_mode = maneuver_park_2(mySubs, myVehicle, myTrajectory, myStates)
		pub_velocity = 10.0

		hitch_target = math.degrees(numeric_radius_to_hitch(myVehicle.l_trailer, myVehicle.l_hitch, myTrajectory.r_trajectory))
		hitch_target_corrected = curved_PID_first.regulate_hitch_curved(myTrajectory.circle_center_first, myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), hitch_target, myTrajectory.r_trajectory, time_step)

		pub_steering = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target_corrected, hitch_target_corrected - myVehicle.get_stored_hitch_angle())
	

	if(myStates.state_park_3 == True):
		pub_drive_mode = maneuver_park_3(mySubs, myVehicle, myTrajectory, myStates)
		pub_velocity = 10.0
		
		hitch_angle = switch_PID.regulate_hitch_straight(myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), myTrajectory.location_vector_parking_lot, myTrajectory.orientation_vector_parking_lot, time_step)

		pub_steering = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_angle, hitch_angle - myVehicle.get_stored_hitch_angle())

	# Publishen der gewünschten Fahrbefehle
	myPub.pub_velc_stc_dmc(pub_velocity, pub_steering, pub_drive_mode)

	# Zykluszeit von 0.05 s --> 20 Hz
	cycle_wait(time_step)


	#print("Hitch Winkel am Truck: " ,mySubs.get_hitch_angle())



plot_PID_regulator(roll_on_PID.steer_angle_data, roll_on_PID.offset_data, time_step, roll_on_PID.k_p, roll_on_PID.k_i, roll_on_PID.k_d, "Steer")


if (log_mode_on == True):	
	log_file_shut_down(logfile)
	
