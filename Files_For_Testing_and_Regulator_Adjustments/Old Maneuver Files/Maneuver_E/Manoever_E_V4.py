import time
import roslibpy
import math
import numpy

from log_maneuvers import log_file_init
from log_maneuvers import log_vehicle_param
from log_maneuvers import log_test_param
from log_maneuvers import log_file_shut_down
from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data_Trailer
from Trajectory_Regulator import Trajectory_Regulator
from Data_Plot import plot_PID_regulator
from Data_Plot import plot_trajectory_trailer
from Data_Plot import plot_hitch_regulator
from Data_Plot import plot_trajectory_truck_trailer
from Maneuver_Parts_Trailer import Maneuver_Parts_Trailer


def cycle_wait(wait_time):
	current_time_start = time.time()		
	while time.time() <= current_time_start + wait_time:
		pass



timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = False



# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6
velocity_forward = 10.0
velocity_backward = 5.0

# Definition der zu veröffentlichen Variablen	
current_steer_angle_target = 0 # Lenkwinkel Truck vorne
current_velocity_target = 0
current_drive_mode_target = 0.0


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz Trajektorie Berechnung
myTrajectory = Trajectory_Data_Trailer()

# Instanz des Reglers um auf Trajektorie zu Fahren
straight_PID = Trajectory_Regulator(100, 100, 100)
roll_on_PID = Trajectory_Regulator(200, 200, 200)
curved_PID_first = Trajectory_Regulator(100, 0, 100)
switching_PID = Trajectory_Regulator(30, 0, 0)
curved_PID_second = Trajectory_Regulator(30, 0, 0)
straight_PID_end = Trajectory_Regulator(30, 0, 0)


hitch_data_1 = []
steer_data_1 = []
hitch_target_1 = 0.0

hitch_data_2 = []
steer_data_2 = []
hitch_target_2 = 0.0

hitch_data_3 = []
steer_data_3 = []
hitch_target_3 = 0.0

# Instanz Manöver
myMParts = Maneuver_Parts_Trailer()

# Starten des Ros-Clients
myClient.start_client()


# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()


# Erstellen der Log Datei --> Neue Datei mit Datum und Zeitpunkt des Erstellens wird im Ordner log_files erstellt
if log_mode_on == True: 
	logfile = log_file_init(timestr, "E")
	log_vehicle_param(logfile, l_hitch, l_truck, l_trailer)
	log_test_param(logfile, hitch_angle_target_1 ,vel_target, 0.05, 18.0,)

# Warten bis Ros-Kommunikation vollständig da ist
while mySubs.get_maneuver_time() == 0.0:
	maneuver_time_start = mySubs.get_maneuver_time()


pub_velocity = velocity_forward
pub_drive_mode = 2
pub_steering = 0

trailer_first_curved_regulating = False
trailer_second_curved_regulating = False

# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myTrajectory.calc_trajectory_straight(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())
myVehicle.save_hitch_angle(mySubs.get_hitch_angle())



print("Hinter: ", mySubs.get_rear_axis_posX(), "  ", mySubs.get_rear_axis_posY())
print("Hitch: ", mySubs.get_hitch_posX(), "  ", mySubs.get_hitch_posY())


# Zykluszeit mit dem die Loop durchgeführt wird in Sekunden
time_step = 0.05

# Main Loop
print("start")
while(not myMParts.state_park_finished):

	#print(mySubs.get_hitch_angle())

	# Bestimmung Trailer Hinterachse Position, nötig für die Regelung der Trajektorie beim Rückwärtsfahren
	myVehicle.calc_trailer_axis_position(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), mySubs.get_hitch_angle())


	# Teilmanöver: Vorbeifahren an Parklücke
	if (myMParts.state_park_prep == True):

		# Regelung zum Fahren auf gerader Trajektorie
		calculated_steering_angle = straight_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)

		# Errechneter Lenkwinkel begrenzen, falls maximal möglicher Lenkwinkel überschritten wird
		pub_steering = myVehicle.limit_steering_angle(calculated_steering_angle)
		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_drive_mode = myMParts.maneuver_park_prep(mySubs, myVehicle, myTrajectory)


	# Teilmanöver: Aus- und wieder Anrollen
	if (myMParts.state_roll_out == True or myMParts.state_roll_on == True):

		trailer_axis_offset_trajectory = roll_on_PID.calc_distance_to_parallel_route(myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight)

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_drive_mode = myMParts.maneuver_park_roll(mySubs, myVehicle, myTrajectory, trailer_axis_offset_trajectory)
		
		# Anhalten
		if (myMParts.state_roll_out == True):
			pub_velocity = 0.0
		
			# Regelung zum Fahren auf gerader Trajektorie, nutzt Regler aus vorherigem Teilmanöver, da noch Vorwärts, gerade gefahren wird
			calculated_steering_angle = straight_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)

			# Errechneter Lenkwinkel begrenzen, falls maximal möglicher Lenkwinkel überschritten wird
			pub_steering = myVehicle.limit_steering_angle(calculated_steering_angle)


		# Wieder Anfahren, nun Rückwärts
		if (myMParts.state_roll_on == True):
			pub_velocity = velocity_backward

			# Regelung zum Fahren auf gerader Trajektorie, neuer Regler fürs Rückwärtsanfahren
			calculated_steering_angle = roll_on_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)
		
			# Errechneter Lenkwinkel begrenzen, falls maximal möglicher Lenkwinkel überschritten wird
			pub_steering = myVehicle.limit_steering_angle(calculated_steering_angle)
		


	# Teilmanöver: erster Kreis zum Parken
	if (myMParts.state_circle_first == True):


		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_drive_mode = myMParts.maneuver_circle_first(mySubs, myVehicle, myTrajectory)
		pub_velocity = velocity_backward
		
		# Berechnung des Knickwinkels, welcher für die Kreisfahrt benötigt wird
		hitch_target = - math.degrees(myVehicle.numeric_radius_to_hitch( myTrajectory.r_trajectory))

	
		if (trailer_first_curved_regulating == False):
			
			steer_target = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle())
			pub_steering = myVehicle.limit_steering_angle(steer_target)

			hitch_data_1.append(mySubs.get_hitch_angle())
			steer_data_1.append(pub_steering)
			hitch_target_1 = hitch_target
			#print("Knickwinkel aktuell:", mySubs.get_hitch_angle(), " Steer Tar:", steer_target, " Hitch Tar: ", hitch_target)

		#if (mySubs.get_hitch_angle() < hitch_target + 1 and steer_target < 0):
			#trailer_first_curved_regulating = False

		if (trailer_first_curved_regulating == True):
			calc_steering = curved_PID_first.regulate_steering_curved_trailer(myTrajectory.circle_center_first, myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), steer_target, myTrajectory.r_trajectory, time_step)

			pub_steering = myVehicle.limit_steering_angle(calc_steering)

			print ("Knickwinkel Ziel: ", hitch_target, "    Knickwinkel aktuell:", mySubs.get_hitch_angle(), " Offset : ", curved_PID_first.offset_prev, "  Steer Target: ", steer_target, "  Lenkwinkel: ", calc_steering)




	# Teilmanöver: gerade Fahrt zwischen den beiden entgegengesetzten Kreisen, zum Wechseln des Knickwinkels
	if (myMParts.state_switch_circle == True):

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_drive_mode = myMParts.maneuver_switch_circle(mySubs, myVehicle, myTrajectory)
		pub_velocity = velocity_backward

		#hitch_angle = straight_PID.regulate_hitch_straight(myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), myTrajectory.location_vector_switch_circle, myTrajectory.orientation_vector_switch_circle, time_step)
		hitch_angle = 0.0

		# Regelung auf den gewünschten Knickwinkel
		steer_target = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_angle, hitch_angle - myVehicle.get_stored_hitch_angle())

		pub_steering = myVehicle.limit_steering_angle(steer_target)

		hitch_data_2.append(mySubs.get_hitch_angle())
		steer_data_2.append(pub_steering)
		hitch_target_2 = hitch_angle
		
		print("switch")
		

	# Teilmanöver: zweiter Kreis zum Parken
	if (myMParts.state_circle_second == True):

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_drive_mode = myMParts.maneuver_circle_second(mySubs, myVehicle, myTrajectory)
		pub_velocity = velocity_backward

		# Berechnung des Knickwinkels, welcher für die Kreisfahrt benötigt wird
		hitch_target = math.degrees(myVehicle.numeric_radius_to_hitch( myTrajectory.r_trajectory))

	
		if (trailer_second_curved_regulating == False):
			
			steer_target = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle())
			pub_steering = myVehicle.limit_steering_angle(steer_target)

			hitch_data_3.append(mySubs.get_hitch_angle())
			steer_data_3.append(pub_steering)
			hitch_target_3 = hitch_target

		#if (mySubs.get_hitch_angle() > hitch_target -1 and steer_target > 0):
			#trailer_second_curved_regulating = False

		if (trailer_second_curved_regulating == True):
			
			calc_steering = curved_PID_second.regulate_steering_curved_trailer(myTrajectory.circle_center_second, myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), steer_target, myTrajectory.r_trajectory, time_step)

			pub_steering = myVehicle.limit_steering_angle(calc_steering)

			print ("Knickwinkel Ziel: ", hitch_target, "    Knickwinkel aktuell:", mySubs.get_hitch_angle(), " Offset : ", curved_PID_second.offset_prev, "  Steer Target: ", steer_target, "  Lenkwinkel: ", calc_steering)



	# Teilmanöver: Kurze gerade Fahrt, um Knickwinkel am Gespann in neutrale Position zu bringen
	if(myMParts.state_park_straighten == True):
		pub_drive_mode = myMParts.maneuver_park_straighten(mySubs, myVehicle, myTrajectory)
		pub_velocity = velocity_backward
		
		#hitch_angle = straight_PID_end.regulate_hitch_straight(myVehicle.get_trailer_axis_posX(), myVehicle.get_trailer_axis_posY(), myTrajectory.location_vector_parking_lot, myTrajectory.orientation_vector_parking_lot, time_step)

		hitch_angle = 0.0

		# Regelung auf den gewünschten Knickwinkel
		pub_steering = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_angle, hitch_angle - myVehicle.get_stored_hitch_angle())



	# Publishen der gewünschten Fahrbefehle
	myPub.pub_velc_stc_dmc(pub_velocity, pub_steering, pub_drive_mode)

	# Speichern der Positionsdaten für spätere Auswertung und Analysis
	myVehicle.save_position_data(myVehicle.trailer_axis_pos[0], myVehicle.trailer_axis_pos[1])
	myVehicle.save_position_data_2(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())

	# Zykluszeit von 0.05 s --> 20 Hz
	cycle_wait(time_step)



#plot_PID_regulator(straight_PID.steer_angle_data, straight_PID.offset_data, time_step, straight_PID.k_p, straight_PID.k_i, straight_PID.k_d, "Steer", "Park_prep")
#plot_PID_regulator(roll_on_PID.steer_angle_data, roll_on_PID.offset_data, time_step, roll_on_PID.k_p, roll_on_PID.k_i, roll_on_PID.k_d, "Steer", "Roll_on")
#plot_PID_regulator(curved_PID_first.steer_angle_data, curved_PID_first.offset_data, time_step, curved_PID_first.k_p, curved_PID_first.k_i, curved_PID_first.k_d, "hitch", "First_circle")
#plot_PID_regulator(switching_PID.steer_angle_data, switching_PID.offset_data, time_step, switching_PID.k_p, switching_PID.k_i, switching_PID.k_d, "Steer", "Switch_circle")
#plot_PID_regulator(curved_PID_second.steer_angle_data, curved_PID_second.offset_data, time_step, curved_PID_second.k_p, curved_PID_second.k_i, curved_PID_second.k_d, "hitch", "Second_circle")
#plot_PID_regulator(straight_PID_end.steer_angle_data, straight_PID_end.offset_data, time_step, straight_PID_end.k_p, straight_PID_end.k_i, straight_PID_end.k_d, "Steer", "Straighten")


plot_hitch_regulator(steer_data_1, hitch_data_1, hitch_target_1, time_step, -0.008, 18.0)
plot_hitch_regulator(steer_data_2, hitch_data_2, hitch_target_2, time_step, -0.008, 18.0)
plot_hitch_regulator(steer_data_3, hitch_data_3, hitch_target_3, time_step, -0.008, 18.0)

# Berechnen der Plot-Punkte der Trajektorie für die spätere grafische Darstellung
myTrajectory.calc_straight_trajectory_data()
myTrajectory.calc_parking_lot_trajectory_data()
myTrajectory.calc_circle_first_data()
myTrajectory.calc_switch_circle_data()
myTrajectory.calc_circle_second_data()

# Plotten der Trajektorien zur Auswertung
plot_trajectory_trailer(myVehicle.get_posX_data(), myVehicle.get_posY_data(), myTrajectory.trajectory_straight_data_posX, myTrajectory.trajectory_straight_data_posY, myTrajectory.trajectory_parking_lot_data_posX, myTrajectory.trajectory_parking_lot_data_posY, myTrajectory.circle_first_data_posX, myTrajectory.circle_first_data_posY, myTrajectory.switch_circle_data_posX, myTrajectory.switch_circle_data_posY, myTrajectory.circle_second_data_posX, myTrajectory.circle_second_data_posY)

# Plotten der Trajektorien zur Auswertung
plot_trajectory_truck_trailer(myVehicle.posX_data_2, myVehicle.posY_data_2, myVehicle.get_posX_data(), myVehicle.get_posY_data(), myTrajectory.trajectory_straight_data_posX, myTrajectory.trajectory_straight_data_posY, myTrajectory.trajectory_parking_lot_data_posX, myTrajectory.trajectory_parking_lot_data_posY, myTrajectory.circle_first_data_posX, myTrajectory.circle_first_data_posY, myTrajectory.switch_circle_data_posX, myTrajectory.switch_circle_data_posY, myTrajectory.circle_second_data_posX, myTrajectory.circle_second_data_posY)

if (log_mode_on == True):	
	log_file_shut_down(logfile)
	
