import time
import roslibpy
import math
import numpy

from log_maneuvers import log_file_init
from log_maneuvers import log_vehicle_param
from log_maneuvers import log_test_param
from log_maneuvers import log_file_shut_down
from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data_Truck
from Trajectory_Regulator import Trajectory_Regulator
from Data_Plot import plot_PID_regulator
from Data_Plot import plot_trajectory




class Maneuver_States_Truck:
# Konstrukor
	def __init__(self):
		self.state_park_prep = True
		self.state_roll_out = False
		self.state_roll_on = False
		self.state_park_1 = False
		self.state_park_2 = False
		self.state_park_3 = False
		self.state_finished = False




def cycle_wait(wait_time):
	current_time_start = time.time()		
	while time.time() <= current_time_start + wait_time:
		pass


def calc_drive_distance(roll_angle_1, roll_angle_2, r_tire):
	
	if roll_angle_2 == roll_angle_1:
		drive_distance = 0.0
	
	if roll_angle_2 > roll_angle_1:
		drive_distance = (roll_angle_2 - roll_angle_1) * r_tire	

	if roll_angle_2 < roll_angle_1:
		drive_distance = (roll_angle_1 - roll_angle_2) * r_tire	
		
	return drive_distance



# Numerische Lösung um von dem Radius der Trajektorie auf einen Lenkwinkel zu kommen
def calc_radius_to_steer (vd, td):
	steering_angle = math.atan(vd.l_truck / td.r_trajectory)
	return math.degrees(steering_angle)


def calc_velocity_rear(velocity_front, radius, l_truck):
	velocity_back = velocity_front * math.sqrt(radius ** 2 + l_truck ** 2) / radius
	return velocity_back





def maneuver_park_prep(subscriber, vd, td, msd):
	
	drive_mode = 1

	# Anfang des ersten Fahrzeugs der Parklücke
	if subscriber.get_side_truck_distance() > 0.0 and td.vehicle1_is_detected == False and td.current_roll_angle_1 == 0.0:
		td.vehicle1_is_detected = True
		#print("1")
		
	
	# Distanz Messen zu ersten Fahrzeug --> Überschreibt sich selbst, bis letzter Punkt vom ersten Fahrzeug registriert ist
	if td.vehicle1_is_detected == True:
		td.d_point_1 = subscriber.get_side_truck_distance()
		#print("2")

	

	# Ende des ersten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeug nötig für die Parklückenberechnung
	if subscriber.get_side_truck_distance() == 0.0 and td.vehicle1_is_detected == True and td.current_roll_angle_1 == 0.0:

		# Zwischenspeichern der Radumdrehung für Weg Messung
		td.current_roll_angle_1 = subscriber.get_roll_angle()
		

		td.vehicle1_is_detected = False
		#print("3")



	# Anfang des zweiten Fahrzeuges der Parklücke --> Zeitpunkt & derzeitiger Abstand zum Fahrzeug nötig für die Parklückenberechnung
	if subscriber.get_side_truck_distance() > 0.0 and td.vehicle2_is_detected == False and td.current_roll_angle_1 > 0.0:
		td.vehicle2_is_detected = True		
		td.d_point_2 = subscriber.get_side_truck_distance()
		print("Truck_Hinten: ", str(td.d_point_2))


		# Weg der noch zurückgelegt wurde, obwohl Sensor schon Parklücke überquert hat
		td.s_1 = td.d_point_1 * math.tan(math.radians(vd.sensor_angle) * 0.5)

		# Weg der noch zurückgelegt werden muss bis Sensor exat orthogonal von Anfang zweites Fahrzeug
		td.s_2 = td.d_point_2 * math.tan(math.radians(vd.sensor_angle) * 0.5)

		# Zwischenspeichern der Radumdrehung für Weg Messung
		td.current_roll_angle_2 = subscriber.get_roll_angle()
		td.drive_distance_parking_lot = calc_drive_distance(td.current_roll_angle_1, td.current_roll_angle_2, vd.r_tire)



	#Sensor exat orthogonal zu Anfang zweites Fahrzeug
	if calc_drive_distance(td.current_roll_angle_2, subscriber.get_roll_angle(), vd.r_tire) > td.s_2 and td.d_point_2 > 0.0 and td.d_point_3 == 0.0:
		td.d_point_3 = subscriber.get_side_truck_distance()
		print("Truck_Vorne: ", str(td.d_point_3))

		# Zwischenspeichern der Radumdrehung für Weg Messung
		td.current_roll_angle_3 = subscriber.get_roll_angle()



	# Endpunkt des Vorwärtsfahren wird erreicht --> Beenden des Maneuvers
	if calc_drive_distance(td.current_roll_angle_3, subscriber.get_roll_angle(), vd.r_tire) > td.d_point_3 * 1.5 and td.d_point_3 > 0.0:
		
		# Berechnung der Trajektorie
		td.calculate_trajectory_data(vd)

		td.current_roll_angle_4 = subscriber.get_roll_angle()		

		# Beenden des Maneuvers
		msd.state_park_prep = False
		msd.state_roll_out = True
		print("Ausrollen")

	return drive_mode


def maneuver_park_roll(subscriber, vd, td, msd, distance_to_trajectory):
	


	if msd.state_roll_out == True:
		drive_mode = 2		

	if subscriber.get_velocity() < 0.05 and msd.state_roll_on == False:
		drive_mode = 3
		td.current_roll_angle_5 = subscriber.get_roll_angle()
		td.roll_out_distance = calc_drive_distance(td.current_roll_angle_4, subscriber.get_roll_angle(), vd.r_tire)
		msd.state_roll_out = False
		msd.state_roll_on = True

		print("Ausrollen Ende ", str(td.roll_out_distance))
		

	if msd.state_roll_on == True:
		drive_mode = 3
		
	if msd.state_roll_on == True and td.roll_out_distance < calc_drive_distance(td.current_roll_angle_5, subscriber.get_roll_angle(), vd.r_tire):
		drive_mode = 3
		td.current_roll_angle_6 = subscriber.get_roll_angle()
		msd.state_roll_on = False
		msd.state_park_1 = True

		# Berechnung der Kreismittelpunkte der Einparktrajektorie für Trajektorie-Regelung
		td.calc_r_trajectory_centers(subscriber.get_rear_axis_posX(), subscriber.get_rear_axis_posY(), distance_to_trajectory)
		td.calc_parking_lot_trajectory(distance_to_trajectory)
		
		print("Anrollen Ende ", str(calc_drive_distance(td.current_roll_angle_5, subscriber.get_roll_angle(), vd.r_tire)))
			
	return drive_mode



def maneuver_park_1(subscriber, vd, td, msd):
	drive_mode = 3

	if td.bgl_trajectory_park_1 < calc_drive_distance(td.current_roll_angle_6, subscriber.get_roll_angle(), vd.r_tire):
		td.current_roll_angle_7 = subscriber.get_roll_angle()
		msd.state_park_1 = False
		msd.state_park_2 = True
	
	return drive_mode


def maneuver_park_2(subscriber, vd, td, msd):
	drive_mode = 3

	if td.bgl_trajectory_park_2 < calc_drive_distance(td.current_roll_angle_7, subscriber.get_roll_angle(), vd.r_tire):
		td.current_roll_angle_8 = subscriber.get_roll_angle()
		msd.state_park_2 = False
		msd.state_park_3 = True
	return drive_mode






timestr = time.strftime("%d_%_m_%Y_%H_%M_%S")

# If True --> A text file will be created to monitor the simulation results
log_mode_on = False



# Definition Regelparameter
# Geschwindigkeit mit dem das Manöver durchgeführt werden soll --> Mit dieser wird später gerechnet, aber Fahrzeug erreicht nicht unbedingt diese Geschwindigkeit
vel_target = 10.0
vel_target_mps = vel_target / 3.6

# Definition der zu veröffentlichen Variablen	
current_steer_angle_target = 0 # Lenkwinkel Truck vorne
current_velocity_target = 0
current_drive_mode_target = 0.0


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz Trajektorie Berechnung
myTrajectory = Trajectory_Data_Truck()

# Instanz des Reglers um auf Trajektorie zu Fahren
straight_PID = Trajectory_Regulator(100, 100, 100)
curved_PID_first = Trajectory_Regulator(200, 200, 200)
curved_PID_second = Trajectory_Regulator(200, 200, 200)

# Instanz Manöver
myStates = Maneuver_States_Truck()

# Starten des Ros-Clients
myClient.start_client()


# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()



# Erstellen der Log Datei --> Neue Datei mit Datum und Zeitpunkt des Erstellens wird im Ordner log_files erstellt
if log_mode_on == True: 
	logfile = log_file_init(timestr, "E")
	log_vehicle_param(logfile, l_hitch, l_truck, l_trailer)
	log_test_param(logfile, hitch_angle_target_1 ,vel_target, 0.05, 18.0,)

# Warten bis Ros-Kommunikation vollständig da ist
while mySubs.get_maneuver_time() == 0.0:
	maneuver_time_start = mySubs.get_maneuver_time()



pub_velocity = 10.0
pub_drive_mode = 2
pub_steering = 0.0

time_step = 0.05
time_prev = 0


# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myTrajectory.calc_trajectory_straight(mySubs.get_hitch_posX(), mySubs.get_hitch_posY(), mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())

print("Hinter: ", mySubs.get_rear_axis_posX(), "  ", mySubs.get_rear_axis_posY())
print("Hitch: ", mySubs.get_hitch_posX(), "  ", mySubs.get_hitch_posY())

# Main Loop
print("start")

while(myStates.state_finished == False):


	if (myStates.state_park_prep == True):

		calculated_steering_angle = straight_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)

		pub_steering = myVehicle.limit_steering_angle(calculated_steering_angle)
		#pub_steering = 0.0

		pub_drive_mode = maneuver_park_prep(mySubs, myVehicle, myTrajectory, myStates)


	if (myStates.state_roll_out == True or myStates.state_roll_on == True):
		
		calculated_steering_angle = straight_PID.regulate_steering_straight(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)

		pub_steering = myVehicle.limit_steering_angle(calculated_steering_angle)
		#pub_steering = 0.0

		pub_drive_mode = maneuver_park_roll(mySubs, myVehicle, myTrajectory, myStates, straight_PID.offset_prev)

		if (myStates.state_roll_out == True):
			pub_velocity = 0.0

		if (myStates.state_roll_on == True):
			pub_velocity = 10.0


	if (myStates.state_park_1 == True):

		#distance_to_center = calc_distance_to_r_trajectory_center (myTrajectory.circle_center_first, mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())
	
		pub_drive_mode = maneuver_park_1(mySubs, myVehicle, myTrajectory, myStates)
		pub_velocity = 10.0
		steer_target = - calc_radius_to_steer(myVehicle, myTrajectory)
		calculated_steering_angle = curved_PID_first.regulate_steering_curved (myTrajectory.circle_center_first, mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), steer_target, myTrajectory.r_trajectory, time_step)
		pub_steering = myVehicle.limit_steering_angle(calculated_steering_angle)
		#pub_steering = steer_target
		
		#print("  Zentrum 1 XY: " , myTrajectory.circle_center_first, "  Radius Soll: " , myTrajectory.r_trajectory , "  Distanz zu Zentrum: ", distance_to_center)
		#print("Lenkwinkel Ziel: ", steer_target, "  Errechneter Lenkwinkel: " , calculated_steering_angle, "  Radius Soll: " , myTrajectory.r_trajectory , "  Radius Ist: ", distance_to_center)

	if (myStates.state_park_2 == True):

		#distance_to_center = calc_distance_to_r_trajectory_center (myTrajectory.circle_center_second, mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())

		pub_drive_mode = maneuver_park_2(mySubs, myVehicle, myTrajectory, myStates)
		pub_velocity = 10.0
		steer_target = calc_radius_to_steer(myVehicle, myTrajectory)
		calculated_steering_angle = curved_PID_second.regulate_steering_curved (myTrajectory.circle_center_second, mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY(), steer_target, myTrajectory.r_trajectory, time_step)
		pub_steering = myVehicle.limit_steering_angle(calculated_steering_angle)
		#pub_steering = steer_target


		#print("  Zentrum 2 XY: " , myTrajectory.circle_center_second, "  Radius Soll: " , myTrajectory.r_trajectory , "  Distanz zu Zentrum: ", distance_to_center)
		#print("Lenkwinkel Ziel: ", steer_target, "  Errechneter Lenkwinkel: " , calculated_steering_angle, "  Radius Soll: " , myTrajectory.r_trajectory , "  Radius Ist: ", distance_to_center)

	if(myStates.state_park_3 == True):
		pub_drive_mode = 2
		pub_velocity = 0.0
		pub_steering = 0.0
		
		if(mySubs.get_velocity() < 0.001):
			myStates.state_park_3 = False
			myStates.state_finished = True

		#print(myTrajectory.circle_center_first, "       "  ,myTrajectory.circle_center_second, "       "  , mySubs.get_rear_axis_posX(), "       ", mySubs.get_rear_axis_posY())


	# Publishen der gewünschten Fahrbefehle
	myPub.pub_velc_stc_dmc(pub_velocity, pub_steering, pub_drive_mode)

	# Speichern der Positionsdaten für spätere Auswertung und Analysis
	myVehicle.save_position_data(mySubs.get_rear_axis_posX(), mySubs.get_rear_axis_posY())
	print(str(time.time()-time_prev))
	time_prev = time.time()

	# Zykluszeit von 0.05 s --> 20 Hz
	cycle_wait(time_step)


plot_PID_regulator(straight_PID.steer_angle_data, straight_PID.offset_data, time_step, straight_PID.k_p, straight_PID.k_i, straight_PID.k_d, "Steer")
plot_PID_regulator(curved_PID_first.steer_angle_data, curved_PID_first.offset_data, time_step, curved_PID_first.k_p, curved_PID_first.k_i, curved_PID_first.k_d, "Steer")
plot_PID_regulator(curved_PID_second.steer_angle_data, curved_PID_second.offset_data, time_step, curved_PID_second.k_p, curved_PID_second.k_i, curved_PID_second.k_d, "Steer")

myTrajectory.calc_straight_trajectory_data()
myTrajectory.calc_parking_lot_trajectory_data()
myTrajectory.calc_circle_first_data()
myTrajectory.calc_circle_second_data()

plot_trajectory(myVehicle.get_posX_data(), myVehicle.get_posY_data(), myTrajectory.trajectory_straight_data_posX, myTrajectory.trajectory_straight_data_posY, myTrajectory.trajectory_parking_lot_data_posX, myTrajectory.trajectory_parking_lot_data_posY, myTrajectory.circle_first_data_posX, myTrajectory.circle_first_data_posY, myTrajectory.circle_second_data_posX, myTrajectory.circle_second_data_posY)




if (log_mode_on == True):	
	log_file_shut_down(logfile)
	
