import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("2022-11-29-12-43-17.csv")
print(df)

range_cols = [col for col in df.columns if 'ranges.' in col]



for col in range_cols:
    df_range = df[col]
    x = list(df_range.index.values)
    y = df_range.values.tolist()
    plt.plot(x, y)
    plt.title(col.title())
    name = col.replace(".", "_")
    plt.savefig(f"Ranges_Figures/Bag_2022-11-29-12-33-14/{name}.png")
    plt.close()
