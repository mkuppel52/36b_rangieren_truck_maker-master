from Vehicle_Parameter import Vehicle_Data
import math
import time


# Definition der Klasse für Koordinatensystem
class Coordinate_System:
    
    # 2 Dimensionales Koordinatensystem
    # Definition x-Richtung: Gerade aus bei Zeitpunkt 0

    def __init__(self, myVehicle: Vehicle_Data):

        # Instanz der Klasse Verhicle_Data für notwendige Fahrzeugparameter
        self.myVehicle = myVehicle
        
        # Positionsdaten
        self.x = 0.0 # X-Position Truck Vorderachse
        self.y = 0.0 # Y-Position Truck Vorderachse
        self.x_rear = 0.0 # X-Position Truck Hinterachse
        self.y_rear = 0.0 # Y-Position Truck Hinterachse
        self.x_trailer = 0.0 # X-Position Trailerachse
        self.y_trailer = 0.0 # Y-Position Trailerachse
        self.x_hitch = 0.0
        self.y_hitch = 0.0
        self.truck_orientation = 0 # Orientirerungsrichtung des Trucks in Bogenmaß (Winkel zur x-Achse)

        self.driven_distance = 0

        self.last_timestamp = time.time()

        # Letzte Position des Trailers zur Berechnung der im Intervall gefahrenen Distanz Trailer
        self.last_position_trailer = {"x":self.x_trailer, "y":self.y_trailer}

     # Berechnung der Truck Position (Vorderachse) durch Integration des Fahrweges
    def calc_truck_position(self, velocity, steering_angle):
         deltaT = time.time() - self.last_timestamp
         self.driven_distance = velocity * deltaT
         deltaX = self.driven_distance * math.cos(self.truck_orientation + math.radians(steering_angle))
         deltaY = self.driven_distance * math.sin(self.truck_orientation + math.radians(steering_angle))
         self.x += deltaX
         self.y += deltaY
         self.__calc_truck_orientation(steering_angle)
         self.last_timestamp = time.time()

     # Berechnung der Hinterachsenposition
    def __calc_position_rear_axis(self):
         # Vorderachse --> Hinterachse: Beachtung Orientierung
         deltaX = self.myVehicle.l_truck * math.cos(self.truck_orientation)
         self.x_rear = self.x - deltaX
         deltaY = self.myVehicle.l_truck * math.sin(self.truck_orientation)
         self.y_rear = self.y - deltaY

    
     # Berechnung der Trailerachsenposition
    def __calc_position_trailer(self, hitch_angle):
         # Speichern der letzten Position
         self.last_position_trailer = {"x":self.x_trailer, "y":self.y_trailer}
         # Vorderachse --> Hitch --> Trailer: Beachtung Orientierung und Hitchwinkel
         deltaX_truck = (self.myVehicle.l_truck + self.myVehicle.l_hitch) * math.cos(self.truck_orientation)
         deltaX_trailer = self.myVehicle.l_trailer * math.cos(self.truck_orientation + math.radians(hitch_angle))
         self.x_trailer = self.x - (deltaX_truck + deltaX_trailer)
         deltaY_truck = (self.myVehicle.l_truck + self.myVehicle.l_hitch) * math.sin(self.truck_orientation)
         deltaY_trailer = self.myVehicle.l_trailer * math.sin(self.truck_orientation + math.radians(hitch_angle))
         self.y_trailer = self.y - (deltaY_truck + deltaY_trailer)


    # Berechnung der Hinterachsenposition
    def __calc_position_hitch(self):
         # Vorderachse --> Hinterachse + Weg zur Kupplung: Beachtung Orientierung
         deltaX = (self.myVehicle.l_truck + self.myVehicle.l_hitch) * math.cos(self.truck_orientation)
         self.x_hitch = self.x - deltaX
         deltaY = (self.myVehicle.l_truck + self.myVehicle.l_hitch) * math.sin(self.truck_orientation)
         self.y_hitch = self.y - deltaY

     # Berechnung der Truck-Orientierung
    def __calc_truck_orientation(self, steering_angle):
            # Berechnen Kurven-Radius der Vorderachse
            if steering_angle != 0:
                R_v = self.myVehicle.l_truck / math.sin(math.radians(steering_angle))
                # alpha_kreisbogen = 2 * math.pi * self.driven_distance / (2 * math.pi * R_v)
                alpha_kreisbogen = 2 * math.pi * self.driven_distance / (2 * math.pi * R_v)
                # print("TO: ", self.truck_orientation, "\talpha: ", alpha_kreisbogen)
                self.truck_orientation += alpha_kreisbogen
     

     # Berechnung aller Positionen mit aufgerufen mit einer Methode 
    def calc_positions(self, velocity, steering_angle, hitch_angle):
         self.calc_truck_position(velocity, steering_angle)
         self.__calc_position_rear_axis()
         self.__calc_position_hitch()
         self.__calc_position_trailer(hitch_angle)

     # Berechnung im Zeitintervall gefahrene Distanz Trailer
    def calc_driven_distance_trailer(self, velocity):
        distance = 0
        if abs(velocity) > 1: 
            distance = math.sqrt((self.x_trailer-self.last_position_trailer["x"])**2 + (self.y_trailer-self.last_position_trailer["y"])**2)
        return distance
         
         




