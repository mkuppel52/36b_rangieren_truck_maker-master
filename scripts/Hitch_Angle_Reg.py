import math


# Regler nach Reichegger --> Funktion zur Lenkwinkelberechnung für die Regelung des Knickwinkels auf konstanten Wert
def calc_steer_angle_reichenegger(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle, hitch_angle_target, delta_hitch_angle_start):

	# Polynom zur Berechnung der Regelparameter in Abhängigkeit des Zielknickwinkels
	if(abs(hitch_angle_target) >= 0.1):
		lambda_1 = 1.90*(10**(-5))*(abs(hitch_angle_target)**2) - 3.76*(10**(-4))*abs(hitch_angle_target) - 0.4285	#Polynom zweiten Grades
	# Ausnahme für kleine Knickwinkel: Ist nicht im Polynom enthalten	
	else:
		lambda_1 = -0.0859

	#lambda_2 = 2.62*(10**(-4))*(abs(hitch_angle_target)**2) - 0.0362*abs(hitch_angle_target) + 2.5024	#Polynom zweiten Grades
	lambda_2 = -6.76*(10**(-7))*(abs(hitch_angle_target)**4) + 8.53*(10**(-5))*(abs(hitch_angle_target)**3) - 3.23*(10**(-3))*(abs(hitch_angle_target)**2) + 0.0157*abs(hitch_angle_target) + 2.3				#Polynom vierten Grades

	#print("lambda 1: ", lambda_1 , "lambda 2:  " , lambda_2, "delta_angle:  ", delta_hitch_angle_start)
	
	delta_hitch_angle_rad = math.radians(hitch_angle_target - current_hitch_angle)
	#delta_hitch_angle_start_rad = math.radians(delta_hitch_angle_start)

	g = g_phi(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle)
	f = f_phi(current_velocity, l_trailer, current_hitch_angle, hitch_angle_target)

	# Verhindert das bei u durch 0 geteilt wird	
	if g == 0:
		g = 0.00001

	# Formel zur Berechnung des Lenkwinkels	
	#u = (1 / g) * (-f - lambda_1 * delta_hitch_angle_start_rad  - lambda_2 * delta_hitch_angle_rad)
	u = (1 / g) * (-f - lambda_1 * math.radians(hitch_angle_target)  - lambda_2 * delta_hitch_angle_rad)

	steering_angle = math.degrees(math.atan(u))

	# Begrenzung des maximalen Lenkwinkels zur naturgetreuen Darstellung --> 25° wird als Wert für das Projekt angenommen
	if steering_angle > 25:
		steering_angle = 25
	if steering_angle < -25:
		steering_angle = -25

	return steering_angle

# Regler nach Reichegger --> Alte Version ohne Regelparameter als Polynom
def calc_steer_angle_reichenegger_old(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle, hitch_angle_target, delta_hitch_angle_start):
	
	lambda_1 = -0.458
	lambda_2 = 18.0

	delta_hitch_angle_rad = math.radians(hitch_angle_target - current_hitch_angle)

	g = g_phi(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle)
	f = f_phi(current_velocity, l_trailer, current_hitch_angle)

	# Verhindert das bei u durch 0 geteilt wird	
	if g == 0:
		g = 0.00001

	# Formel zur Berechnung des Lenkwinkels	
	#u = (1 / g) * (-f - lambda_1 * delta_hitch_angle_start_rad  - lambda_2 * delta_hitch_angle_rad)
	u = (1 / g) * (-f - lambda_1 * math.radians(hitch_angle_target)  - lambda_2 * delta_hitch_angle_rad)

	steering_angle = math.degrees(math.atan(u))

	# Begrenzung des maximalen Lenkwinkels zur naturgetreuen Darstellung --> 25° ist nur geschätzer Wert
	if steering_angle > 25:
		steering_angle = 25
	if steering_angle < -25:
		steering_angle = -25

	return steering_angle

# Mathematische Teilfunktion für die Lenkwinkelberechnung
def g_phi(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle):
	
	current_hitch_angle_rad = math.radians(current_hitch_angle)
	
	g = (current_velocity / l_truck) - (current_velocity * l_hitch * math.cos(current_hitch_angle_rad)) / (l_truck * l_trailer)

	return g



# Mathematische Teilfunktion für die Lenkwinkelberechnung
def f_phi(current_velocity, l_trailer,  current_hitch_angle, hitch_angle_target):
	
	current_hitch_angle_rad = math.radians(current_hitch_angle)

	f = hitch_angle_target + (current_velocity * math.sin(current_hitch_angle_rad)) / l_trailer

	return f
