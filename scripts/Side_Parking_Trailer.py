import time
import roslibpy
import math
import numpy


from Hitch_Angle_Reg import calc_steer_angle_reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data_Trailer
from Trajectory_Regulator import Trajectory_Regulator
from Data_Plot import plot_PID_regulator
from Data_Plot import plot_trajectory_trailer
from Data_Plot import plot_hitch_regulator
from Data_Plot import plot_trajectory_truck_trailer
from Maneuver_Parts_Trailer import Maneuver_Parts_Trailer
from Coordinate_system import Coordinate_System


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz der Koordinaten System Klasse
myCoords = Coordinate_System(myVehicle)

# Instanz Trajektorie Berechnung
myTrajectory = Trajectory_Data_Trailer()

# Instanz Manöver
myMParts = Maneuver_Parts_Trailer()

# Instanz des Reglers um auf Trajektorie zu Fahren
straight_PID = Trajectory_Regulator(100, 100, 100)
roll_on_PID = Trajectory_Regulator(200, 200, 200)
curved_PID_first = Trajectory_Regulator(100, 0, 100)
switching_PID = Trajectory_Regulator(30, 0, 0)
curved_PID_second = Trajectory_Regulator(30, 0, 0)
straight_PID_end = Trajectory_Regulator(30, 0, 0)

# Geschwindigkeit mit dem das Manöver durchgeführt werden soll
# --> Definition in Maneuver_Parts_Trailer
	
# ungefähre Zykluszeit mit dem die Loop durchgeführt wird in Sekunden --> In IPG Truckmaker 20 Hz eingestellt
time_step = 0.05


# Container zum Speichern von Lenk- und Knickwinkel

hitch_data_1 = []
steer_data_1 = []
hitch_target_1 = 0.0

hitch_data_2 = []
steer_data_2 = []
hitch_target_2 = 0.0

hitch_data_3 = []
steer_data_3 = []
hitch_target_3 = 0.0

# Initiale Werte, die gepublished werden
pub_velocity = 0
pub_steering = 0

steer_target = 0

# Starten des Ros-Clients
myClient.start_client()

# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()

# Warten bis Ros-Kommunikation vollständig da ist
mySubs.wait_for_signals()

# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myCoords.calc_positions(mySubs.get_velocity(), mySubs.get_steering_angle(), mySubs.get_hitch_angle())
print("Coords for calc:", myCoords.x_hitch, myCoords.y_hitch, myCoords.x_rear, myCoords.y_rear)
myTrajectory.calc_trajectory_straight(myCoords.x_hitch, myCoords.y_hitch, myCoords.x_rear, myCoords.y_rear)
myVehicle.save_hitch_angle(mySubs.get_hitch_angle())

# Main Loop
while(not myMParts.state_park_finished):
	# Warten, bis ROS neuer Simulationstakt sendet
	mySubs.wait_for_signals()

	# Alter Kommentar: Bestimmung Trailer Hinterachse Position, nötig für die Regelung der Trajektorie beim Rückwärtsfahren
	# Neu: Berechnung Positionen
	myCoords.calc_positions(mySubs.get_velocity(), mySubs.get_steering_angle(), mySubs.get_hitch_angle())


	# Teilmanöver: Vorbeifahren an Parklücke
	if (myMParts.state_park_prep == True):

		# Regelung zum Fahren auf gerader Trajektorie
		calculated_steering_angle = straight_PID.regulate_steering_straight(myCoords.x_rear, myCoords.y_rear, myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)
		
		# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_velocity = myMParts.maneuver_park_prep(mySubs, myVehicle, myTrajectory)


	# Teilmanöver: Aus- und wieder Anrollen
	if (myMParts.state_roll_out == True or myMParts.state_roll_on == True):

		# Distanz der Hinterachse des Trailers zur Trajektorie
		trailer_axis_offset_trajectory = roll_on_PID.calc_distance_to_parallel_route(myCoords.x_trailer, myCoords.y_trailer, myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight)

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_velocity = myMParts.maneuver_park_roll(mySubs, myCoords, myVehicle, myTrajectory, trailer_axis_offset_trajectory)
		
		# Anhalten
		if (myMParts.state_roll_out == True):
		
			# Regelung zum Fahren auf gerader Trajektorie, nutzt Regler aus vorherigem Teilmanöver, da noch Vorwärts, gerade gefahren wird
			calculated_steering_angle = straight_PID.regulate_steering_straight(myCoords.x_rear, myCoords.y_rear, myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)

			# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
			pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())


		# Wieder Anfahren, nun Rückwärts
		if (myMParts.state_roll_on == True):

			# Regelung zum Fahren auf gerader Trajektorie, neuer Regler fürs Rückwärtsanfahren
			calculated_steering_angle = roll_on_PID.regulate_steering_straight(myCoords.x_rear, myCoords.y_rear, myTrajectory.location_vector_straight, myTrajectory.orientation_vector_straight, time_step)
		
			# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
			pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), calculated_steering_angle, mySubs.get_steering_angle())
		

	# Teilmanöver: erster Kreis zum Parken
	if (myMParts.state_circle_first == True):
		
		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_velocity = myMParts.maneuver_circle_first(mySubs, myCoords, myVehicle, myTrajectory)
		
		# Berechnung des Knickwinkels, welcher für die Kreisfahrt benötigt wird
		hitch_target = - myVehicle.numeric_radius_to_hitch(myTrajectory.r_trajectory)
		print("\tRadius: ", myTrajectory.r_trajectory, "\tSoll-Hitch: ", hitch_target, "\tIst-Hitch: ", mySubs.get_hitch_angle())

		# Knickwinkel-Regler zum Einstellen des obig berechneten Knickwinkels
		steer_target = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle())

		# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), steer_target, mySubs.get_steering_angle())

		hitch_data_1.append(mySubs.get_hitch_angle())
		steer_data_1.append(pub_steering)
		hitch_target_1 = hitch_target


	# Teilmanöver: gerade Fahrt zwischen den beiden entgegengesetzten Kreisen, zum Wechseln des Knickwinkels
	if (myMParts.state_switch_circle == True):

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_velocity = myMParts.maneuver_switch_circle(mySubs, myVehicle, myTrajectory)

		#hitch_angle = straight_PID.regulate_hitch_straight(myCoords.x_trailer, myCoords.y_trailer, myTrajectory.location_vector_switch_circle, myTrajectory.orientation_vector_switch_circle, time_step)
		hitch_angle = 0.0

		# Regelung auf den gewünschten Knickwinkel
		steer_target = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_angle, hitch_angle - myVehicle.get_stored_hitch_angle())

		# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), steer_target, mySubs.get_steering_angle())

		hitch_data_2.append(mySubs.get_hitch_angle())
		steer_data_2.append(pub_steering)
		hitch_target_2 = hitch_angle
		

	# Teilmanöver: zweiter Kreis zum Parken
	if (myMParts.state_circle_second == True):

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_velocity = myMParts.maneuver_circle_second(mySubs, myVehicle, myTrajectory)

		# Berechnung des Knickwinkels, welcher für die Kreisfahrt benötigt wird
		hitch_target = myVehicle.numeric_radius_to_hitch( myTrajectory.r_trajectory)

	
		# Regelung auf den gewünschten Knickwinkel
		steer_target = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle())

		# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), steer_target, mySubs.get_steering_angle())

		hitch_data_3.append(mySubs.get_hitch_angle())
		steer_data_3.append(pub_steering)
		hitch_target_3 = hitch_target

		
	# Teilmanöver: Kurze gerade Fahrt, um Knickwinkel am Gespann in neutrale Position zu bringen
	if(myMParts.state_park_straighten == True):
		pub_velocity = myMParts.maneuver_park_straighten(mySubs, myVehicle, myTrajectory)
		
		#hitch_angle = straight_PID_end.regulate_hitch_straight(myCoords.x_trailer, myCoords.y_trailer, myTrajectory.location_vector_parking_lot, myTrajectory.orientation_vector_parking_lot, time_step)

		# Gespann soll sich wieder gerade ausrichten
		hitch_angle = 0.0

		# Regelung auf den gewünschten Knickwinkel
		steer_target = calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_angle, hitch_angle - myVehicle.get_stored_hitch_angle())
	
		# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), steer_target, mySubs.get_steering_angle())


	# Publishen der gewünschten Fahrbefehle
	print("\tIst-Lenkwinkel: ", mySubs.get_steering_angle(), "\tSoll-Lenkwinkel: ", steer_target)
	myPub.pub_velc_steer(pub_velocity, steer_target)

	# Speichern der Positionsdaten für spätere Auswertung und Analysis
	myVehicle.save_position_data(myCoords.x_trailer, myCoords.y_trailer)
	myVehicle.save_position_data_2(myCoords.x_rear, myCoords.y_rear)


# Plot der Regler-Daten
plot_hitch_regulator(steer_data_1, hitch_data_1, hitch_target_1, time_step, "Poly-Approx", "Poly-Approx")
plot_hitch_regulator(steer_data_2, hitch_data_2, hitch_target_2, time_step, "Poly-Approx", "Poly-Approx")
plot_hitch_regulator(steer_data_3, hitch_data_3, hitch_target_3, time_step, "Poly-Approx", "Poly-Approx")

# Berechnen der Plot-Punkte der Trajektorie für die spätere grafische Darstellung
myTrajectory.calc_straight_trajectory_data()
myTrajectory.calc_parking_lot_trajectory_data()
myTrajectory.calc_circle_first_data()
myTrajectory.calc_switch_circle_data()
myTrajectory.calc_circle_second_data()

# Plotten der Trajektorien zur Auswertung
plot_trajectory_trailer(myVehicle.get_posX_data(), myVehicle.get_posY_data(), myTrajectory.trajectory_straight_data_posX, myTrajectory.trajectory_straight_data_posY, myTrajectory.trajectory_parking_lot_data_posX, myTrajectory.trajectory_parking_lot_data_posY, myTrajectory.circle_first_data_posX, myTrajectory.circle_first_data_posY, myTrajectory.switch_circle_data_posX, myTrajectory.switch_circle_data_posY, myTrajectory.circle_second_data_posX, myTrajectory.circle_second_data_posY)

# Plotten der Trajektorien zur Auswertung
plot_trajectory_truck_trailer(myVehicle.posX_data_2, myVehicle.posY_data_2, myVehicle.get_posX_data(), myVehicle.get_posY_data(), myTrajectory.trajectory_straight_data_posX, myTrajectory.trajectory_straight_data_posY, myTrajectory.trajectory_parking_lot_data_posX, myTrajectory.trajectory_parking_lot_data_posY, myTrajectory.circle_first_data_posX, myTrajectory.circle_first_data_posY, myTrajectory.switch_circle_data_posX, myTrajectory.switch_circle_data_posY, myTrajectory.circle_second_data_posX, myTrajectory.circle_second_data_posY)
	
