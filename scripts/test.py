from Vehicle_Parameter import Vehicle_Data
import math

myVehicle = Vehicle_Data()

radius = 1812.31


hitch_target = - (myVehicle.numeric_radius_to_hitch(radius))
print("\tRadius: ",radius, "\tSoll-Hitch: ", hitch_target)